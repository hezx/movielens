#!/bin/sh
#
# File: paste.sh
#
# Created: 2013-11-25 by Zongxiao He
#

models=`grep -v "^#" model.txt | cut -f2`
trainout="ml_train.csv"
testout="ml_test.csv"


fristmodel=true
for m in $models
do
	echo $m
	pfile="./MovieLensModel/probe_${m}.csv"
	wfile="./MovieLensModel/wts_${m}.csv"
	if [ "$fristmodel" = "true" ] 
	then
		fristmodel=false
		cut -d"," -f2 ${pfile} > $trainout
		cut -d"," -f2 ${wfile} > $testout
	else
		cut -d"," -f2 ${pfile} > tmp1
		paste -d ',' $trainout tmp1 > tmp2
		mv tmp2 $trainout

		cut -d"," -f2 ${wfile} > ttmp1
		paste -d ',' $testout ttmp1 > ttmp2
		mv ttmp2 $testout	
		
		rm tmp1 ttmp1
	fi
done

# lastly append user support and rating
sed '1d' $trainout > tmp111
paste -d"," tmp111 ./MovieLensData/probe_suffix.txt > train.csv

sed '1d' $testout > ttmp111
paste -d"," ttmp111 ./MovieLensData/wts_suffix.txt > test.csv

rm tmp111 ttmp111
