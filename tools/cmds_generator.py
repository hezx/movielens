#!/usr/bin/env python
#
# File: 
#
__author__ = "Zongxiao He"
__copyright__ = "Copyright 2013"
__license__ = "GPL"
__version__ = "1.0"
__email__ = "zongxiah@bcm.edu"
__date__ = "2013-09-28"

import argparse
import random
import sys
import numpy as np
from datetime import datetime, date, time


###
# parameters
###



# gamma1 = [0.002, 0.005, 0.01, 0.02]
# gamma3 = [0.03, 0.05, 0.07, 0.1]
# lambda6 = [0.002, 0.005, 0.008, 0.01]
# lambda8 = [0.002, 0.005, 0.01, 0.02]

#np.arange(1, 10, 1)
# svd++ and neighbor

# gamma1 = [0.005, 0.008, 0.01, 0.015, 0.02] 
# gamma2 = [0.008, 0.01, 0.15]
# lambda6 = [0.001, 0.002, 0.005, 0.008, 0.01, 0.015, 0.02]
# lambda7 = [0.03, 0.05, 0.07]
# factor = [200, 300, 400]

# gamma1 = [0.01, 0.02, 0.03, 0.05, 0.07]
# gamma2 = [0.02]
# lambda6 = [0.001, 0.005, 0.01, 0.02, 0.03, 0.05]
# lambda7 = [ 0.02]
# factor=[100, 200, 300, 400]

# gamma1 = [0.001, 0.005, 0.01, 0.05]
# gamma2 = [0.005, 0.01, 0.05]
# gamma3 = [0.005, 0.01, 0.05, 0.07]
# lambda6 = [0.001, 0.005, 0.01, 0.05]
# lambda7 = [0.001, 0.005, 0.01, 0.05]
# lambda8 = [0.001, 0.005, 0.01, 0.02]
# factor=[100, 400]


# gamma1 = [0.001, 0.002, 0.005, 0.15, 0.01, 0.02, 0.05] 
# gamma3 = [0.03, 0.05, 0.07]
# lambda6 = [0.0005, 0.001, 0.002, 0.005, 0.01, 0.15, 0.02]
# lambda8 = [0.002, 0.005, 0.01, 0.015]

# gamma1 = [0.02, 0.25, 0.03, 0.04, 0.05]
# gamma2 = [0.015, 0.02, 0.025]
# lambda6 = [0.001, 0.005, 0.01, 0.015, 0.02, 0.03]
# lambda7 = [0.015, 0.02, 0.025]
# factor=[100, 200, 300]

# gamma1 = [0.03, 0.04, 0.05, 0.06, 0.07]
# gamma2 = [0.02, 0.03, 0.04]
# gamma3 = [0.008, 0.009, 0.01]
# lambda6 = [0.001, 0.003, 0.005, 0.008, 0.01]
# lambda7 = [0.02, 0.03, 0.04]
# lambda8 = [0.10, 0.15, 0.20]
# factor=[100, 200, 300, 400]

# model = "svdasym"
# train = "probe"
# gamma1 = [0.008, 0.01, 0.012, 0.015] 
# gamma2 = [0.01, 0.012, 0.015]
# lambda6 = [0.0005, 0.0008, 0.001, 0.002, 0.005]
# lambda7 = [0.04, 0.05, 0.06]
# factor = [100, 200, 300]

model = "svdneighbor"
train = "probe"
# gamma1 = [0.02, 0.025, 0.03, 0.035, 0.04] 
# gamma2 = [0.018, 0.02, 0.022]
# lambda6 = [0.005, 0.008, 0.01, 0.015, 0.02]
# lambda7 = [0.025, 0.03, 0.035]
# factor = [100, 150, 200]

gamma1 = [0.03, 0.04, 0.05]
gamma2 = [0.035, 0.04, 0.045]
gamma3 = [0.01, 0.012]
lambda6 = [0.001, 0.003, 0.005, 0.008, 0.01]
lambda7 = [0.035, 0.04, 0.045]
lambda8 = [0.15]
factor=[300, 400]

# gamma0 = [0.0001, 0.0002, 0.0005, 0.0007, 0.001, 0.005, 0.01, 0.005]
# lambda0 = [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.015, 0.02, 0.05, 0.1]

###
# write to file
#ofs = open(outfile, 'w')
curtime = datetime.now().strftime("-%d%H%M%S")

for k in factor:
    for g1 in gamma1:
        for g2 in gamma2:
            for g3 in gamma3:
                for l6 in lambda6:
                    for l7 in lambda7:
                        for l8 in lambda8:
                            pstr = model + "_" + train + "_factor" + str(k) + "_g1E" + str(g1) + "_g2E" + str(g2) + "_g3E" + str(g3) + "_l6E" + str(l6) + "_l7E" + str(l7) + "_l8E" + str(l8) + curtime
                            # pstr = model + "_" + train + "_factor" + str(k) + "_g1E" + str(g1) + "_g2E" + str(g2) + "_l6E" + str(l6) + "_l7E" + str(l7) + curtime
                            # pstr = model + "_" + train +  "_g1E" + str(g1) + "_g3E" + str(g3) + "_l6E" + str(l6) + "_l8E" + str(l8) + curtime
                            proj = pstr.replace(".", "p")
                            cmd = "./MovieLens " + proj + " " + model + " " + train + " "
                            cmd = cmd + " --gamma1 " + str(g1) + " --gamma2 " + str(g2) + " --gamma3 " + str(g3) + " --lambda6 " + str(l6) + " --lambda7 " + str(l7) + " --lambda8 " + str(l8) +  " -e " + str(k) + " -m"
                            # cmd = cmd + " --gamma1 " + str(g1) + " --gamma2 " + str(g2) + " --lambda6 " + str(l6) + " --lambda7 " + str(l7) + " -e " + str(k) + " -m"
                            # cmd = cmd + " --gamma1 " + str(g1) + " --gamma3 " + str(g3) + " --lambda6 " + str(l6) + " --lambda8 " + str(l8) + " -m"
                            print cmd



#model = "baseline"
#train = "probe"
#for g0 in gamma0:
#    for l0 in lambda0:
#        pstr = model + "_" + train + "_g0E" + str(g0) + "_l0E" + str(l0) + curtime
#        proj = pstr.replace(".", "p")
#        cmd = "./MovieLens " + proj + " " + model + " " + train + " "
#        cmd = cmd + " --gamma0 " + str(g0) + " --lambda0 " + str(l0)  + " "
#        print cmd
