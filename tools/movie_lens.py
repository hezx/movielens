#!/usr/bin/env python
#
# File: movie_lens.py
#
__author__ = "Zongxiao He"
__copyright__ = "Copyright 2013"
__license__ = "GPL"
__version__ = "1.0"
__email__ = "zongxiah@bcm.edu"
__date__ = "2013-09-28"

import argparse
import random
import sys

def add_com_args(parser):
    parser.add_argument('-i', '--input',
                        help='''input folder''')
    parser.add_argument('-o', '--output',
                        help='''output folder''')

def split_data(args):
    ofile = args.output + "/wts_train_rating.csv"
    ofs = open(ofile, 'w')
    with open(args.input + "/wts_train_rating.csv") as f:
        counter = 1
        for line in f:
            if(counter == 1):
                ofs.write(line)
            else:
                items = line.split(",")
                ids = int(items[0].rstrip())
                rat = int(items[1].rstrip())
                uid = (ids-1)%6040
                iid = (ids-1)/6040
                if uid < args.user and iid < args.movie:
                    ofs.write(str(uid +1 + iid*args.user) + "," + str(rat) + "\n")
            counter = counter + 1
    ofs.close()

def probe_data(args):
    pfile = args.output + "/pts_train_rating.csv"
    pfs = open(pfile, 'w')
    rfile = args.output + "/probe_train_rating.csv"
    rfs = open(rfile, 'w')

    with open(args.input + "/wts_train_rating.csv") as f:
        counter = 1
        for line in f:
            if(counter == 1):
                pfs.write(line)
                rfs.write(line)
            else:
                if random.random() <= args.proportion:
                    rfs.write(line)
                else:
                    pfs.write(line)
            counter = counter + 1
    pfs.close()
    rfs.close()



    
if __name__ == '__main__':
    master_parser = argparse.ArgumentParser(
        description = '''Movie Lens dataset manipulation''',
        prog = 'movie_lens',
        fromfile_prefix_chars = '@',
        epilog = '''Zongxiao He and Di Fu (c) 2013. Contact: {zh6, df14}@rice.edu''')
    master_parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    subparsers = master_parser.add_subparsers()

    # basic stats
    # stat_parser = subparsers.add_parser('stats', help="summary statistics about movie lens")
    # add_com_args(stat_parser)
    # stat_parser.set_defaults(func=stats)
    
    ##
    split_parser = subparsers.add_parser('split', help="partition a small dataset for debug")
    add_com_args(split_parser)
    split_parser.add_argument('-u', '--user',
                           type=int, default=600,
                           help='''number of user''')
    split_parser.add_argument('-m', '--movie',
                           type=int, default=300,
                           help='''number of movie''')
    split_parser.set_defaults(func=split_data)

    ##
    probe_parser = subparsers.add_parser('probe', 
                                      help="partition the whole training set into training set and probe set")
    add_com_args(probe_parser)
    probe_parser.add_argument('-p', '--proportion',
                           type=float, default=0.03,
                           help='''proportion of probe set''')
    probe_parser.set_defaults(func=probe_data)
    
    # getting args
    args = master_parser.parse_args()

    # run program
    try:
        args.func(args)
    except Exception as e:
        sys.exit(e)



