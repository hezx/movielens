#!/usr/bin/env python
#
# File: tuning.py
#
__author__ = "Zongxiao He"
__copyright__ = "Copyright 2013"
__license__ = "GPL"
__version__ = "1.0"
__email__ = "zongxiah@bcm.edu"
__date__ = "2013-09-28"

import argparse
import random
import sys
import glob
import json
from operator import itemgetter
import numpy as np
import math


def count(args):
    countdict = {}
    ifs = open(args.input)
    for line in ifs:
        if line.startswith("ID"):
            continue
        ids = int(line.split(",")[0]) 
        userid = ids % 6400
        if userid == 0:
            userid = 6400
        if userid in countdict:
            countdict[userid] = countdict[userid] + 1
        else:
            countdict[userid] = 1
            
    ofs = open(args.output, 'w')
    for k,v in countdict.items():
        ofs.write(str(k) + "\t" + str(math.log(v)) + "\n")
    ifs.close()
    ofs.close()


    
def read_count(filename):
    my_dict = {}
    with open(filename, 'r') as f:
        for line in f:
            items = line.split()
            key, values = items[0], items[1]
            my_dict[key] = values
    return my_dict

def read_rating(filename):
    my_dict = {}
    with open(filename, 'r') as f:
        for line in f:
            if line.startswith("ID"):
                continue
            items = line.rstrip().split(",")
            key, values = items[0], items[1]
            my_dict[key] = values
    return my_dict
    
    
def suffix(args):
    ifs = open(args.input)
    ofs = open(args.output, 'w')
    rating = {}
    if args.probe:
        rating = read_rating(args.rate)
    countdict = read_count(args.count)
    n = 0
    for line in ifs:
        n = n+1
        if line.startswith("ID"):
            continue
        ids = int(line.rstrip()) 
        userid = ids % 6400
        if userid == 0:
            userid = 6400

        res = str(countdict[str(userid)]) + ","

        if args.probe:
            if str(ids) not in rating:
                res = res + "3.5"
                print ids
            else:
                res = res + rating[str(ids)]
        else:
            res = res + "0"
        ofs.write(res + "\n")

        if (n%1000000 == 0):
            print str(n) + " is dealed!"
    ofs.close()
    ifs.close()
            
    return

        
if __name__ == '__main__':
    master_parser = argparse.ArgumentParser(
        description = '''Tools for blending''',
        prog = 'blending',
        fromfile_prefix_chars = '@',
        epilog = '''Zongxiao He and Di Fu (c) 2013. Contact: {zh6, df14}@rice.edu''')
    master_parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    subparsers = master_parser.add_subparsers()

    count_parser = subparsers.add_parser('count', help="user support count")
    count_parser.add_argument('-i', '--input', help='''input file''')
    count_parser.add_argument('-o', '--output', help='''output file''')
    count_parser.set_defaults(func=count)
    
    
    suffix_parser = subparsers.add_parser('suffix', help="generate last two columns for blending program ELF")
    suffix_parser.add_argument('-i', '--input', help='''input file''')
    suffix_parser.add_argument('-o', '--output', help='''output file''')
    suffix_parser.add_argument('-c', '--count', help='''user support count file''')
    suffix_parser.add_argument('-r', '--rate', help='''probe rating''')
    suffix_parser.add_argument('-p', '--probe', action='store_true', help='''include real rating, otherwise use 0''')
    suffix_parser.set_defaults(func=suffix)


    
    # getting args
    args = master_parser.parse_args()
    
    # run program
    try:
        args.func(args)
    except Exception as e:
        sys.exit(e)
        


