#!/usr/bin/env python
#
# File: heatmap.py
#
__author__ = "Zongxiao He"
__copyright__ = "Copyright 2013, Leal Group"
__license__ = "GPL"
__version__ = "1.0"
__email__ = "zongxiah@bcm.edu"
__date__ = "2013-11-19"


import sys, os
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Arial']})
rc('text', usetex=True)
import math
import numpy as np



def pairwise_heatmap(args, headers, values):
    npara = len(headers) - 2
    if npara < 2:
        print("Two few parameters to plot \n")
        sys.exit(1)
    nplots = npara*(npara - 1)/2

    Z = [i[0] for i in values]
    minv, maxv = min(Z), max(Z)
    if nplots == 1:
        X = [i[1] for i in values]
        Y = [i[2] for i in values]
        dat, xx, yy = heat_format(X, Y, Z)
        fig, ax = plt.subplots(1)
        im = plt.imshow(dat, interpolation='nearest', vmin=minv,vmax=maxv,cmap=plt.cm.jet)
        # put the major ticks at the middle of each cell
        ax.set_title(headers[1] + " VS " + headers[2],fontsize=14)
        ax.xaxis.set_ticks(np.arange(len(xx)), minor=False)
        ax.set_xlabel(headers[1])
        ax.yaxis.set_ticks(np.arange(len(yy)), minor=False)
        ax.set_ylabel(headers[2])
        ax.set_xticklabels(xx, minor=False)
        ax.set_yticklabels(yy, minor=False)
        fig.subplots_adjust(right=0.9)
        cbar_ax = fig.add_axes([0.95, 0.15, 0.02, 0.7])
        fig.colorbar(im, cax=cbar_ax)
        plt.savefig(args.output + ".pdf", bbox_inches="tight")
        return 
    
    ncol = 3
    nrow = int(math.ceil(1.0*nplots/ncol))
    fig, axes = plt.subplots(nrow, ncol, figsize = (18, 6.0*nrow))
    plt.subplots_adjust(wspace = .25, hspace = .15)

    n = 0
    for r in range(1, npara+1):
        for c in range(r+1, npara+1):
            xsi = n/ncol
            xsj = n%ncol
            if nrow == 1:
                xs = axes[xsj]
            else:
                xs = axes[xsi, xsj]
            n = n+1 

            X = [i[r] for i in values]
            Y = [i[c] for i in values]
            Z = [i[0] for i in values]

            dat, xx, yy = heat_format(X, Y, Z)

            # plot: nearest bilinear 
            im = xs.imshow(dat, interpolation='nearest', vmin=minv,vmax=maxv,cmap=plt.cm.jet)
            #xs.grid(True)
            # put the major ticks at the middle of each cell
            xs.set_title(headers[r] + " VS " + headers[c],fontsize=14)
            xs.xaxis.set_ticks(np.arange(len(xx)), minor=False)
            xs.set_xlabel(headers[r])
            xs.yaxis.set_ticks(np.arange(len(yy)), minor=False)
            xs.set_ylabel(headers[c])
            xs.set_xticklabels(xx, minor=False)
            xs.set_yticklabels(yy, minor=False)

    fig.subplots_adjust(right=0.9)
    cbar_ax = fig.add_axes([0.95, 0.15, 0.02, 0.7])
    fig.colorbar(im, cax=cbar_ax)
    plt.savefig(args.output + ".pdf", bbox_inches="tight")
         
    
###
# X,Y,Z --> (xlabel, ylabel, nxn matrix for Z)
def heat_format(X, Y, Z):
    xlabel = sorted(set(X))
    ylabel = sorted(set(Y), reverse=True )
    
    dl = {}
    for (index, x) in enumerate(X):
        if (x, Y[index]) in dl:
            dl[(x, Y[index])].append(Z[index])
        else:
            dl[(x, Y[index])] = []
            dl[(x, Y[index])].append(Z[index])
            
    d = {}
    for k, v in dl.items():
        d[k] = sum(v) / float(len(v))

    # x label = len(x) column 
    dat = [[2 for i in range(len(xlabel))] for j in range(len(ylabel))]    
    for (xi,x) in enumerate(xlabel):
        for (yi,y) in enumerate(ylabel):
            if (x,y) in d:
                dat[yi][xi] = d[(x,y)]

    return (np.array(dat), xlabel, ylabel)
    
