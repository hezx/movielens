#!/usr/bin/env python
#
# File: tuning.py
#
__author__ = "Zongxiao He"
__copyright__ = "Copyright 2013"
__license__ = "GPL"
__version__ = "1.0"
__email__ = "zongxiah@bcm.edu"
__date__ = "2013-09-28"

import argparse
import random
import sys
import glob
import json
from operator import itemgetter
import numpy as np
from heatmap import *



def add_com_args(parser):
    parser.add_argument('-i', '--input', help='''input folder''')
    parser.add_argument('-o', '--output', help='''output prefix''')
    parser.add_argument('-t', '--top', type=int, default=10, help='''t smallest rmse''')
    parser.add_argument('-p', '--para', nargs='+', help='''parameters to compare/plot''')
    parser.add_argument('-c', '--cv', action='store_true', help='''use cross-validation rmse, otherwise use probe rmse''')

    
def find_files(args):
    pat = "./" + args.input + "/*.json"
    ret = glob.glob(pat)
    if len(ret) == 0:
        print("No Json file found in " + pat)
        sys.exit(1)
    return ret

    
def extract_values(args, files):
    # header
    header = ["rmse"] + args.para + ["project"]
    values = []
    for f in files:
        onetry = []
        data = json.load(open(f))
        # RMSE
        try:
            if args.cv:
                rval = data["cv"]["rmse_mean"]
            else:
                rval = data["rmse"]["prmse"]
        except KeyError:
            print("No RMSE found in json file" + f)
            sys.exit(1)
        onetry.append(round(rval,6))

        # parameters
        for p in args.para:
            try:
                pval = data["parameter"][p]
                onetry.append(round(pval, 5))
            except KeyError:
                print("No para " + p + " in json file" + f)
                sys.exit(1)

        # project id
        try:
            proj = data["project"]
        except KeyError:
            proj = "sorry_no_project_id"
        onetry.append(proj)
        values.append(onetry)
    return (header, values)


def print_minimal(args, header, values):
    sdat = sorted(values, key=itemgetter(0))

    ofile = args.output + ".txt"
    ofs = open(ofile, 'w')
    print("\t".join(header))
    ofs.write("\t".join(header) + "\n")
    
    for i,v in enumerate(sdat):
        ofs.write("\t".join( [str(d) for d in v] )  + "\n")
        if i < args.top:
            print("\t".join( [str(d) for d in v] ))
    
    return


        

def show_para(args):
    # all json files in folder
    files = find_files(args)
    # extract values [rmse para1 para2 ]
    header, values = extract_values(args, files)
    # print minimal 
    print_minimal(args, header, values)
    # plot
    if(args.figure):
        pairwise_heatmap(args, header, values)
        
    return


def cv_para(args):
    # all json files in folder
    files = find_files(args)
    # extract values [rmse para1 para2 ]
    header, values = extract_values(args, files)
    # sort by RMSE
    sdat = sorted(values, key=itemgetter(0))
    jobs = [j[-1] for j in sdat][:args.top]

    ofile = args.output + "_cv.sh"
    ofs = open(ofile, 'w')
    
    # for i,v in enumerate(sdat):
    #     ofs.write("\t".join( [str(d) for d in v] )  + "\n")
    #     if i < args.top:
    #         print("\t".join( [str(d) for d in v] ))
            
    for j in jobs:
        f = "./" + args.input + "/" + j + ".json"
        data = json.load(open(f))
        cmd = "./MovieLens " + j + "_CV "

        try:
            cmd = cmd + data["train"]["model"] #model
            cmd = cmd + " cv " 
            iters = []
            for p in data["parameter"]:
                if p == "maxiter" or p == "probe_iters":
                    iters.append(data["parameter"][p])
                else:
                    cmd = cmd + " --" + p + " " + str(data["parameter"][p])
                    

            if (iters[0] == 0):
                cmd = cmd + " --maxiter " + str(iters[1])
            elif (iters[1] == 0):
                cmd = cmd + " --maxiter " + str(iters[0])
            elif (iters[0] <= iters[1]):
                cmd = cmd + " --maxiter " + str(iters[0])
            elif (iters[1] <= iters[0]):
                cmd = cmd + " --maxiter " + str(iters[1])
            else:
                print("can not determine iteration number")
            cmd = cmd + " -m "

            print cmd
            ofs.write(cmd + "\n")

        except KeyError:
            print("Errors when handle json file" + f)
            sys.exit(1)

    return


def cmds(args):
    # all json files in folder
    files = find_files(args)
    # extract values [rmse para1 para2 ]
    header, values = extract_values(args, files)
    # sort by RMSE
    sdat = sorted(values, key=itemgetter(0))
    jobs = [j[-1] for j in sdat][:args.top]

    ofile = args.output + "_commands.sh"
    ofs = open(ofile, 'w')
    
    # for i,v in enumerate(sdat):
    #     ofs.write("\t".join( [str(d) for d in v] )  + "\n")
    #     if i < args.top:
    #         print("\t".join( [str(d) for d in v] ))

    # cmd1 for probe prediction
    # cmd2 for wts prediction
    for j in jobs:
        f = "./" + args.input + "/" + j + ".json"
        jid = j.rstrip("_CV") # remove _CV at the end
        data = json.load(open(f))
        cmd1 = "./MovieLens " + "probe_" + jid + " "
        cmd2 = "./MovieLens " + "wts_" + jid + " "

        try:
            cmd1 = cmd1 + data["train"]["model"] + " probe " #model
            cmd2 = cmd2 + data["train"]["model"] + " wts "
            iters = []
            for p in data["parameter"]:
                if p == "maxiter" or p == "probe_iters":
                    iters.append(data["parameter"][p])
                else:
                    cmd1 = cmd1 + " --" + p + " " + str(data["parameter"][p])
                    cmd2 = cmd2 + " --" + p + " " + str(data["parameter"][p])

            i = ""
            if (iters[0] == 0):
                i = str(iters[1])
            elif (iters[1] == 0):
                i = str(iters[0])
            elif (iters[0] <= iters[1]):
                i = str(iters[0])
            elif (iters[1] <= iters[0]):
                i = str(iters[1])
            else:
                print("can not determine iteration number")
            cmd1 = cmd1 + " --maxiter " + i
            cmd2 = cmd2 + " --maxiter " + i + " -l" # do not include original dataset
            #cmd = cmd + " -m "

            print cmd1, "\n", cmd2
            ofs.write(cmd1 + "\n" + cmd2 + "\n")

        except KeyError:
            print("Errors when handle json file" + f)
            sys.exit(1)

    return

    
        
if __name__ == '__main__':
    master_parser = argparse.ArgumentParser(
        description = '''Tools to find best tuning parameters''',
        prog = 'tuning',
        fromfile_prefix_chars = '@',
        epilog = '''Zongxiao He and Di Fu (c) 2013. Contact: {zh6, df14}@rice.edu''')
    master_parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    subparsers = master_parser.add_subparsers()

    show_parser = subparsers.add_parser('show', help="find parameters with minimal RMSE")
    add_com_args(show_parser)
    show_parser.add_argument('-f', '--figure', action='store_true', help='''plot pairwise heatmap plot''')
    show_parser.set_defaults(func=show_para)


    cv_parser = subparsers.add_parser('cvcmd', help="generate cross-validation commands for minimal RMSE")
    add_com_args(cv_parser)
    cv_parser.set_defaults(func=cv_para)

    final_parser = subparsers.add_parser('cmds', help="generate final prediction commands from cv minimal RMSE")
    add_com_args(final_parser)
    final_parser.set_defaults(func=cmds)
    
    # getting args
    args = master_parser.parse_args()
    
    # run program
    try:
        args.func(args)
    except Exception as e:
        sys.exit(e)
        


