#ifndef __KERNEL_RIDGE_REGRESSION_
#define __KERNEL_RIDGE_REGRESSION_

#include "StandardAlgorithm.h"
#include "Framework.h"

/**
 * Kernel ridge regression
 * Regression in a high-dimensional feature space, defined by the kernel
 *
 * Tunable parameters are the regularization constant and some kernel constants
 *
 * Supported kernels:
 * - linear
 * - polynomial
 *
 */

class KernelRidgeRegression : public StandardAlgorithm, public Framework
{
public:
    KernelRidgeRegression();
    ~KernelRidgeRegression();

    virtual void modelInit();
    virtual void modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun );
    virtual void predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun );
    virtual void readSpecificMaps();
    virtual void saveWeights ( int cross );
    virtual void loadWeights ( int cross );
    virtual void loadMetaWeights ( int cross );

    static string templateGenerator ( int id, string preEffect, int nameID, bool blendStop );

private:

    // solution weights (per cross validation set)
    REAL** m_x;
    REAL* m_trainMatrix;
    double m_reg;

    // polynomial kernel
    double m_polyScale;
    double m_polyBiasPos;
    double m_polyBiasNeg;
    double m_polyPower;

    // gauss kernel
    double m_gaussSigma;

    // tanh kernel
    double m_tanhScale;
    double m_tanhBiasPos;
    double m_tanhBiasNeg;

    // dsc file
    string m_kernelType;
};


#endif
