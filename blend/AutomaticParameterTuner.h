#ifndef __AUTOMATIC_PARAMETER_TUNER_
#define __AUTOMATIC_PARAMETER_TUNER_

#include "Data.h"
#include "StreamOutput.h"
#include "NumericalTools.h"
#include "Framework.h"

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include <math.h>
#include <fstream>
#include <string>

using namespace std;

/**
 * Automatic Parameter Tuner
 *
 * This is used to tune some parameters in order to minimize a error function
 * Two different error function templates:
 * - calcRMSEonProbe()
 * - calcRMSEonBlend()
 *
 * The Algorithm class should be derived from this class to call the optimization functions
 * In the Algorithm class there should be a reimplementation of the two methods from above
 * And in those functions the error evaluation should be done there
 *
 * Three different optimizers are implemented here
 * - Stochastic search
 * - Structured coordinate search
 * - Nelder mead
 */

class AutomaticParameterTuner : public Framework
{
public:
    AutomaticParameterTuner();
    virtual ~AutomaticParameterTuner();

    void addEpochParameter ( int *param, string name );  // for epoch-wise training
    void addIntegerParameter ( int *param, string name, int min=INT_MIN, int max=INT_MAX );
    void addDoubleParameter ( double *param, string name, double min=-DBL_MAX, double max=DBL_MAX );
    void removeEpochParameter ( string name );
    void removeIntegerParameter ( string name );
    void removeDoubleParameter ( string name );
    virtual double calcRMSEonProbe() = 0;
    virtual double calcRMSEonBlend() = 0;
    virtual void saveBestPrediction() = 0;

    // ====================== stochastic tuner ======================

    void simpleStochasticParameterFinder ( double minProbeImpro=0.00002, int maxProbeEpochsWithoutImpro=100, double minBlendImpro=0.000001, int maxBlendEpochsWithoutImpro=200, double stdDev = 0.1 );

    void setOptimizeProbeRmse ( bool enable );
    void setOptimizeBlendRmse ( bool enable );

    void setDebug ( bool en );

    // ====================== structured tuner ======================


    // example for outside call
    //
    // AutomaticParameterTuner APT;
    // double param0 = 1.23;
    // APT.addDoubleParameter(&param0, "double");
    // APT.expSearchParams(100);
    // while(APT.expSearchChangeParams())
    // {
    //     double error = function(param0);
    //     expSearchCheckErr(error);
    // }

public:
    void expSearcher ( int minEpochs=0, int maxEpochs=200, int paramEpochs=3, int accelerationEpochs=2, double expInit=0.8, bool enableProbe=true, bool enableBlend=true );
    void expSearchParams ( int minEpochs=0, int maxEpochs=100, int paramEpochs=3, int accelerationEpochs=10000, double expInit=0.8 );
    bool expSearchChangeParams();
    void expSearchCheckErr ( double error );
    void expSearchReinitStepSize();
    void expSearchSetEpochsToMinimizeBlend ( int epochs );
    double expSearchGetLowestError();

protected:

    int m_expSearchMinEpochs;
    int m_expSearchMaxEpochs;
    int m_expSearchMaxEpochsBlend;
    int m_expSearchParamEpochs;
    int m_expSearchParamAccelerationEpochs;
    int m_expSearchEpoch;
    int m_epochParamPos;
    bool m_epochParamBreak;
    int m_expSearchDoubleParamPos;
    int m_expSearchIntParamPos;
    int m_expSearchVariationCnt;
    double m_expInit;
    double m_expSearchErrorBest;
    double m_expSearchAcceleration;
    int m_expSearchAccelerationEpoch;
    time_t m_expSearchTime;

    vector<int> m_epochParamBest;
    vector<double> m_expSearchExponent;
    vector<double> m_expSearchDoubleParamBest;
    vector<int> m_expSearchIntParamBest;
    vector<double> m_expSearchParamDirection;


    vector<int*> m_epochParam;
    vector<string> m_epochName;
    vector<double*> m_doubleParam;
    vector<string> m_doubleName;
    vector<int*> m_intParam;
    vector<string> m_intName;

    // set the range
    vector<double> m_doubleMin;
    vector<double> m_doubleMax;
    vector<int> m_intMin;
    vector<int> m_intMax;

    bool m_optimizeProbeRMSE;
    bool m_optimizeBlendRMSE;

    bool m_enableDebug;


    // ====================== nelder-mead tuner ======================
public:
    void NelderMeadSearch ( int maxEpochs=200 );
protected:
    double calcError ( const vector<double> &paramVector );
    void setCurrentParameter ( const vector<double> &paramVector );
    void calcCenter ( const vector<pair<double,vector<double> > > &simplexPoints, vector<double> &center );
    double getReflectionPoint ( const vector<double> &center, const vector<double> &worstPoint, vector<double> &reflectionPoint, double alpha );
    void plotParameters ( int epoch, const vector<pair<double,vector<double> > > &simplexPoints );
    void calcMinMax ( const vector<pair<double,vector<double> > > &simplexPoints, int paramNr, double &min, double &max );

};

#endif
