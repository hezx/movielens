#ifndef __NEURAL_NETWORK_
#define __NEURAL_NETWORK_

#include "StandardAlgorithm.h"
#include "Framework.h"
#include "nn.h"

using namespace std;

/**
 * Basic NeuralNetwork for real-valued prediction
 * The Algorithm makes use of the NN class.
 * In the NN class, there is the complete NN defined, this class is just for data preparation around
 *
 */

class NeuralNetwork : public StandardAlgorithm, public Framework
{
public:
    NeuralNetwork();
    ~NeuralNetwork();

    void demo();

    virtual void modelInit();
    virtual void modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun );
    virtual void predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun );
    virtual void readSpecificMaps();
    virtual void saveWeights ( int cross );
    virtual void loadWeights ( int cross );
    virtual void loadMetaWeights ( int cross );

    static string templateGenerator ( int id, string preEffect, int nameID, bool blendStop );

private:
    // inputs/targets
    REAL** m_inputs;

    // the NNs
    NN** m_nn;
    int m_epoch;

    // dsc file
    int m_nrLayer;
    int m_batchSize;
    double m_offsetOutputs;
    double m_scaleOutputs;
    double m_initWeightFactor;
    double m_learnrate;
    double m_learnrateMinimum;
    double m_learnrateSubtractionValueAfterEverySample;
    double m_learnrateSubtractionValueAfterEveryEpoch;
    double m_momentum;
    double m_weightDecay;
    double m_minUpdateErrorBound;
    double m_etaPosRPROP;
    double m_etaNegRPROP;
    double m_minUpdateRPROP;
    double m_maxUpdateRPROP;
    bool m_enableL1Regularization;
    bool m_enableErrorFunctionMAE;
    bool m_enableRPROP;
    bool m_useBLASforTraining;
    string m_neuronsPerLayer;
    string m_activationFunction;

};


#endif
