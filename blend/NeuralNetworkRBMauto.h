#ifndef __NEURAL_NETWORK_RBM_AUTO_
#define __NEURAL_NETWORK_RBM_AUTO_

#include "StandardAlgorithm.h"
#include "Framework.h"
#include "nnrbm.h"

using namespace std;

/**
 * EXPERIMENTAL ALGORITHM !!
 *
 * NeuralNetwork with an autoencoder for real-valued prediction, autoencoder weights are pretrained
 * by a RBM, RBM pretraining should give a good startpoint for the backprop optimization
 * -> finetuning
 *
 * The Algorithm makes use of a nnrbm class.
 *
 */

class NeuralNetworkRBMauto : public StandardAlgorithm, public Framework
{
public:
    NeuralNetworkRBMauto();
    ~NeuralNetworkRBMauto();

    virtual void modelInit();
    virtual void modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun );
    virtual void predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun );
    virtual void readSpecificMaps();
    virtual void saveWeights ( int cross );
    virtual void loadWeights ( int cross );
    virtual void loadMetaWeights ( int cross );

    static string templateGenerator ( int id, string preEffect, int nameID, bool blendStop );

private:
    // inputs/targets
    REAL** m_inputs;

    // the NNs
    NNRBM** m_nn;
    int m_epoch;

    // RBM setup
    bool *m_isFirstEpoch;

    // dsc file
    int m_nrLayerAuto;
    int m_nrLayerNetHidden;
    int m_batchSize;
    int m_tuningEpochsRBMFinetuning;
    int m_tuningEpochsOutputNet;
    double m_offsetOutputs;
    double m_scaleOutputs;
    double m_initWeightFactor;
    double m_learnrate;
    double m_learnrateAutoencoderFinetuning;
    double m_learnrateMinimum;
    double m_learnrateSubtractionValueAfterEverySample;
    double m_learnrateSubtractionValueAfterEveryEpoch;
    double m_momentum;
    double m_weightDecay;
    double m_minUpdateErrorBound;
    double m_etaPosRPROP;
    double m_etaNegRPROP;
    double m_minUpdateRPROP;
    double m_maxUpdateRPROP;
    bool m_enableL1Regularization;
    bool m_enableErrorFunctionMAE;
    bool m_enableRPROP;
    bool m_useBLASforTraining;
    string m_neuronsPerLayerAuto;
    string m_neuronsPerLayerNetHidden;

};


#endif
