#ifndef __NUMERICAL_TOOLS_
#define __NUMERICAL_TOOLS_

#include "StreamOutput.h"
#include "Framework.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
//#include <iostream>
#include <malloc.h>
#include <mkl.h>
#include <assert.h>
#include <cstring>


#define ALLOC_MEM 1
#define FREE_MEM 2

using namespace std;

/**
 * A collection of numerical tools that are used in this framework
 * - linear equation solvers (for multiple targets, non-negative solver)
 * - logistic equation solver (experimental)
 * - helper fuctions
 *
 */

class NumericalTools : public Framework
{
public:
    NumericalTools();
    ~NumericalTools();

    static void RidgeRegressionMultisolutionSinglecallGELSS ( REAL *A, REAL *b, REAL *x, int n, int m, int k, REAL lambda, bool isRowMajor=false );
    static void RidgeRegressionMultisolutionSinglecall ( REAL *A, REAL *b, REAL *x, int n, int m, int k, REAL lambda, bool isRowMajor, double* rideModifier=0 );
    int RidgeRegressionNonNegSinglecall ( REAL *A, REAL *b, REAL *x, int n, int m, REAL lamda, REAL eps, int maxIter, bool isRowMajor=true, int debug=0 );

    static void RidgeRegressionMultisolutionMulticall ( REAL *A, REAL *b, REAL *x, int n, int m, int k, REAL lambda, int alloc=0, bool isRowMajor=false );
    static int RidgeRegressionNonNegmulticall ( REAL *A, REAL *b, REAL *x, int n, int m, REAL lamda, REAL eps, int maxIter, int alloc=0,  bool isRowMajor=true, int debug=0 );

    static void LogisticRegressionMultisolutionSinglecall ( REAL *A, REAL *b, REAL *x, int n, int m, int k, REAL lambda, REAL offset = 0.0, REAL scale = 1.0, bool isRowMajor=true );

    static REAL getNormRandomNumber ( REAL mean, REAL std );
    static REAL getUniformRandomNumber ( REAL min, REAL max );
    static REAL clipValue ( REAL value, REAL min, REAL max );

private:

};

#endif
