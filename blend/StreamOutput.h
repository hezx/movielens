#ifndef _STREAMOUTPUT_H__
#define _STREAMOUTPUT_H__

#include <fstream>
#include <string>
#include <ctime>

#include "Framework.h"

/**
 * This class is the replacement for std::cout
 * With option to write to std out and write to a file
 * This works with operator overloading
 *
 */

class StreamOutput
{
public:
    StreamOutput ();
    ~StreamOutput ();
    void setOutputFile ( std::string s );
    StreamOutput& operator << ( const char* s );
    StreamOutput& operator << ( char* s );
    StreamOutput& operator << ( char s );
    StreamOutput& operator << ( int s );
    StreamOutput& operator << ( unsigned long s );
    StreamOutput& operator << ( double s );
    StreamOutput& operator << ( double* s );
    StreamOutput& operator << ( float s );
    StreamOutput& operator << ( float* s );
    StreamOutput& operator << ( time_t s );
    StreamOutput& operator << ( unsigned int s );
    StreamOutput& operator << ( std::string s );
    StreamOutput& operator << ( std::ostream& ( *s ) ( std::ostream& ) );
    void disableAllOutputs();
    void disableFileOutputs();
    void enableAllOutputs();

    std::string m_name;
    bool m_enableOutput;
    bool m_enableFileOutput;
};

#endif
