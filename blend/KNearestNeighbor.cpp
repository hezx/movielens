#include "KNearestNeighbor.h"

extern StreamOutput cout;

/**
 * Constructor
 */
KNearestNeighbor::KNearestNeighbor()
{
    cout<<"KNearestNeighbor"<<endl;
    // init member vars
    m_targetActualPtr = 0;
    m_lengthActual = 0;
    m_isLoadWeights = 0;
    m_xNormalized = 0;
    m_predictionAllOutputs = 0;
    m_predictionAllOutputsAllTargets = 0;
    m_scale = 0;
    m_scale2 = 0;
    m_offset = 0;
    m_globalOffset = 0;
    m_power = 0;
    m_k = 0;
    m_euclideanPower = 1.0;
    m_euclideanLimit = 0.01;
    m_evaluationBlockSize = 0;
    m_enableAdvancedKNN = 0;
    m_dataDenormalization = 0;
    m_takeEuclideanDistance = 0;
    m_optimizeEuclideanPower = 0;
}

/**
 * Destructor
 */
KNearestNeighbor::~KNearestNeighbor()
{
    cout<<"descructor KNearestNeighbor"<<endl;

    if ( m_xNormalized )
    {
        for ( int i=0;i<m_nCross+1;i++ )
            if ( m_xNormalized[i] )
                delete[] m_xNormalized[i];
        delete[] m_xNormalized;
    }
    m_xNormalized = 0;
    if ( m_targetActualPtr )
        delete[] m_targetActualPtr;
    m_targetActualPtr = 0;
    if ( m_lengthActual )
        delete[] m_lengthActual;
    m_lengthActual = 0;
}

/**
 * Read the Algorithm specific values from the description file
 *
 */
void KNearestNeighbor::readSpecificMaps()
{
    cout<<"Read specific maps"<<endl;

    // read dsc vars
    m_evaluationBlockSize = m_intMap["evaluationBlockSize"];
    m_k = m_intMap["kInit"];
    m_euclideanPower = m_doubleMap["euclideanPower"];
    m_euclideanLimit = m_doubleMap["euclideanLimit"];
    m_enableAdvancedKNN = m_boolMap["enableAdvancedKNN"];
    m_takeEuclideanDistance = m_boolMap["takeEuclideanDistance"];
    m_dataDenormalization = m_boolMap["dataDenormalization"];
    m_optimizeEuclideanPower = m_boolMap["optimizeEuclideanPower"];
    m_scale = m_doubleMap["initScale"];
    m_scale2 = m_doubleMap["initScale2"];
    m_offset = m_doubleMap["initOffset"];
    m_globalOffset = m_doubleMap["initGlobalOffset"];
    m_power = m_doubleMap["initPower"];
}

/**
 * Init the KNN model
 *
 */
void KNearestNeighbor::modelInit()
{
    // init / add tunable parameters

    // if advanced KNN is used
    if ( m_enableAdvancedKNN )
    {
        paramDoubleValues.push_back ( &m_globalOffset );
        paramDoubleNames.push_back ( "globalOffset" );
        paramDoubleValues.push_back ( &m_scale );
        paramDoubleNames.push_back ( "scale" );
        paramDoubleValues.push_back ( &m_scale2 );
        paramDoubleNames.push_back ( "scale2" );
        paramDoubleValues.push_back ( &m_offset );
        paramDoubleNames.push_back ( "offset" );
        paramDoubleValues.push_back ( &m_power );
        paramDoubleNames.push_back ( "power" );
    }

    // standard value k in k-NN, k-best neighbors
    paramIntValues.push_back ( &m_k );
    paramIntNames.push_back ( "k" );

    if ( m_takeEuclideanDistance && m_optimizeEuclideanPower )
    {
        paramDoubleValues.push_back ( &m_euclideanPower );
        paramDoubleNames.push_back ( "euclideanPower" );
        paramDoubleValues.push_back ( &m_euclideanLimit );
        paramDoubleNames.push_back ( "euclideanLimit" );
    }

    // init pointers
    if ( m_xNormalized == 0 )
    {
        m_xNormalized = new REAL*[m_nCross+1];
        m_targetActualPtr = new REAL*[m_nCross+1];
        m_lengthActual = new int[m_nCross+1];
        for ( int i=0;i<m_nCross+1;i++ )
        {
            m_xNormalized[i] = 0;
            m_targetActualPtr[i] = 0;
            m_lengthActual[i] = m_trainSize[i];
        }
    }
}

/**
 * Prediction for outside use, predicts outputs based on raw input values
 *
 * @param rawInputs The input feature, without normalization (raw)
 * @param outputs The output value (prediction of target)
 * @param nSamples The input size (number of rows)
 * @param crossRun Number of cross validation run (in training)
 */
void KNearestNeighbor::predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun )
{
    int lengthActual = m_lengthActual[crossRun];
    REAL* targetActualPtr = m_targetActualPtr[crossRun];

    // init outputs
    for ( int i=0;i<nSamples;i++ )
        for ( int j=0;j<m_nClass*m_nDomain;j++ )
            outputs[i*m_nClass*m_nDomain + j] = 0.0;

    REAL* allCorrelations = new REAL[m_evaluationBlockSize * lengthActual];
    REAL* allCorrelationsTransposed = new REAL[m_evaluationBlockSize * lengthActual];
    REAL* euclTmp = new REAL[m_nFeatures];
    REAL* squaredSumInputs = new REAL[m_evaluationBlockSize];
    REAL* squaredSumData = new REAL[lengthActual];

    // divide the prediction into blocks
    int sampleOffset = 0;
    bool breakLoop = true;
    while ( breakLoop )
    {
        int blockSize = nSamples - sampleOffset;
        if ( blockSize >= m_evaluationBlockSize )
            blockSize = m_evaluationBlockSize;
        if ( sampleOffset + blockSize == nSamples )
            breakLoop = false;

        // z-score of input
        REAL* zScores = new REAL[blockSize*m_nFeatures];
        for ( int i=0;i<blockSize;i++ )
        {
            REAL* ptr0 = zScores + i*m_nFeatures;
            REAL* ptr1 = rawInputs + ( i+sampleOffset ) *m_nFeatures;

            REAL mean = 0.0, std = 0.0, denormalized;
            for ( int j=0;j<m_nFeatures;j++ )
            {
                denormalized = ptr1[j];
                if ( m_dataDenormalization )
                    denormalized = denormalized * m_std[j] + m_mean[j];
                mean += denormalized;
            }
            mean /= ( REAL ) m_nFeatures;
            if ( m_takeEuclideanDistance )
                mean = 0.0;
            for ( int j=0;j<m_nFeatures;j++ )
            {
                denormalized = ptr1[j];
                if ( m_dataDenormalization )
                    denormalized = denormalized * m_std[j] + m_mean[j];
                std += ( denormalized - mean ) * ( denormalized - mean );
            }
            std = sqrt ( std / ( REAL ) ( m_nFeatures-1 ) );
            if ( m_takeEuclideanDistance )
                std = 1.0;
            for ( int j=0;j<m_nFeatures;j++ )
            {
                denormalized = ptr1[j];
                if ( m_dataDenormalization )
                    denormalized = denormalized * m_std[j] + m_mean[j];
                ptr0[j] = ( denormalized - mean ) / std;
            }
        }

        // calc all euclidean distances
        if ( m_takeEuclideanDistance )
        {
            // this code is slower
            /*for(int i=0;i<blockSize;i++)
            {
                REAL* corrPtr = allCorrelationsTransposed + i*lengthActual;
                for(int j=0;j<lengthActual;j++)
                {
                    REAL eucl = 0.0;
                    REAL* ptr0 = m_xNormalized[crossRun] + j*m_nFeatures;  // data
                    REAL* ptr1 = zScores + i*m_nFeatures;  // inputs
                    for(int k=0;k<m_nFeatures;k++)
                        eucl += (ptr0[k]-ptr1[k])*(ptr0[k]-ptr1[k]);
                    //corrPtr[j] = 1.0 / (sqrt(eucl) + 1e-9);  // <-- THIS IS ORIGINAL
                    corrPtr[j] = eucl;
                }
                for(int j=0;j<lengthActual;j++)
                    corrPtr[j] += 1e-9;
                V_POWC(lengthActual, corrPtr, m_euclideanPower, corrPtr);  // power=-0.5 results in: 1/sqrt(eucl)
            }
            */

            // speedup calculation by using BLAS in x'y: ||x-y||^2 = sum(x.^2) - 2 * x'y + sum(y.^2)
            for ( uint i=0;i<blockSize;i++ )
            {
                double sum = 0.0;
                REAL *ptr = zScores + i*m_nFeatures;
                for ( uint k=0;k<m_nFeatures;k++ )
                    sum += ptr[k] * ptr[k];
                squaredSumInputs[i] = sum;  // inputs feature squared sum
            }
            for ( uint i=0;i<lengthActual;i++ )
            {
                double sum = 0.0;
                REAL *ptr = m_xNormalized[crossRun] + i*m_nFeatures;
                for ( uint k=0;k<m_nFeatures;k++ )
                    sum += ptr[k] * ptr[k];
                squaredSumData[i] = sum;  // data feature squared sun
            }

            // allCorrelations = zScores * m_xNormalized[crossRun]'
            CBLAS_GEMM ( CblasRowMajor, CblasNoTrans, CblasTrans, lengthActual, blockSize, m_nFeatures, 1.0, m_xNormalized[crossRun], m_nFeatures, zScores, m_nFeatures, 0.0, allCorrelations, blockSize );

            // transpose to get linear column access
            // searching is much faster with linear access: ptr[j]
            for ( int i=0;i<lengthActual;i++ )
                for ( int j=0;j<blockSize;j++ )
                    allCorrelationsTransposed[i + j*lengthActual] = allCorrelations[i*blockSize + j];

            for ( int i=0;i<blockSize;i++ )
            {
                //if(m_evaluationBlockSize < blockSize)
                //{
                //    cout<<"m_block:"<<m_evaluationBlockSize<<" blockSize:"<<blockSize<<endl;
                //    assert(false);
                //}
                REAL* corrPtr = allCorrelationsTransposed + i*lengthActual;
                REAL sqSum = squaredSumInputs[i];

                for ( int j=0;j<lengthActual;j++ )
                {
                    corrPtr[j] = squaredSumData[j] + sqSum - 2.0 * corrPtr[j];
                    //corrPtr[j] = corrPtr[j]*corrPtr[j] + m_euclideanLimit;
                }
            }

            V_SQRT ( blockSize*lengthActual, allCorrelationsTransposed, allCorrelationsTransposed );
            for ( uint i=0;i<blockSize*lengthActual;i++ )
                allCorrelationsTransposed[i] += m_euclideanLimit;
            V_INV ( blockSize*lengthActual, allCorrelationsTransposed, allCorrelationsTransposed );
            V_POWC ( blockSize*lengthActual, allCorrelationsTransposed, m_euclideanPower, allCorrelationsTransposed );
        }
        else
        {
            // calc exact pearson: product of z-scores
            // this is a matrix-matrix multiplication and therefore fast
            CBLAS_GEMM ( CblasRowMajor, CblasNoTrans, CblasTrans, lengthActual, blockSize, m_nFeatures, 1.0, m_xNormalized[crossRun], m_nFeatures, zScores, m_nFeatures, 0.0, allCorrelations, blockSize );

            REAL scale = 1.0 / ( REAL ) ( m_nFeatures );
            V_MULC ( allCorrelations, scale, allCorrelations, blockSize * lengthActual );
            IPPS_THRESHOLD ( allCorrelations, allCorrelations, blockSize * lengthActual, -1.0, ippCmpLess );  // clip negative
            IPPS_THRESHOLD ( allCorrelations, allCorrelations, blockSize * lengthActual, +1.0, ippCmpGreater );  // clip positive

            // transpose to get linear column access
            // searching is much faster with linear access: ptr[j]
            for ( int i=0;i<lengthActual;i++ )
                for ( int j=0;j<blockSize;j++ )
                    allCorrelationsTransposed[i + j*lengthActual] = allCorrelations[i*blockSize + j];
        }

        int* indexKBest = new int[m_k];
        REAL* valueKBest = new REAL[m_k];

        // per input make a k-NN prediction
        for ( int i=0;i<blockSize;i++ )
        {
            /*REAL* predictionPtr = outputs + sampleOffset*m_nClass*m_nDomain + i*m_nClass*m_nDomain;
            REAL* corrPtr = allCorrelationsTransposed + i*lengthActual;

            REAL worstValue = 1e10;
            int worstIdx = -1;
            int topKLength = 0;

            // find k-best (largest) correlations
            for ( int j=0;j<lengthActual;j++ )
            {
                REAL c = corrPtr[j];

                // initially, fill top-k list
                if ( topKLength < m_k )
                {
                    indexKBest[topKLength] = j;
                    valueKBest[topKLength] = c;
                    if ( worstValue > c )
                    {
                        worstValue = c;
                        worstIdx = topKLength;
                    }
                    topKLength++;
                }
                else if ( c > worstValue ) // check for candidate
                {
                    indexKBest[worstIdx] = j;
                    valueKBest[worstIdx] = c;

                    // find the new worst value
                    worstIdx = -1;
                    worstValue = 1e10;
                    for ( int k=0;k<m_k;k++ )
                        if ( worstValue > valueKBest[k] )
                        {
                            worstValue = valueKBest[k];
                            worstIdx = k;
                        }
                }
            }

            // calculate k-nn: sum(ci * vi) / sum(|ci|)
            for ( int j=0;j<m_nClass*m_nDomain;j++ )
            {
                REAL sumCV = 0.0, sumC = 0.0, v;
                for ( int k=0;k<topKLength;k++ )
                {
                    int idx = indexKBest[k];
                    REAL c = corrPtr[idx]; // * (m_takeEuclideanDistance? -1.0 : 1.0);
                    if ( c > 1e6 && m_takeEuclideanDistance ) // reject train samples
                        continue;
                    REAL v = targetActualPtr[j + idx*m_nClass*m_nDomain];

                    // sigmoid mapping function
                    if ( m_enableAdvancedKNN )
                    {
                        REAL sign = c>0.0? 1.0 : -1.0;
                        c = pow ( fabs ( c ), ( float ) m_power ) * sign;
                        c = m_scale2 * 1.0 / ( 1.0 + exp ( - ( m_scale * c - m_offset ) ) ) + m_globalOffset;
                    }

                    sumCV += c * v;
                    sumC += fabs ( c );
                }
                v = sumCV / sumC;
                if ( isnan ( v ) || isinf ( v ) )
                    v = 0.0;
                predictionPtr[j] = v;
            }*/

            
            // OLD (slower search of nearest neighbors)
            // calculate k-nn: sum(ci * vi) / sum(|ci|)
            REAL* predictionPtr = outputs + sampleOffset*m_nClass*m_nDomain + i*m_nClass*m_nDomain;
            REAL sumC = 0.0;
            int k = 0;
            while(k<m_k)
            {
                // find max. correlation
                int indMax = -1;
                REAL max = -1e10;
                REAL* corrPtr = allCorrelationsTransposed + i*lengthActual;
                for(int j=0;j<lengthActual;j++)
                {
                    if(max < corrPtr[j])
                    {
                        max = corrPtr[j];
                        indMax = j;
                    }
                }

                if(indMax == -1)
                    break;

                // sigmoid mapping function
                if(m_enableAdvancedKNN)
                {
                    REAL sign = max>0.0? 1.0 : -1.0;
                    max = pow(fabs(max), (float)m_power) * sign;
                    max = m_scale2 * 1.0 / (1.0 + exp(- (m_scale * max - m_offset) ) ) + m_globalOffset;
                }

                REAL sign = 1.0;
                //if(m_takeEuclideanDistance)
                //    sign = -1.0;
                for(int j=0;j<m_nClass*m_nDomain;j++)
                    predictionPtr[j] += max * targetActualPtr[j + indMax*m_nClass*m_nDomain] * sign;

                // clear best
                corrPtr[indMax] = -1e10;

                // denomiator: sum of |correlation|
                sumC += fabs(max);
                k++;
            }
            if(sumC > 1e-6)
                for(int j=0;j<m_nClass*m_nDomain;j++)
                    predictionPtr[j] /= sumC;
            
        }

        sampleOffset += blockSize;

        delete[] indexKBest;
        delete[] valueKBest;

        if ( zScores )
            delete[] zScores;
        zScores = 0;
    }

    delete[] allCorrelations;
    delete[] allCorrelationsTransposed;
    delete[] euclTmp;
    delete[] squaredSumInputs;
    delete[] squaredSumData;
}

/**
 * Make a model update, set the new cross validation set or set the whole training set
 * for retraining
 *
 * KNN prediction model has no training phase
 * KNN finds prototypes in the train dataset based on a similarity measure
 *
 * Here, pearson correlation are used as distance measure
 * This is equivalent to dot products of z-scores
 *
 * @param input Pointer to input (can be cross validation set, or whole training set) (#rows x nFeatures)
 * @param target The targets (can be cross validation targets)
 * @param nSamples The sample size (#rows) in input
 * @param crossRun The cross validation run (for training)
 */
void KNearestNeighbor::modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun )
{
    m_targetActualPtr[crossRun] = target;
    m_lengthActual[crossRun] = nSamples;

    // allocate new memory
    if ( m_xNormalized[crossRun] == 0 )
        m_xNormalized[crossRun] = new REAL[nSamples*m_nFeatures];

    // calculate z-score for current train set
    for ( int i=0;i<nSamples;i++ )
    {
        REAL* ptr0 = m_xNormalized[crossRun] + i*m_nFeatures;
        REAL* ptr1 = input + i*m_nFeatures;

        // mean, std
        REAL mean = 0.0, std = 0.0, denormalized;
        for ( int j=0;j<m_nFeatures;j++ )
        {
            denormalized = ptr1[j];
            if ( m_dataDenormalization )
                denormalized = denormalized * m_std[j] + m_mean[j];
            mean += denormalized;
        }
        mean = mean / ( REAL ) m_nFeatures;
        if ( m_takeEuclideanDistance )
            mean = 0.0;
        for ( int j=0;j<m_nFeatures;j++ )
        {
            denormalized = ptr1[j];
            if ( m_dataDenormalization )
                denormalized = denormalized * m_std[j] + m_mean[j];
            std += ( denormalized - mean ) * ( denormalized - mean );
        }
        if ( std == 0.0 )
            std = 1.0;
        else
            std = sqrt ( std / ( REAL ) ( m_nFeatures-1 ) );
        if ( m_takeEuclideanDistance )
            std = 1.0;
        for ( int j=0;j<m_nFeatures;j++ )
        {
            denormalized = ptr1[j];
            if ( m_dataDenormalization )
                denormalized = denormalized * m_std[j] + m_mean[j];
            ptr0[j] = ( denormalized - mean ) / std;
        }
    }
}

/**
 * Save the weights and all other parameters for loading the complete prediction model
 *
 */
void KNearestNeighbor::saveWeights ( int cross )
{
    char buf[1024];
    sprintf ( buf,"%03d",cross );
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile + "." + buf + Framework::getGradientBoostingEpochString();
    if ( m_inRetraining )
        cout<<"Save:"<<name<<endl;
    fstream f ( name.c_str(), ios::out );
    f.write ( ( char* ) &m_nTrain, sizeof ( int ) );
    f.write ( ( char* ) &m_nFeatures, sizeof ( int ) );
    f.write ( ( char* ) &m_nClass, sizeof ( int ) );
    f.write ( ( char* ) &m_nDomain, sizeof ( int ) );
    f.write ( ( char* ) &m_lengthActual[cross], sizeof ( int ) );
    f.write ( ( char* ) m_xNormalized[cross], sizeof ( REAL ) *m_lengthActual[cross]*m_nFeatures );
    f.write ( ( char* ) m_targetActualPtr[cross], sizeof ( REAL ) *m_lengthActual[cross]*m_nClass*m_nDomain );
    f.write ( ( char* ) &m_scale, sizeof ( double ) );
    f.write ( ( char* ) &m_scale2, sizeof ( double ) );
    f.write ( ( char* ) &m_offset, sizeof ( double ) );
    f.write ( ( char* ) &m_globalOffset, sizeof ( double ) );
    f.write ( ( char* ) &m_k, sizeof ( int ) );
    f.write ( ( char* ) &m_euclideanPower, sizeof ( double ) );
    f.write ( ( char* ) &m_euclideanLimit, sizeof ( double ) );
    f.write ( ( char* ) &m_maxSwing, sizeof ( double ) );
    f.close();
}

/**
 * Load the weights and all other parameters and make the model ready to predict
 *
 */
void KNearestNeighbor::loadWeights ( int cross )
{
    char buf[1024];
    sprintf ( buf,"%03d",cross );
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile + "." + buf + Framework::getGradientBoostingEpochString();
    cout<<"Load:"<<name<<endl;
    fstream f ( name.c_str(), ios::in );
    if ( f.is_open() == false )
        assert ( false );
    f.read ( ( char* ) &m_nTrain, sizeof ( int ) );
    f.read ( ( char* ) &m_nFeatures, sizeof ( int ) );
    f.read ( ( char* ) &m_nClass, sizeof ( int ) );
    f.read ( ( char* ) &m_nDomain, sizeof ( int ) );

    m_xNormalized = new REAL*[m_nCross+1];
    m_targetActualPtr = new REAL*[m_nCross+1];
    m_lengthActual = new int[m_nCross+1];
    for ( int i=0;i<m_nCross+1;i++ )
    {
        m_xNormalized[i] = 0;
        m_targetActualPtr[i] = 0;
        m_lengthActual[i] = 0;
    }

    f.read ( ( char* ) &m_lengthActual[cross], sizeof ( int ) );

    // the train prototype matrix
    m_xNormalized[cross] = new REAL[m_lengthActual[cross]*m_nFeatures];
    m_targetActualPtr[cross] = new REAL[m_lengthActual[cross]*m_nClass*m_nDomain];

    f.read ( ( char* ) m_xNormalized[cross], sizeof ( REAL ) *m_lengthActual[cross]*m_nFeatures );
    f.read ( ( char* ) m_targetActualPtr[cross], sizeof ( REAL ) *m_lengthActual[cross]*m_nClass*m_nDomain );
    f.read ( ( char* ) &m_scale, sizeof ( double ) );
    f.read ( ( char* ) &m_scale2, sizeof ( double ) );
    f.read ( ( char* ) &m_offset, sizeof ( double ) );
    f.read ( ( char* ) &m_globalOffset, sizeof ( double ) );
    f.read ( ( char* ) &m_k, sizeof ( int ) );
    f.read ( ( char* ) &m_euclideanPower, sizeof ( double ) );
    f.read ( ( char* ) &m_euclideanLimit, sizeof ( double ) );
    f.read ( ( char* ) &m_maxSwing, sizeof ( double ) );
    f.close();

    m_isLoadWeights = 1;
}

/**
 *
 */
void KNearestNeighbor::loadMetaWeights ( int cross )
{
    char buf[1024];
    sprintf ( buf,"%03d",cross );
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile + "." + buf + Framework::getGradientBoostingEpochString();
    cout<<"LoadMeta:"<<name<<endl;
    fstream f ( name.c_str(), ios::in );
    if ( f.is_open() == false )
        assert ( false );
    f.read ( ( char* ) &m_nTrain, sizeof ( int ) );
    f.read ( ( char* ) &m_nFeatures, sizeof ( int ) );
    f.read ( ( char* ) &m_nClass, sizeof ( int ) );
    f.read ( ( char* ) &m_nDomain, sizeof ( int ) );

    int lengthActual;
    f.read ( ( char* ) &lengthActual, sizeof ( int ) );

    REAL* tmp = new REAL[lengthActual*m_nFeatures];
    f.read ( ( char* ) tmp, sizeof ( REAL ) *lengthActual*m_nFeatures );
    delete[] tmp;
    tmp = new REAL[lengthActual*m_nClass*m_nDomain];
    f.read ( ( char* ) tmp, sizeof ( REAL ) *lengthActual*m_nClass*m_nDomain );
    delete[] tmp;
    f.read ( ( char* ) &m_scale, sizeof ( double ) );
    f.read ( ( char* ) &m_scale2, sizeof ( double ) );
    f.read ( ( char* ) &m_offset, sizeof ( double ) );
    f.read ( ( char* ) &m_globalOffset, sizeof ( double ) );
    f.read ( ( char* ) &m_k, sizeof ( int ) );
    f.read ( ( char* ) &m_euclideanPower, sizeof ( double ) );
    f.read ( ( char* ) &m_euclideanLimit, sizeof ( double ) );
    f.read ( ( char* ) &m_maxSwing, sizeof ( double ) );
    f.close();
}

/**
 * Generates a template of the description file
 *
 * @return The template string
 */
string KNearestNeighbor::templateGenerator ( int id, string preEffect, int nameID, bool blendStop )
{
    stringstream s;
    s<<"ALGORITHM=KNearestNeighbor"<<endl;
    s<<"ID="<<id<<endl;
    s<<"TRAIN_ON_FULLPREDICTOR="<<preEffect<<endl;
    s<<"DISABLE=0"<<endl;
    s<<endl;
    s<<"[int]"<<endl;
    s<<"kInit=10"<<endl;
    s<<"maxTuninigEpochs=10"<<endl;
    s<<"evaluationBlockSize=1000"<<endl;
    s<<endl;
    s<<"[double]"<<endl;
    s<<"initMaxSwing=1.0"<<endl;
    s<<"initScale=5.0"<<endl;
    s<<"initScale2=1.0"<<endl;
    s<<"initOffset=2.0"<<endl;
    s<<"initGlobalOffset=-0.05"<<endl;
    s<<"initPower=1.0"<<endl;
    s<<"euclideanPower=1.0"<<endl;
    s<<"euclideanLimit=0.01"<<endl;
    s<<endl;
    s<<"[bool]"<<endl;
    s<<"takeEuclideanDistance=1"<<endl;
    s<<"dataDenormalization=0"<<endl;
    s<<"optimizeEuclideanPower=0"<<endl;
    s<<endl;
    s<<"enableAdvancedKNN=0"<<endl;
    s<<endl;
    s<<"enableClipping=1"<<endl;
    s<<"enableTuneSwing=0"<<endl;
    s<<endl;
    s<<"minimzeProbe="<< ( !blendStop ) <<endl;
    s<<"minimzeProbeClassificationError=0"<<endl;
    s<<"minimzeBlend="<<blendStop<<endl;
    s<<"minimzeBlendClassificationError=0"<<endl;
    s<<endl;
    s<<"[string]"<<endl;
    s<<"weightFile=KNearestNeighbor_"<<nameID<<"_weights.dat"<<endl;
    s<<"fullPrediction=KNearestNeighbor_"<<nameID<<".dat"<<endl;

    return s.str();
}
