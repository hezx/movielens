#ifndef __GBDT_
#define __GBDT_

#include <deque>

#include "StandardAlgorithm.h"
#include "Framework.h"

/**
 * This is one node of the tree (for prediction and training)
 */
typedef struct node_
{
    int m_featureNr;                // decision on this feature
    REAL m_value;                   // the prediction value
    struct node_* m_toSmallerEqual; // pointer to node, if:  feature[m_featureNr] <=  m_value
    struct node_* m_toLarger;       // pointer to node, if:  feature[m_featureNr] > m_value
    int* m_trainSamples;            // a list of indices of the training samples in this node
    int m_nSamples;                 // the length of m_trainSamples
} node;

/**
 * this struct is used to build the heap data structure (for selecting the largest node)
 */
typedef struct nodeReduced_
{
    node* m_node;
    uint m_size;
} nodeReduced;

/**
 * Gradient boosted decision tree
 * An extention to: Jerome H. Friedman 1999 "Stochastic Gradient Boosting"
 *
 * The tree is build in a greedy fashion, to minimize the RMSE of the prediction
 * Greedy means that always the largest node gets splitted
 *
 * Splits can be performed optimally (in terms of RMSE) or random value (uniform sample indices)
 * Subspace size can be selected (number of features considered in a split)
 *
 */
class GBDT : public StandardAlgorithm, public Framework
{
public:
    GBDT();
    ~GBDT();

    virtual void modelInit();
    virtual void modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun );
    virtual void predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun );
    virtual void readSpecificMaps();
    virtual void saveWeights ( int cross );
    virtual void loadWeights ( int cross );
    virtual void loadMetaWeights ( int cross );

    static string templateGenerator ( int id, string preEffect, int nameID, bool blendStop );

private:
    void trainSingleTree ( node* n, deque<nodeReduced> &largestNodes, REAL* input, REAL* inputTmp, REAL* inputTargetsSort, REAL* singleTarget, bool* usedFeatures, int nSamples, int* sortIndex, int* radixTmp0, REAL* radixTmp1 );
    void cleanTree ( node* n );
    REAL predictSingleTree ( node* n, REAL* input );
    void saveTreeRecursive ( node* n, fstream &f );
    void loadTreeRecursive ( node* n, fstream &f );

    // tree members
    node*** m_trees;   // [nCross][target][boostingStep]
    int m_epoch;
    REAL** m_globalMean;  // [nCross][target]
    REAL** m_treeTargets;  // [nCross][target x samples]
    double** m_validationPredictions;  // [nCross][nClass*nDomain x m_probeSize]

    // dsc file
    int m_featureSubspaceSize;
    int m_maxTreeLeafes;
    bool m_useOptSplitPoint;
    bool m_calculateGlobalMean;
    double m_lRate;
};


#endif
