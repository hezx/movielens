#ifndef __LOGISTIC_REGRESSION_
#define __LOGISTIC_REGRESSION_

#include "StandardAlgorithm.h"
#include "Framework.h"

/**
 * EXPERIMENTAL ALGORITHM !!
 *
 * THIS CODE IS NOT fully CORRECT!! or too slow..
 * Please do not use this class
 *
 * Logistic regression prediction model
 * Based on a sigmoid wrapper function with a linear model
 * Look at: http://www.cs.cmu.edu/~ggordon/IRLS-example/
 *
 * Tunable parameters are the regularization constant
 *
 */

class LogisticRegression : public StandardAlgorithm, public Framework
{
public:
    LogisticRegression();
    ~LogisticRegression();

    virtual void modelInit();
    virtual void modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun );
    virtual void predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun );
    virtual void readSpecificMaps();
    virtual void saveWeights ( int cross );
    virtual void loadWeights ( int cross );
    virtual void loadMetaWeights ( int cross );

    static string templateGenerator ( int id, string preEffect, int nameID, bool blendStop );

private:

    // solution weights (per cross validation set)
    REAL** m_x;
    double m_reg;
    double m_scale;  // target transformation: targetNew = target*scale + offset
    double m_offset; // ouput transformation : outoutNew = (output-offset)/scale
    bool m_tuneOffsetScale;

    NumericalTools solver;
};


#endif
