#ifndef _CONFIG_H__
#define _CONFIG_H__

#include "ipps.h"
#include "mkl.h"

/**
 * The whole frameworks use internally the REAL datatype as floating point
 *
 * This header makes it possible to switch the floating point precision between 32/64 bit
 * For most algorithms it is enough when using single precision (float) accuracy
 * If the user want double precision, enable the first block an recompile the code
 * with "make clean" and "make".
 * Comment/Uncomment either one of the blocks below
 */


/*
typedef double REAL;
#define IPPS_THRESHOLD ippsThreshold_64f
#define CBLAS_SCAL cblas_dscal
#define CBLAS_GEMV cblas_dgemv
#define CBLAS_GEMM cblas_dgemm
#define CBLAS_DOT cblas_ddot
#define CBLAS_AXPY cblas_daxpy
#define V_SUB vdSub
#define V_ADD vdAdd
#define V_ADDCI ippsAddC_64f_I
#define V_MUL vdMul
#define V_MULC ippsMulC_64f
#define V_MULCI ippsMulC_64f_I
#define V_DIV vdDiv
#define V_DIVC ippsDivC_64f
#define GELSS dgelss
#define FIND_MAX cblas_idamax
#define V_COPY ippsCopy_64f
#define V_TANH vdTanh
#define V_SQR vdSqr
#define V_ZERO ippsZero_64f
#define V_NORMALIZE ippsNormalize_64f
#define V_POWC vdPowx
#define V_POW vdPow
#define LAPACK_POTRF dpotrf
#define LAPACK_POTRS dpotrs
#define SORT_ASCEND ippsSortAscend_64f_I
#define SORT_ASCEND_I ippsSortIndexAscend_64f_I
#define SORT_ASCEND_RADIX_I ippsSortRadixIndexAscend_64f
#define GAUSS_DISTRIBUTION ippsRandGauss_Direct_64f
#define UNIFORM_DISTRIBUTION ippsRandUniform_Direct_64f
#define V_EXP ippsExp_64f_I
#define V_INV vdInv
#define V_SQRT vdSqrt
#define SVD_DECOMP dgesvd
*/

typedef float REAL;
#define IPPS_THRESHOLD ippsThreshold_32f
#define CBLAS_SCAL cblas_sscal
#define CBLAS_GEMV cblas_sgemv
#define CBLAS_GEMM cblas_sgemm
#define CBLAS_DOT cblas_sdot
#define CBLAS_AXPY cblas_saxpy
#define V_SUB vsSub
#define V_ADD vsAdd
#define V_ADDCI ippsAddC_32f_I
#define V_MUL vsMul
#define V_MULC ippsMulC_32f
#define V_MULCI ippsMulC_32f_I
#define V_DIV vsDiv
#define V_DIVC ippsDivC_32f
#define GELSS sgelss
#define FIND_MAX cblas_isamax
#define V_COPY ippsCopy_32f
#define V_TANH vsTanh
#define V_SQR vsSqr
#define V_ZERO ippsZero_32f
#define V_NORMALIZE ippsNormalize_32f
#define V_POWC vsPowx
#define V_POW vsPow
#define LAPACK_POTRF spotrf
#define LAPACK_POTRS spotrs
#define SORT_ASCEND ippsSortAscend_32f_I
#define SORT_ASCEND_I ippsSortIndexAscend_32f_I
#define SORT_ASCEND_RADIX_I ippsSortRadixIndexAscend_32f
#define GAUSS_DISTRIBUTION ippsRandGauss_Direct_32f
#define UNIFORM_DISTRIBUTION ippsRandUniform_Direct_32f
#define V_EXP ippsExp_32f_I
#define V_INV vsInv
#define V_SQRT vsSqrt
#define SVD_DECOMP sgesvd



// Data directories for: dataset, algo-description, temporary and full-prediction files

#define DATA_PATH "DataFiles"
#define DSC_PATH "DscFiles"
#define TMP_PATH "TempFiles"
#define FULL_PREDICTOR_PATH "FullPredictorFiles"

#endif
