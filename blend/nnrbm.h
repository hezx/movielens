#ifndef __NNRBM_
#define __NNRBM_

#include "StreamOutput.h"
#include "Framework.h"

#include <math.h>
#include <assert.h>
#include <sstream>
#include <vector>

using namespace std;

/**
 * This is a Neural Network implementation with a RBM pretraining method
 *
 * The target of this class is to give a basic and fast class for training and prediction
 * It supports basic training functionality
 * - momentum
 * - weight decay
 * - batch training
 * - learnrate decreasing
 * - break criteria based on a probeset (hold out set)
 *
 * Data (features + targets) memeory allocation and normalization must be done outside
 * This class gets the pointer to the training and probe (=validation) set
 *
 * Forward and backward calculation can be done in loops or in Vector-Matrix operations (BLAS)
 * For large nets the BLAS calculation should be used (~2x faster)
 *
 * Standard training is performed with global learnrate and stochastic gradient descent
 * A batch training is also possible if the batchSize > 1
 *
 * This class implements also the RPROP learning algorithm
 * Rprop - Description and Implementation Details Martin Riedmiller, 1994. Technical report.
 *
 */

class NNRBM : public Framework
{
public:
    NNRBM();
    ~NNRBM();

    // input/output data definition methods
    void setNrTargets ( int n );
    void setNrInputs ( int n );
    void setNrExamplesTrain ( int n );
    void setNrExamplesProbe ( int n );
    void setTrainInputs ( REAL* inputs );
    void setTrainTargets ( REAL* targets );
    void setProbeInputs ( REAL* inputs );
    void setProbeTargets ( REAL* targets );

    // learn parameters
    void setInitWeightFactor ( REAL factor );
    void setLearnrate ( REAL learnrate );
    void setLearnrateMinimum ( REAL learnrateMin );
    void setLearnrateSubtractionValueAfterEverySample ( REAL learnrateDecreaseRate );
    void setLearnrateSubtractionValueAfterEveryEpoch ( REAL learnrateDecreaseRate );
    void setMomentum ( REAL momentum );
    void setWeightDecay ( REAL weightDecay );
    void setBatchSize ( int size );
    void setMinUpdateErrorBound ( REAL minUpdateBound );
    void setMaxEpochs ( int epochs );
    void setRPROPPosNeg ( REAL etaPos, REAL etaNeg );
    void setRPROPMinMaxUpdate ( REAL min, REAL max );
    void setL1Regularization ( bool en );
    void initNNWeights ( time_t seed );
    void enableErrorFunctionMAE ( bool en );
    void setRBMLearnParams ( REAL learnrateWeights, REAL learnrateBiasVis, REAL learnrateBiasHid, REAL weightDecay, int maxEpoch );

    // set net inner stucture
    void setNNStructure ( int nrLayer, int* neuronsPerLayer, bool lastLinearLayer = false, int* layerType = 0 );

    // all around training
    void printLearnrate();
    void setScaleOffset ( REAL scale, REAL offset );
    void setNormalTrainStopping ( bool en );
    void setGlobalEpochs ( int e );
    void enableRPROP ( bool en );
    void useBLASforTraining ( bool enable );
    void trainOneEpoch ( bool* updateLayer = 0 );
    int trainNN();
    REAL getRMSETrain();
    REAL getRMSEProbe();
    void predictSingleInput ( REAL* input, REAL* output );
    REAL* getWeightPtr();
    void setWeights ( REAL* w );
    int getNrWeights();
    double m_sumSquaredError;
    double m_sumSquaredErrorSamples;

    int getWeightIndex ( int layer, int neuron, int weight );
    int getBiasIndex ( int layer, int neuron );
    int getOutputIndex ( int layer, int neuron );

    void rbmPretraining ( REAL* input, REAL* target, int nSamples, int nTarget, int firstNLayer = -1, int crossRun = -1, int nFinetuningEpochs = -1, double finetuningLearnRate = 0.0 );
    vector<int> getEncoder();
    
    // tmp methods
    void printMiddleLayerToFile(string fname, REAL* input, REAL* target, int nSamples, int nTarget);
    void printAutoencoderWeightsToJavascript(string fname);

    NNRBM* m_nnAuto;

private:
    // training
    void saveWeights();
    REAL calcRMSE ( REAL* inputs, REAL* targets, int examples );
    void forwardCalculation ( REAL* input );
    void forwardCalculationBLAS ( REAL* input );
    void backpropBLAS ( REAL* input, REAL* target, bool* updateLayer = 0 );
    void backprop ( REAL* input, REAL* target, bool* updateLayer = 0 );

    // weight init
    REAL getInitWeight ( int fanIn );

    // data description
    int m_nrTargets;
    int m_nrInputs;
    int m_nrExamplesTrain;
    int m_nrExamplesProbe;
    REAL* m_inputsTrain;
    REAL* m_inputsProbe;
    REAL* m_targetsTrain;
    REAL* m_targetsProbe;

    // learn params
    REAL m_initWeightFactor;
    int m_globalEpochs;
    REAL m_RPROP_etaPos;
    REAL m_RPROP_etaNeg;
    REAL m_RPROP_updateMin;
    REAL m_RPROP_updateMax;
    REAL m_learnRate;
    REAL m_learnRateMin;
    REAL m_learnrateDecreaseRate;
    REAL m_learnrateDecreaseRateEpoch;
    REAL m_momentum;
    REAL m_weightDecay;
    REAL m_minUpdateBound;
    int m_batchSize;
    REAL m_rbmLearnrateWeights;
    REAL m_rbmLearnrateBiasVis;
    REAL m_rbmLearnrateBiasHid;
    REAL m_rbmWeightDecay;
    int m_rbmMaxEpochs;

    // training
    REAL m_scaleOutputs;
    REAL m_offsetOutputs;
    int m_maxEpochs;
    bool m_useBLAS;
    bool m_enableRPROP;
    bool m_normalTrainStopping;
    bool m_enableL1Regularization;
    bool m_errorFunctionMAE;

    // net description, inner structure
    int m_nrLayer;
    int* m_neuronsPerLayer;
    int m_nrWeights;
    int m_nrOutputs;
    int* m_nrLayWeights;
    int* m_nrLayWeightOffsets;
    REAL* m_outputs;
    REAL* m_outputsTmp;
    REAL* m_derivates;
    REAL* m_d1;
    REAL* m_weights;
    REAL* m_weightsTmp0;
    REAL* m_weightsTmp1;
    REAL* m_weightsTmp2;
    REAL* m_weightsBatchUpdate;
    REAL* m_weightsOld;
    REAL* m_weightsOldOld;
    REAL* m_deltaW;
    REAL* m_deltaWOld;
    REAL* m_adaptiveRPROPlRate;
    int* m_activationFunctionPerLayer;
    bool m_enableAutoencoder;

};

#endif
