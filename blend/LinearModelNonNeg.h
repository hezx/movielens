#ifndef __LINEARMODELNONNEG_
#define __LINEARMODELNONNEG_

#include "StandardAlgorithm.h"
#include "Framework.h"

/**
 * Linear prediction model with non-negativ solver
 * Based on simple linear regression on targets
 *
 * Tunable parameters are the regularization constant
 * and the target offsets
 */

class LinearModelNonNeg : public StandardAlgorithm, public Framework
{
public:
    LinearModelNonNeg();
    ~LinearModelNonNeg();

    virtual void modelInit();
    virtual void modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun );
    virtual void predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun );
    virtual void readSpecificMaps();
    virtual void saveWeights ( int cross );
    virtual void loadWeights ( int cross );
    virtual void loadMetaWeights ( int cross );

    static string templateGenerator ( int id, string preEffect, int nameID, bool blendStop );

private:

    // solution weights (per cross validation set)
    REAL** m_x;
    double m_reg;
    double m_scale;
    double m_bias;
    int m_maxNonNegSolverIterations;

    NumericalTools solver;

    // dsc file
    double m_initReg;
    double m_initScale;
    double m_initBias;
};


#endif
