#ifndef __STANDARD_ALGORITHM_
#define __STANDARD_ALGORITHM_

#include "Algorithm.h"
#include "AutomaticParameterTuner.h"
#include "BlendStopping.h"
#include "Framework.h"
#include "AUC.h"
#include "DatasetReader.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

/**
 * Base class for a standard algorithm
 * This is not a stand-alone class
 * Other algorithms must be derived from this class (which unites the requirements of a StandardAlgorithms)
 */

class StandardAlgorithm : public Algorithm, public AutomaticParameterTuner, public AUC, public Framework
{
public:
    StandardAlgorithm();
    ~StandardAlgorithm();

    virtual double calcRMSEonProbe();
    virtual double calcRMSEonBlend();
    void saveBestPrediction();

    virtual void setPredictionMode ( int cross );
    virtual double train();

    //int predictOutput(REAL* rawInput, REAL* effect, REAL* output);
    virtual void predictMultipleOutputs ( REAL* rawInput, REAL* effect, REAL* output, int* label, int nSamples, int crossRun );

    // must be implemented in the particular algorithm
    virtual void modelInit() = 0;
    virtual void modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun ) = 0;
    virtual void predictAllOutputs ( REAL* rawInputs, REAL* outputs,  uint nSamples, uint crossRun ) = 0;
    virtual void readSpecificMaps() = 0;
    virtual void saveWeights ( int cross ) = 0;
    virtual void loadWeights ( int cross ) = 0;
    virtual void loadMetaWeights ( int cross ) = 0;

protected:

    void init();
    void readMaps();

    void calculateFullPrediction();
    void writeFullPrediction(int nSamples);

    BlendStopping* m_blendStop;

    // params
    vector<int*> paramEpochValues;
    vector<string> paramEpochNames;
    vector<double*> paramDoubleValues;
    vector<string> paramDoubleNames;
    vector<int*> paramIntValues;
    vector<string> paramIntNames;
    double m_maxSwing;

    // tmp fields
    REAL* m_crossValidationPrediction;
    REAL* m_prediction;
    REAL* m_predictionBest;
    REAL** m_predictionProbe;
    REAL* m_singlePrediction;
    int* m_labelPrediction;
    int* m_wrongLabelCnt;
    REAL* m_outOfBagEstimate;
    int* m_outOfBagEstimateCnt;

    // dsc file
    int m_maxTuninigEpochs;
    int m_minTuninigEpochs;
    bool m_enableClipping;
    bool m_enableTuneSwing;
    bool m_minimzeProbe;
    bool m_minimzeProbeClassificationError;
    bool m_minimzeBlend;
    bool m_minimzeBlendClassificationError;
    double m_initMaxSwing;
    string m_weightFile;
    string m_fullPrediction;

};


#endif
