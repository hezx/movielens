#ifndef __BLENDING_NN_
#define __BLENDING_NN_

#include "Data.h"
#include "Framework.h"
#include "nn.h"
#include "AutomaticParameterTuner.h"

using namespace std;

/**
 * Train a neural network with k-fold cross-validation
 * in order to blend the existing fullPredictions after training
 * Fullpredictions are existing predictions of the training set from models
 *
 * This can be used to replace the linear blender
 * It should maybe? deliver better results in terms of RMSE
 *
 */

class BlendingNN : public Data, public AutomaticParameterTuner, public Framework
{
public:
    BlendingNN();
    ~BlendingNN();

    void readSpecificMaps();
    void init();
    void train();

    // re-implemented here (for parameter tuner)
    double calcRMSEonProbe();
    double calcRMSEonBlend();
    void saveBestPrediction();
    void loadWeights();

    // prediction
    void predictEnsembleOutput ( REAL** predictions, REAL* output );

private:

    // the NNs
    NN** m_nn;

    // train data
    REAL** m_fullPredictionsReal;
    REAL** m_inputsTrain;
    REAL** m_inputsProbe;

    // mean and std
    REAL* m_meanBlend;
    REAL* m_stdBlend;

    // prediction cache
    REAL* m_predictionCache;

    int m_nPredictors;
    vector<string> m_usedFiles;

    int m_epochs;
    int m_epochsBest;

    // dsc file
    int m_minTuninigEpochs;
    int m_maxTuninigEpochs;
    int m_nrLayer;
    int m_batchSize;
    double m_offsetOutputs;
    double m_scaleOutputs;
    double m_initWeightFactor;
    double m_learnrate;
    double m_learnrateMinimum;
    double m_learnrateSubtractionValueAfterEverySample;
    double m_learnrateSubtractionValueAfterEveryEpoch;
    double m_momentum;
    double m_weightDecay;
    double m_minUpdateErrorBound;
    double m_etaPosRPROP;
    double m_etaNegRPROP;
    double m_minUpdateRPROP;
    double m_maxUpdateRPROP;
    bool m_enableRPROP;
    bool m_useBLASforTraining;
    string m_neuronsPerLayer;
    string m_weightFile;

};


#endif
