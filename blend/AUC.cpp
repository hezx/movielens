#include "AUC.h"

extern StreamOutput cout;

/**
 * Constructor
 */
AUC::AUC()
{
    //cout<<"Constructor AUC"<<endl;
}

/**
 * Destructor
 */
AUC::~AUC()
{
    //cout<<"Destructor AUC"<<endl;
}

/**
 * Direct port of a MATLAB script to C++ (provided by Isabelle Guyon from KDDCup2009)
 * 
 * @param prediction real valued predictions (real* pointer)
 * @param labels correct class labels (targets) (int* pointer)
 * @param nClass number of classes
 * @param nDomain number of domains
 * @param nLines number of samples
 * @return AUC value (0..1)
 */
REAL AUC::getAUC ( REAL* prediction, int* labels, int nClass, int nDomain, int nLines )
{
    if ( nClass != 2 )
        assert ( false );

    double avgArea = 0.0;

    for ( int d=0;d<nDomain;d++ )
    {
        // translate it to a score
        double* score = new double[nLines];
        int* index = new int[nLines];
        int neg = 0, pos = 0;
        for ( int i=0;i<nLines;i++ )
        {
            index[i] = i;
            //score[i] = prediction[d*2 + i*nDomain*2 + 1] - prediction[d*2 + i*nDomain*2 + 0];
            score[i] = prediction[i+d*2*nLines + nLines] - prediction[i+d*2*nLines];
            score[i] = -score[i];
            if ( labels[i*nDomain+d] == 1 )
                pos++;
            if ( labels[i*nDomain+d] == 0 )
                neg++;
        }

        // get pos and neg target index
        int* negIndex = new int[neg];
        int* posIndex = new int[pos];
        neg = 0;
        pos = 0;
        for ( int i=0;i<nLines;i++ )
        {
            if ( labels[i*nDomain+d] == 1 )
            {
                posIndex[pos] = i;
                pos++;
            }
            if ( labels[i*nDomain+d] == 0 )
            {
                negIndex[neg] = i;
                neg++;
            }
        }

        // sort scores
        ippsSortIndexAscend_64f_I ( score, index, nLines );
        //quickSort(score, index, 0, nLines-1);

        // translated from a MATLAB script...
        double oldval = score[0];
        double newval = score[0];
        double* R = new double[nLines+1];
        for ( int i=1;i<=nLines;i++ )
            R[i] = i;
        int k0 = 1;
        for ( int k=2;k<=nLines;k++ ) // for k=2:n
        {
            newval = score[k-1];  // newval=u(k);
            if ( newval == oldval ) //if newval==oldval
            {
                double v = R[k-1]* ( double ) ( k-k0 ) / ( double ) ( k-k0+1.0 ) +R[k]/ ( double ) ( k-k0+1.0 );

                for ( int j=k0;j<=k;j++ )
                {
                    R[j] = v;
                }
            }
            else
                k0 = k;
            oldval = newval;
        }
        double* S = new double[nLines];
        for ( int i=0;i<nLines;i++ )
            S[index[i]]=R[i+1];


        //SS=sort(S(negidx));
        double* SS = new double[neg];
        for ( int i=0;i<neg;i++ )
            SS[i] = S[negIndex[i]];
        //quickSort(SS,index,0,neg-1);
        ippsSortIndexAscend_64f_I ( SS, index, neg );

        //RR=[1:neg];
        double* RR = new double[neg+1];
        for ( int i=1;i<=neg;i++ )
            RR[i] = i;

        // SEN=(SS-RR)/pos;
        //area(kk)=sum(SEN)/neg;
        double area = 0.0;
        for ( int i=1;i<=neg;i++ )
            area += ( SS[i-1]-RR[i] ) /pos;
        area /= neg;

        if ( area <= 0.0 )
            assert ( false );
        if ( area >= 1.0 )
            assert ( false );
        if ( area < 0.5 )
            area = 1.0 - area;

        avgArea += area;

        if ( score )
            delete[] score;
        score = 0;
        if ( index )
            delete[] index;
        index = 0;
        if ( negIndex )
            delete[] negIndex;
        negIndex = 0;
        if ( posIndex )
            delete[] posIndex;
        posIndex = 0;
        if ( R )
            delete[] R;
        R = 0;
        if ( S )
            delete[] S;
        S = 0;
        if ( SS )
            delete[] SS;
        SS = 0;
        if ( RR )
            delete[] RR;
        RR = 0;
    }

    avgArea /= ( double ) nDomain;

    return -avgArea;  // we want to maximize the area
}
