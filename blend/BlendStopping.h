#ifndef __BLEND_STOPPING_
#define __BLEND_STOPPING_

#include "AutomaticParameterTuner.h"
#include "NumericalTools.h"
#include "Algorithm.h"
#include "Framework.h"
#include "ipps.h"
#include "AUC.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

/**
 * BlendStopping
 *
 * This class calculates the ensemble error (in the easiest case a linear combination)
 * Is used for determine the error from a set of Algorithms
 * The Algorithms are trained sequentially
 * The class can read a directory of full-predictions of the trainingset,
 * typically predicted with k-fold cross-validation (can also be predictions from out-of-bag estimates)
 *
 * The class solves a linear equation system in a least-squares sense with
 * simple pseudoinverse to get the linear combination of predictions
 *
 * Extention: k-fold cross validation of the blending
 * In order to determine the optimal ridge-regression regularizer lambda
 *
 * Extentions: nonNeg solver, parameter-search based combiner (nelder-mead for AUC optimization)
 *
 */

class BlendStopping : public Framework, public AutomaticParameterTuner, public AUC
{
public:
    BlendStopping();
    BlendStopping ( Algorithm *data );
    BlendStopping ( Algorithm *data, string algorithmFullPrediction );
    ~BlendStopping();

    // virtual overrider in AutomaticParameterTuner (for tuning lambda)
    double calcRMSEonProbe();
    double calcRMSEonBlend();
    void saveBestPrediction();

    // fill this with the new predictions per output class
    REAL** m_newPrediction;

    // calc rmse over all classes with pseudo-inverse
    double calcBlending ( char* fname = 0 );
    double getClassificationError();
    void setRegularization ( REAL reg );
    void clear();

    // for final prediction
    void saveTmpBestWeights();
    void saveBlendingWeights ( string path, bool saveBest = false );
    void loadBlendingWeights ( string path, int nPredictors );
    void predictEnsembleOutput ( REAL** predictions, REAL* output );
    void printWeights ( bool printBest = false );

protected:
    int m_nPredictors;
    int m_nClass;
    int m_nDomain;
    int m_nTrain;
    double m_blendingRegularization;
    REAL m_classificationError;
    REAL* m_singlePrediction;
    REAL* m_prediction;

    // pseudo inverse per class
    REAL** m_A;
    REAL** m_b;
    REAL** m_x;
    REAL** m_xBest;

    vector<string> m_usedFiles;
    Algorithm* m_data;
    NumericalTools m_numTools;
    string m_algorithmFullPrediction;
    bool m_globalWeights;

    // area under curve tuning parameters
    double* m_paramsAUC;
    bool m_optimizeRMSE;
};

#endif
