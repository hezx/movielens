#include "Algorithm.h"

extern StreamOutput cout;

/**
 * Constructor
 */
Algorithm::Algorithm()
{
    cout<<"Algorithm"<<endl;
    m_inRetraining = false;
    m_enableBagging = false;
}

/**
 * Desstructor
 */
Algorithm::~Algorithm()
{
    cout<<"descructor Algorithm"<<endl;
}

/**
 * Clip a value with low/high bounds
 *
 * @param input The input value for clipping
 * @param low The lower bound
 * @param high The higher bound
 * @return The clipped value
 *
 */
REAL Algorithm::clipValue ( REAL input, REAL low, REAL high )
{
    if ( input < low )
        input = low;
    if ( input > high )
        input = high;
    return input;
}
