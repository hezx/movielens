#include "BlendingNN.h"

extern StreamOutput cout;

/**
 * Constructor
 */
BlendingNN::BlendingNN()
{
    cout<<"BlendingNN"<<endl;

    // init member vars
    m_nn = 0;
    m_fullPredictionsReal = 0;
    m_inputsTrain = 0;
    m_inputsProbe = 0;
    m_meanBlend = 0;
    m_stdBlend = 0;
    m_predictionCache = 0;
    m_nPredictors = 0;
    m_epochs = 0;
    m_epochsBest = 0;
    m_minTuninigEpochs = 0;
    m_maxTuninigEpochs = 0;
    m_nrLayer = 0;
    m_batchSize = 0;
    m_offsetOutputs = 0;
    m_scaleOutputs = 0;
    m_initWeightFactor = 0;
    m_learnrate = 0;
    m_learnrateMinimum = 0;
    m_learnrateSubtractionValueAfterEverySample = 0;
    m_learnrateSubtractionValueAfterEveryEpoch = 0;
    m_momentum = 0;
    m_weightDecay = 0;
    m_minUpdateErrorBound = 0;
    m_etaPosRPROP = 0;
    m_etaNegRPROP = 0;
    m_minUpdateRPROP = 0;
    m_maxUpdateRPROP = 0;
    m_enableRPROP = 0;
    m_useBLASforTraining = 0;
}

/**
 * Destructor
 */
BlendingNN::~BlendingNN()
{
    cout<<"descructor BlendingNN"<<endl;

    for ( int i=0;i<m_usedFiles.size();i++ )
        delete[] m_fullPredictionsReal[i];
    delete[] m_fullPredictionsReal;

    for ( int i=0;i<m_nCross;i++ )
    {
        if ( m_inputsTrain )
        {
            if ( m_inputsTrain[i] )
                delete[] m_inputsTrain[i];
            m_inputsTrain[i] = 0;
        }
        if ( m_inputsProbe )
        {
            if ( m_inputsProbe[i] )
                delete[] m_inputsProbe[i];
            m_inputsProbe[i] = 0;
        }
    }
    if ( m_inputsTrain )
        delete[] m_inputsTrain;
    if ( m_inputsProbe )
        delete[] m_inputsProbe;
    if ( m_meanBlend )
        delete[] m_meanBlend;
    if ( m_stdBlend )
        delete[] m_stdBlend;

    for ( int i=0;i<m_nCross+1;i++ )
        if ( m_nn[i] )
            delete m_nn[i];
    if ( m_nn )
        delete[] m_nn;
    if ( m_predictionCache )
        delete[] m_predictionCache;
}

/**
 * Read the Algorithm specific values from the description file
 *
 */
void BlendingNN::readSpecificMaps()
{
    cout<<"Read specific maps"<<endl;

    // read dsc vars
    m_minTuninigEpochs = m_intMap["minTuninigEpochs"];
    m_maxTuninigEpochs = m_intMap["maxTuninigEpochs"];
    m_nrLayer = m_intMap["nrLayer"];
    m_batchSize = m_intMap["batchSize"];
    m_offsetOutputs = m_doubleMap["offsetOutputs"];
    m_scaleOutputs = m_doubleMap["scaleOutputs"];
    m_initWeightFactor = m_doubleMap["initWeightFactor"];
    m_learnrate = m_doubleMap["learnrate"];
    m_learnrateMinimum = m_doubleMap["learnrateMinimum"];
    m_learnrateSubtractionValueAfterEverySample = m_doubleMap["learnrateSubtractionValueAfterEverySample"];
    m_learnrateSubtractionValueAfterEveryEpoch = m_doubleMap["learnrateSubtractionValueAfterEveryEpoch"];
    m_momentum = m_doubleMap["momentum"];
    m_weightDecay = m_doubleMap["weightDecay"];
    m_minUpdateErrorBound = m_doubleMap["minUpdateErrorBound"];
    m_etaPosRPROP = m_doubleMap["etaPosRPROP"];
    m_etaNegRPROP = m_doubleMap["etaNegRPROP"];
    m_minUpdateRPROP = m_doubleMap["minUpdateRPROP"];
    m_maxUpdateRPROP = m_doubleMap["maxUpdateRPROP"];
    m_enableRPROP = m_boolMap["enableRPROP"];
    m_useBLASforTraining = m_boolMap["useBLASforTraining"];
    m_neuronsPerLayer = m_stringMap["neuronsPerLayer"];
    m_weightFile = m_stringMap["weightFile"];
}

/**
 * Initialize the blend networks
 */
void BlendingNN::init()
{
    cout<<"Init BlendingNN"<<endl<<endl;

    // read full predictions in directory
    string directory = m_datasetPath + "/" + m_fullPredPath + "/";
    vector<string> files = m_algorithmNameList; //Data::getDirectoryFileList(directory);
    m_nPredictors = 0;
    for ( int i=0;i<files.size();i++ )
    {
        if ( files[i].at ( files[i].size()-1 ) != '.' && files[i].find ( ".dat" ) == files[i].length()-4 )
        {
            //cout<<endl<<"File: "<<files[i]<<endl<<endl;
            m_usedFiles.push_back ( files[i] );
            m_nPredictors++;
        }
    }

    // allocte memory for fullPredictions
    m_fullPredictionsReal = new REAL*[m_nPredictors];

    // allocate memory for cross train sets
    m_inputsTrain = new REAL*[m_nCross+1];
    m_inputsProbe = new REAL*[m_nCross];
    for ( int i=0;i<m_nCross;i++ )
    {
        m_inputsTrain[i] = new REAL[m_trainSize[i]*m_nPredictors*m_nClass*m_nDomain];
        m_inputsProbe[i] = new REAL[m_probeSize[i]*m_nPredictors*m_nClass*m_nDomain];
    }

    // load the fullPredictors
    cout<<"Prediction files:"<<endl;
    for ( int i=0;i<m_usedFiles.size();i++ )
    {
        // allocate and read
        m_fullPredictionsReal[i] = new REAL[m_nTrain*m_nClass*m_nDomain];
        fstream f ( m_usedFiles[i].c_str(), ios::in );
        if ( f.is_open() == false )
            assert ( false );
        f.read ( ( char* ) m_fullPredictionsReal[i], sizeof ( REAL ) *m_nTrain*m_nClass*m_nDomain );
        f.close();

        // calc RMSE and classification error
        int* wrongLabelCnt = new int[m_nDomain];
        for ( int d=0;d<m_nDomain;d++ )
            wrongLabelCnt[d] = 0;
        double rmse = 0.0, err;
        for ( int k=0;k<m_nTrain;k++ )
        {
            for ( int j=0;j<m_nClass*m_nDomain;j++ )
            {
                err = m_fullPredictionsReal[i][k*m_nClass*m_nDomain + j] - m_trainTargetOrig[k*m_nClass*m_nDomain + j];
                rmse += err * err;
            }
        }
        if ( Framework::getDatasetType() ==true )
        {
            for ( int d=0;d<m_nDomain;d++ )
            {
                for ( int k=0;k<m_nTrain;k++ )
                {
                    REAL max = -1e10;
                    int ind = -1;
                    for ( int j=0;j<m_nClass;j++ )
                        if ( m_fullPredictionsReal[i][d*m_nClass+k*m_nClass*m_nDomain + j] > max )
                        {
                            max = m_fullPredictionsReal[i][d*m_nClass+k*m_nClass*m_nDomain + j];
                            ind = j;
                        }

                    if ( m_trainLabelOrig[k*m_nDomain + d] != ind )
                        wrongLabelCnt[d]++;
                }
            }

            int nWrong = 0;
            for ( int d=0;d<m_nDomain;d++ )
            {
                nWrong += wrongLabelCnt[d];
                if ( m_nDomain > 1 )
                    cout<<"["<< ( double ) wrongLabelCnt[d]/ ( double ) m_nTrain<<"] ";
            }
            cout<<" (classErr:"<<100.0* ( double ) nWrong/ ( ( double ) m_nTrain* ( double ) m_nDomain ) <<"%)";
        }
        delete[] wrongLabelCnt;
        cout<<"File:"<<m_usedFiles[i]<<"  RMSE:"<<sqrt ( rmse/ ( double ) ( m_nClass*m_nDomain*m_nTrain ) );
        cout<<endl;
    }
    cout<<endl;

    // mean and std
    m_meanBlend = new REAL[m_nPredictors*m_nClass*m_nDomain];
    m_stdBlend = new REAL[m_nPredictors*m_nClass*m_nDomain];
    for ( int i=0;i<m_nPredictors*m_nClass*m_nDomain;i++ )
    {
        m_meanBlend[i] = 0.0;
        m_stdBlend[i] = 0.0;
    }
    for ( int i=0;i<m_nTrain;i++ )
        for ( int j=0;j<m_nPredictors;j++ )
            for ( int k=0;k<m_nClass*m_nDomain;k++ )
                m_meanBlend[j*m_nClass*m_nDomain+k] += m_fullPredictionsReal[j][i*m_nClass*m_nDomain+k];
    for ( int i=0;i<m_nPredictors*m_nClass*m_nDomain;i++ )
        m_meanBlend[i] /= REAL ( m_nTrain );
    for ( int i=0;i<m_nTrain;i++ )
        for ( int j=0;j<m_nPredictors;j++ )
            for ( int k=0;k<m_nClass*m_nDomain;k++ )
            {
                REAL v = m_fullPredictionsReal[j][i*m_nClass*m_nDomain+k] - m_meanBlend[j*m_nClass*m_nDomain+k];
                m_stdBlend[j*m_nClass*m_nDomain+k] += v * v;
            }
    for ( int i=0;i<m_nPredictors*m_nClass*m_nDomain;i++ )
        m_stdBlend[i] = sqrt ( m_stdBlend[i]/REAL ( m_nTrain ) );

    REAL meanMin=1e10,meanMax=-1e10,stdMin=1e10,stdMax=-1e10;
    for ( int i=0;i<m_nPredictors*m_nClass*m_nDomain;i++ )
    {
        if ( m_meanBlend[i]<meanMin )
            meanMin = m_meanBlend[i];
        if ( m_meanBlend[i]>meanMax )
            meanMax = m_meanBlend[i];
        if ( m_stdBlend[i]<stdMin )
            stdMin = m_stdBlend[i];
        if ( m_stdBlend[i]>stdMax )
            stdMax = m_stdBlend[i];
    }
    cout<<"meanMin:"<<meanMin<<" meanMax:"<<meanMax<<" stdMin:"<<stdMin<<" stdMax:"<<stdMax<<endl;

    // apply to read fullpredictions
    for ( int i=0;i<m_nPredictors;i++ )
    {
        for ( int j=0;j<m_nTrain;j++ )
            for ( int k=0;k<m_nClass*m_nDomain;k++ )
                m_fullPredictionsReal[i][j*m_nClass*m_nDomain+k] = ( m_fullPredictionsReal[i][j*m_nClass*m_nDomain+k]-m_meanBlend[i*m_nClass*m_nDomain+k] ) /m_stdBlend[i*m_nClass*m_nDomain+k];
    }


    // copy data to cross-validation slots
    // the only difference here is new input data
    for ( int i=0;i<m_nCross;i++ )
    {
        // slot of probeset
        int begin = m_slotBoundaries[i];
        int end = m_slotBoundaries[i+1];

        int probeCnt = 0, trainCnt = 0;

        // go through whole trainOrig set
        for ( int j=0;j<m_nTrain;j++ )
        {
            int index = m_mixList[j];

            // probe set
            if ( j>=begin && j <end )
            {
                for ( int k=0;k<m_nPredictors;k++ )
                    for ( int l=0;l<m_nClass*m_nDomain;l++ )
                        m_inputsProbe[i][probeCnt* ( m_nPredictors*m_nClass*m_nDomain ) +k*m_nClass*m_nDomain+l] = m_fullPredictionsReal[k][index*m_nClass*m_nDomain+l];
                probeCnt++;
            }
            else  // train set
            {
                for ( int k=0;k<m_nPredictors;k++ )
                    for ( int l=0;l<m_nClass*m_nDomain;l++ )
                        m_inputsTrain[i][trainCnt* ( m_nPredictors*m_nClass*m_nDomain ) +k*m_nClass*m_nDomain+l] = m_fullPredictionsReal[k][index*m_nClass*m_nDomain+l];
                trainCnt++;
            }
        }
        if ( probeCnt != m_probeSize[i] || trainCnt != m_trainSize[i] ) // safety check
            assert ( false );
    }
    m_inputsTrain[m_nCross] = new REAL[m_nTrain*m_nPredictors*m_nClass*m_nDomain];
    for ( int i=0;i<m_nTrain;i++ )
        for ( int j=0;j<m_nPredictors;j++ )
            for ( int k=0;k<m_nClass*m_nDomain;k++ )
                m_inputsTrain[m_nCross][i* ( m_nPredictors*m_nClass*m_nDomain ) +j*m_nClass*m_nDomain+k] = m_fullPredictionsReal[j][i*m_nClass*m_nDomain+k];


    // construct neural networks as blender (cross fold validation)
    m_nn = new NN*[m_nCross+1];
    for ( int i=0;i<m_nCross+1;i++ )
        m_nn[i] = new NN();

    // create NNs
    for ( int i=0;i<m_nCross+1;i++ )
    {
        cout<<"Create a Neural Network ("<<i+1<<"/"<<m_nCross+1<<")"<<endl;
        m_nn[i] = new NN();
        m_nn[i]->setNrTargets ( m_nClass*m_nDomain );
        m_nn[i]->setNrInputs ( m_nPredictors*m_nClass*m_nDomain );
        m_nn[i]->setNrExamplesTrain ( i<m_nCross?m_trainSize[i]:m_nTrain );
        m_nn[i]->setNrExamplesProbe ( i<m_nCross?m_probeSize[i]:0 );
        m_nn[i]->setTrainInputs ( m_inputsTrain[i] );
        m_nn[i]->setTrainTargets ( i<m_nCross?m_trainTarget[i]:m_trainTargetOrig );
        m_nn[i]->setProbeInputs ( i<m_nCross?m_inputsProbe[i]:0 );
        m_nn[i]->setProbeTargets ( i<m_nCross?m_probeTarget[i]:0 );

        // learn parameters
        m_nn[i]->setInitWeightFactor ( m_initWeightFactor );
        m_nn[i]->setLearnrate ( m_learnrate );
        m_nn[i]->setLearnrateMinimum ( m_learnrateMinimum );
        m_nn[i]->setLearnrateSubtractionValueAfterEverySample ( m_learnrateSubtractionValueAfterEverySample );
        m_nn[i]->setLearnrateSubtractionValueAfterEveryEpoch ( m_learnrateSubtractionValueAfterEveryEpoch );
        m_nn[i]->setMomentum ( m_momentum );
        m_nn[i]->setWeightDecay ( m_weightDecay );
        m_nn[i]->setMinUpdateErrorBound ( m_minUpdateErrorBound );
        m_nn[i]->setBatchSize ( m_batchSize );
        m_nn[i]->setMaxEpochs ( m_maxTuninigEpochs );

        // set net inner stucture
        int nrLayer = m_nrLayer;
        int* neuronsPerLayer = Data::splitStringToIntegerList ( m_neuronsPerLayer, ',' );
        m_nn[i]->enableRPROP ( m_enableRPROP );
        m_nn[i]->setNNStructure ( nrLayer, neuronsPerLayer );
        m_nn[i]->setScaleOffset ( m_scaleOutputs, m_offsetOutputs );
        m_nn[i]->setRPROPPosNeg ( m_etaPosRPROP, m_etaNegRPROP );
        m_nn[i]->setRPROPMinMaxUpdate ( m_minUpdateRPROP, m_maxUpdateRPROP );
        m_nn[i]->setNormalTrainStopping ( true );
        m_nn[i]->useBLASforTraining ( m_useBLASforTraining );
        m_nn[i]->initNNWeights ( m_randSeed );
        delete neuronsPerLayer;

        cout<<endl<<endl;
    }

    // prediction of train set
    m_predictionCache = new REAL[m_nTrain*m_nClass*m_nDomain];
}

/**
 * Calculate the current error
 *
 * @return rmse
 */
double BlendingNN::calcRMSEonProbe()
{
    for ( int i=0;i<m_nCross;i++ )
    {
        cout<<"."<<flush;

        // one gradient descent step (one epoch)
        m_nn[i]->trainOneEpoch();

        // predict the probe samples
        for ( int j=0;j<m_probeSize[i];j++ )
            m_nn[i]->predictSingleInput ( m_inputsProbe[i] + j*m_nPredictors*m_nClass*m_nDomain, m_predictionCache + m_probeIndex[i][j]*m_nClass*m_nDomain );
    }
    /*REAL* tmp = new REAL[m_nPredictors*m_nClass];
    for(int i=0;i<m_nTrain;i++)
    {
        for(int j=0;j<m_nPredictors;j++)
            for(int k=0;k<m_nClass;k++)
                tmp[j*m_nClass+k] = m_fullPredictionsReal[j][i*m_nClass + k];
        m_nn[m_nCross]->predictSingleInput(tmp, m_predictionCache+i*m_nClass);
    }
    delete[] tmp;*/

    // calc RMSE
    int* wrongLabelCnt = new int[m_nDomain];
    for ( int d=0;d<m_nDomain;d++ )
        wrongLabelCnt[d] = 0;
    double rmse = 0.0, err;
    for ( int i=0;i<m_nTrain;i++ )
    {
        for ( int j=0;j<m_nClass*m_nDomain;j++ )
        {
            REAL v = m_predictionCache[i*m_nClass*m_nDomain+j];
            if ( m_enablePostBlendClipping )
                v = NumericalTools::clipValue ( m_predictionCache[i*m_nClass*m_nDomain+j], m_negativeTarget, m_positiveTarget );
            err = v - m_trainTargetOrig[i*m_nClass*m_nDomain+j];
            err = m_predictionCache[i*m_nClass*m_nDomain+j] - m_trainTargetOrig[i*m_nClass*m_nDomain+j];
            rmse += err * err;
        }
    }
    if ( Framework::getDatasetType() ==true )
    {
        for ( int d=0;d<m_nDomain;d++ )
        {
            for ( int i=0;i<m_nTrain;i++ )
            {
                REAL max = -1e10;
                int ind = -1;
                for ( int j=0;j<m_nClass;j++ )
                    if ( m_predictionCache[d*m_nClass+i*m_nDomain*m_nClass+j] > max )
                    {
                        max = m_predictionCache[d*m_nClass+i*m_nDomain*m_nClass+j];
                        ind = j;
                    }
                if ( ind != m_trainLabelOrig[d + i*m_nDomain] )
                    wrongLabelCnt[d]++;
            }
        }
        int nWrong = 0;
        for ( int d=0;d<m_nDomain;d++ )
        {
            nWrong += wrongLabelCnt[d];
            if ( m_nDomain > 1 )
                cout<<"["<< ( double ) wrongLabelCnt[d]/ ( double ) m_nTrain<<"] ";
        }
        cout<<" (classError:"<<100.0* ( double ) nWrong/ ( ( double ) m_nTrain* ( double ) m_nDomain ) <<"%)";
    }
    delete[] wrongLabelCnt;
    rmse = sqrt ( rmse/ ( double ) ( m_nTrain*m_nClass*m_nDomain ) );

    return rmse;
}

/**
 * Reimplementation of the virtual method
 * Not needed here
 *
 * @return 0.0
 */
double BlendingNN::calcRMSEonBlend()
{
    return 0.0;
}

/**
 * Save the epochs, where the error is minimal
 *
 */
void BlendingNN::saveBestPrediction()
{
    m_epochsBest = m_epochs;
    cout<<"!";
}

/**
 * Start the training process
 * Training goal is mix current train predictions
 */
void BlendingNN::train()
{
    cout<<"Start train blending NN"<<endl;

    m_epochs = 0;
    addEpochParameter ( &m_epochs, "epoch" );

    cout<<"(min|max. epochs: "<<m_minTuninigEpochs<<"|"<<m_maxTuninigEpochs<<")"<<endl;
    expSearcher ( m_minTuninigEpochs, m_maxTuninigEpochs, 3, 1, 0.8, true, false );

    // do retraining
    m_nn[m_nCross]->setNormalTrainStopping ( false );
    m_nn[m_nCross]->setMaxEpochs ( m_epochsBest );
    int epochs = m_nn[m_nCross]->trainNN();

    // Save weights
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile;
    cout<<"Save:"<<name<<endl;
    int n = m_nn[m_nCross]->getNrWeights();
    cout<<"#weights:"<<n<<endl;
    REAL* w = m_nn[m_nCross]->getWeightPtr();

    fstream f ( name.c_str(), ios::out );
    f.write ( ( char* ) &m_nPredictors, sizeof ( int ) );
    f.write ( ( char* ) &m_nClass, sizeof ( int ) );
    f.write ( ( char* ) &m_nDomain, sizeof ( int ) );
    f.write ( ( char* ) &n, sizeof ( int ) );
    f.write ( ( char* ) w, sizeof ( REAL ) *n );
    f.write ( ( char* ) m_meanBlend, sizeof ( REAL ) *m_nPredictors*m_nClass*m_nDomain );
    f.write ( ( char* ) m_stdBlend, sizeof ( REAL ) *m_nPredictors*m_nClass*m_nDomain );
    f.close();
}

/**
 * Load saved weights, go into ready-to-predict mode
 */
void BlendingNN::loadWeights()
{
    cout<<"Load weights"<<endl;

    // load weights
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile;
    cout<<"Load:"<<name<<endl;
    int n = 0;

    // open
    fstream f ( name.c_str(), ios::in );
    if ( f.is_open() == false )
        assert ( false );

    // read #predictions
    f.read ( ( char* ) &m_nPredictors, sizeof ( int ) );

    // read #class
    f.read ( ( char* ) &m_nClass, sizeof ( int ) );

    // read #domain
    f.read ( ( char* ) &m_nDomain, sizeof ( int ) );

    // set up NNs (only the last one is used)
    m_nn = new NN*[m_nCross+1];
    for ( int i=0;i<m_nCross+1;i++ )
        m_nn[i] = 0;
    m_nn[m_nCross] = new NN();
    m_nn[m_nCross]->setNrTargets ( m_nClass*m_nDomain );
    m_nn[m_nCross]->setNrInputs ( m_nPredictors*m_nClass*m_nDomain );
    m_nn[m_nCross]->setNrExamplesTrain ( 0 );
    m_nn[m_nCross]->setNrExamplesProbe ( 0 );
    m_nn[m_nCross]->setTrainInputs ( 0 );
    m_nn[m_nCross]->setTrainTargets ( 0 );
    m_nn[m_nCross]->setProbeInputs ( 0 );
    m_nn[m_nCross]->setProbeTargets ( 0 );

    // learn parameters
    m_nn[m_nCross]->setInitWeightFactor ( m_initWeightFactor );
    m_nn[m_nCross]->setLearnrate ( m_learnrate );
    m_nn[m_nCross]->setLearnrateMinimum ( m_learnrateMinimum );
    m_nn[m_nCross]->setLearnrateSubtractionValueAfterEverySample ( m_learnrateSubtractionValueAfterEverySample );
    m_nn[m_nCross]->setLearnrateSubtractionValueAfterEveryEpoch ( m_learnrateSubtractionValueAfterEveryEpoch );
    m_nn[m_nCross]->setMomentum ( m_momentum );
    m_nn[m_nCross]->setWeightDecay ( m_weightDecay );
    m_nn[m_nCross]->setMinUpdateErrorBound ( m_minUpdateErrorBound );
    m_nn[m_nCross]->setBatchSize ( m_batchSize );
    m_nn[m_nCross]->setMaxEpochs ( m_maxTuninigEpochs );

    // set net inner stucture
    int nrLayer = m_nrLayer;
    int* neuronsPerLayer = Data::splitStringToIntegerList ( m_neuronsPerLayer, ',' );
    m_nn[m_nCross]->setNNStructure ( nrLayer, neuronsPerLayer );

    m_nn[m_nCross]->setRPROPPosNeg ( m_etaPosRPROP, m_etaNegRPROP );
    m_nn[m_nCross]->setRPROPMinMaxUpdate ( m_minUpdateRPROP, m_maxUpdateRPROP );
    m_nn[m_nCross]->setScaleOffset ( m_scaleOutputs, m_offsetOutputs );
    m_nn[m_nCross]->setNormalTrainStopping ( true );
    m_nn[m_nCross]->enableRPROP ( m_enableRPROP );
    m_nn[m_nCross]->useBLASforTraining ( m_useBLASforTraining );
    m_nn[m_nCross]->initNNWeights ( m_randSeed );
    delete[] neuronsPerLayer;

    // #weights
    f.read ( ( char* ) &n, sizeof ( int ) );

    REAL* w = new REAL[n];

    // read weights
    f.read ( ( char* ) w, sizeof ( REAL ) *n );

    // read mean, std
    m_meanBlend = new REAL[m_nPredictors*m_nClass*m_nDomain];
    m_stdBlend = new REAL[m_nPredictors*m_nClass*m_nDomain];
    f.read ( ( char* ) m_meanBlend, sizeof ( REAL ) *m_nPredictors*m_nClass*m_nDomain );
    f.read ( ( char* ) m_stdBlend, sizeof ( REAL ) *m_nPredictors*m_nClass*m_nDomain );

    f.close();

    // init the NN weights
    m_nn[m_nCross]->setWeights ( w );

    if ( w )
        delete[] w;
    w = 0;
}

/**
 * Make predictions, based on the current blending neural net (including normalization)
 *
 * @param predictions pointer to pointers of predictions
 * @param output the output (return value)
 */
void BlendingNN::predictEnsembleOutput ( REAL** predictions, REAL* output )
{
    REAL* tmp = new REAL[m_nPredictors*m_nClass*m_nDomain];
    for ( int i=0;i<m_nPredictors;i++ )
        for ( int j=0;j<m_nClass*m_nDomain;j++ )
            tmp[i*m_nClass*m_nDomain+j] = ( predictions[i+1][j] - m_meanBlend[i*m_nClass*m_nDomain+j] ) / m_stdBlend[i*m_nClass*m_nDomain+j];  // +1 because the first is constant
    m_nn[m_nCross]->predictSingleInput ( tmp, output );
    if ( m_enablePostBlendClipping )
        for ( int i=0;i<m_nClass*m_nDomain;i++ )
            output[i] = NumericalTools::clipValue ( output[i], m_negativeTarget, m_positiveTarget );
    if ( tmp )
        delete[] tmp;
    tmp = 0;
}
