#ifndef __POLYNOMIAL_REGRESSION_
#define __POLYNOMIAL_REGRESSION_

#include "StandardAlgorithm.h"
#include "Framework.h"

/**
 * Polynomial prediction model
 * This is nothing more than linear regression with an extention of the feature space
 * Cross interactions enable interaction between input features, but increase the
 * dimensionality with O(n^2). This works for a few input features.
 *
 * Normalization is done internally of the extended feature space
 *
 * Tunable parameters are the regularization constant.
 *
 */

class PolynomialRegression : public StandardAlgorithm, public Framework
{
public:
    PolynomialRegression();
    ~PolynomialRegression();

    virtual void modelInit();
    virtual void modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun );
    virtual void predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun );
    virtual void readSpecificMaps();
    virtual void saveWeights ( int cross );
    virtual void loadWeights ( int cross );
    virtual void loadMetaWeights ( int cross );

    static string templateGenerator ( int id, string preEffect, int nameID, bool blendStop );

private:
    REAL power ( REAL x, int e );

    // solution weights
    REAL** m_x;
    double m_reg;
    int m_inputDim;

    // trainset with constant input column 1
    //REAL* m_crossTrain;

    NumericalTools solver;

    REAL* m_polyMean;
    REAL* m_polyStd;

    // dsc file
    int m_polyOrder;
    bool m_enableCrossInteractions;
    bool m_enableInternalNormalization;
};


#endif
