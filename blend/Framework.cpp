#include "Framework.h"

extern StreamOutput cout;

bool Framework::m_isClassificationDataset;
int Framework::m_maxThreads;
int Framework::m_randomSeed;
int Framework::m_frameworkMode;
int Framework::m_additionalStartupParameter;
int Framework::m_gradientBoostingEpoch;
int Framework::m_gradientBoostingMaxEpochs;
int Framework::m_id;
string Framework::m_validationTypeFramework;

/**
 * Constructor
 */
Framework::Framework()
{
    //cout<<"Framework"<<endl;
    m_id = 0;
}

/**
 * Constructor
 */
Framework::~Framework()
{
    //cout<<"descructor Framework"<<endl;
}

/**
 * Set the dataset type
 *
 * @param isClassification True: the dataset is a classification dataset
 */
void Framework::setDatasetType ( bool isClassification )
{
    m_isClassificationDataset = isClassification;
    cout<<"isClassificationDataset: "<<m_isClassificationDataset<<endl;
}

/**
 * Returns the stored type
 *
 * @return The dataset type (bool)
 */
bool Framework::getDatasetType()
{
    return m_isClassificationDataset;
}

/**
 * Set max threads
 *
 * @param n Max. threads for OPENMP
 */
void Framework::setMaxThreads ( int n )
{
    m_maxThreads = n;
    omp_set_num_threads ( m_maxThreads );  // set this value for OPENMP pragmas
    cout<<"maxThreads(OPENMP): "<<m_maxThreads<<endl;
}

/**
 * Return max threads
 *
 * @return Max. threads
 */
int Framework::getMaxThreads()
{
    return m_maxThreads;
}

/**
 * Set random seed
 *
 * @param s Random Seed
 */
void Framework::setRandomSeed ( int s )
{
    m_randomSeed = s;
    cout<<"randomSeed: "<<m_randomSeed<<endl;
}

/**
 * Return random seed
 *
 * @return Random Seed
 */
int Framework::getRandomSeed()
{
    return m_randomSeed;
}

/**
 * Set Framework mode
 * train(0), predict(1), blend(2)
 *
 * @param mode The mode
 */
void Framework::setFrameworkMode ( int mode )
{
    m_frameworkMode = mode;
    cout<<"frameworkMode: "<<m_frameworkMode<<endl;
}

/**
 * Return Framework mode
 *
 * @return the mode: train(0), predict(1), blend(2)
 */
int Framework::getFrameworkMode()
{
    return m_frameworkMode;
}

/**
 * Set additional run parameter on startup
 *
 * @param s Random Seed
 */
void Framework::setAdditionalStartupParameter ( char* s )
{
    int n = -1;
    sscanf ( s,"%d",&n );
    m_additionalStartupParameter = n;
    cout<<"additionalStartupParameter: "<<m_additionalStartupParameter<<endl;
}

/**
 * Return additional run parameter on startup
 *
 * @return Random Seed
 */
int Framework::getAdditionalStartupParameter()
{
    return m_additionalStartupParameter;
}

/**
 * Convert a date to a unix time_t stamp
 *
 * @param day The day: 1..31
 * @param month The month: 1..12
 * @param year The year
 * @param hour The hour: 0..23
 * @param minute The minute: 0..59
 * @param second The second: 0..59
 * @return Unix time stamp time_t
 */
uint Framework::convertDateToInt(uint day, uint month, uint year, uint hour, uint minute, uint second)
{
    struct tm t;
    t.tm_year = year - 1900;
    t.tm_mon = month - 1;
    t.tm_mday = day;
    t.tm_hour = hour;
    t.tm_min = minute;
    t.tm_sec = second;
    t.tm_isdst = 0;
    time_t tt = mktime(&t);
    
    time_t secondsFromNow = time(0);
    tm* t0 = gmtime(&secondsFromNow);
    
    if(t0->tm_isdst == 0)
        tt += 3600;
    
    return tt;
}

/**
 * Convert the unix time stamp back to individual values.
 * 
 * @param date Input: unix time stamp
 * @param day Reference value, return parameter
 * @param month Reference value, return parameter
 * @param year Reference value, return parameter
 * @param hour Reference value, return parameter
 * @param minute Reference value, return parameter
 * @param second Reference value, return parameter
 */
void Framework::convertIntToDate(uint date, uint &day, uint &month, uint &year, uint &hour, uint &minute, uint &second, uint &weekday)
{
    time_t seconds = date;
    struct tm t = *gmtime(&seconds);
    day = t.tm_mday;
    month = t.tm_mon + 1;
    year = t.tm_year + 1900;
    hour = t.tm_hour;
    minute = t.tm_min;
    second = t.tm_sec;
    weekday = t.tm_wday; // 0..6  days since Sunday
}

/**
 * Set the current epoch in gradient boosting
 *
 * @param epoch 
 */
void Framework::setGradientBoostingEpoch( int epoch )
{
    m_gradientBoostingEpoch = epoch;
    //cout<<"gradientBoostingEpoch:"<<m_gradientBoostingEpoch<<endl;
}

/**
 * Set the max epochs in gradient boosting
 *
 * @param epoch 
 */
void Framework::setGradientBoostingMaxEpochs( int epochs )
{
    m_gradientBoostingMaxEpochs = epochs;
    cout<<"gradientBoostingMaxEpochs:"<<m_gradientBoostingMaxEpochs<<endl;
}

/**
 * Get the current epoch in gradient boosting
 *
 * @return 
 */
int Framework::getGradientBoostingEpoch()
{
    return m_gradientBoostingEpoch;
}

/**
 * Get total epochs in gradient boosting
 *
 * @return 
 */
int Framework::getGradientBoostingMaxEpochs()
{
    return m_gradientBoostingMaxEpochs;
}

/**
 * Get the current epoch in gradient boosting string
 *
 * @return 
 */
string Framework::getGradientBoostingEpochString()
{
    if(m_gradientBoostingMaxEpochs < 2)
        return string();
    char buf[1024];
    sprintf(buf,".gb%04d",m_gradientBoostingEpoch);
    return string(buf);
}

/**
 * Set the id
 *
 * @param id 
 */
void Framework::setFrameworkID(int id)
{
    m_id = id;
}

/**
 * Get the id
 *
 * @return 
 */
int Framework::getFramworkID()
{
    return m_id;
}

void Framework::setValidationType ( string s )
{
    m_validationTypeFramework = s;
}

string Framework::getValidationType()
{
    return m_validationTypeFramework;
}
