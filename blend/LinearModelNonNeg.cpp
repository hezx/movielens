#include "LinearModelNonNeg.h"

extern StreamOutput cout;

/**
 * Constructor
 */
LinearModelNonNeg::LinearModelNonNeg()
{
    cout<<"LinearModelNonNeg"<<endl;
    // init member vars
    m_x = 0;
    m_reg = 0;
    m_scale = 0;
    m_bias = 0;
    m_maxNonNegSolverIterations = 0;
    m_initReg = 0;
    m_initScale = 0;
    m_initBias = 0;
}

/**
 * Destructor
 */
LinearModelNonNeg::~LinearModelNonNeg()
{
    cout<<"descructor LinearModelNonNeg"<<endl;
    for ( int i=0;i<m_nCross+1;i++ )
    {
        if ( m_x )
        {
            if ( m_x[i] )
                delete[] m_x[i];
            m_x[i] = 0;
        }
    }
    if ( m_x )
        delete[] m_x;
    m_x = 0;
}

/**
 * Read the Algorithm specific values from the description file
 *
 */
void LinearModelNonNeg::readSpecificMaps()
{
    m_initReg = m_doubleMap["initReg"];
    m_initScale = m_doubleMap["initScale"];
    m_initBias = m_doubleMap["initBias"];

    m_maxNonNegSolverIterations = m_intMap["maxNonNegSolverIterations"];
}

/**
 * Init the Linear Model
 *
 */
void LinearModelNonNeg::modelInit()
{
    // add the tunable parameter
    m_reg = m_initReg;
    paramDoubleValues.push_back ( &m_reg );
    paramDoubleNames.push_back ( "reg" );

    m_bias = m_initBias;
    paramDoubleValues.push_back ( &m_bias );
    paramDoubleNames.push_back ( "bias" );

    m_scale = m_initScale;
    paramDoubleValues.push_back ( &m_scale );
    paramDoubleNames.push_back ( "scale" );

    // alloc mem for weights
    if ( m_x == 0 )
    {
        m_x = new REAL*[m_nCross+1];
        for ( int i=0;i<m_nCross+1;i++ )
            m_x[i] = new REAL[m_nFeatures * m_nClass * m_nDomain];
    }
}

/**
 * Prediction for outside use, predicts outputs based on raw input values
 *
 * @param rawInputs The input feature, without normalization (raw)
 * @param outputs The output value (prediction of target)
 * @param nSamples The input size (number of rows)
 * @param crossRun Number of cross validation run (in training)
 */
void LinearModelNonNeg::predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun )
{
    for ( int i=0;i<nSamples;i++ )
    {
        for ( int j=0;j<m_nClass*m_nDomain;j++ )
        {
            REAL sum = 0.0;
            for ( int k=0;k<m_nFeatures;k++ )
            {
                REAL x = rawInputs[i*m_nFeatures + k];
                sum += x * m_x[crossRun][k*m_nClass*m_nDomain + j];
            }
            outputs[i*m_nClass*m_nDomain + j] = ( sum - m_bias ) / m_scale;
        }
    }
}

/**
 * Make a model update, set the new cross validation set or set the whole training set
 * for retraining
 *
 * @param input Pointer to input (can be cross validation set, or whole training set) (#rows x nFeatures) [col0;col1..]
 * @param target The targets (can be cross validation targets), [row0;row1..]
 * @param nSamples The sample size (#rows) in input
 * @param crossRun The cross validation run (for training)
 */
void LinearModelNonNeg::modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun )
{
    REAL* tmpTargets = new REAL[nSamples*m_nClass*m_nDomain];
    REAL* x = new REAL[m_nFeatures];

    // set targets
    for ( int i=0;i<m_nClass*m_nDomain;i++ )
        for ( int j=0;j<nSamples;j++ )
            tmpTargets[i * nSamples + j] = target[j * m_nClass * m_nDomain + i] * m_scale + m_bias;

    // solve the linear systems (per output class)
    for ( int i=0;i<m_nClass*m_nDomain;i++ )
    {
        int nIter = solver.RidgeRegressionNonNegSinglecall ( input, tmpTargets+i*nSamples, x, nSamples, m_nFeatures, m_reg, 1e-10, m_maxNonNegSolverIterations );

        for ( int j=0;j<m_nFeatures;j++ )
            m_x[crossRun][j*m_nClass*m_nDomain+i] = x[j];
    }

    if ( tmpTargets )
        delete[] tmpTargets;
    tmpTargets = 0;
    if ( x )
        delete[] x;
    x = 0;
}

/**
 * Save the weights and all other parameters for load the complete prediction model
 *
 */
void LinearModelNonNeg::saveWeights ( int cross )
{
    char buf[1024];
    sprintf ( buf,"%03d",cross );
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile + "." + buf + Framework::getGradientBoostingEpochString();
    if ( m_inRetraining )
        cout<<"Save:"<<name<<endl;
    fstream f ( name.c_str(), ios::out );
    f.write ( ( char* ) &m_nTrain, sizeof ( int ) );
    f.write ( ( char* ) &m_nFeatures, sizeof ( int ) );
    f.write ( ( char* ) &m_nClass, sizeof ( int ) );
    f.write ( ( char* ) &m_nDomain, sizeof ( int ) );
    f.write ( ( char* ) m_x[cross], sizeof ( REAL ) *m_nFeatures*m_nClass*m_nDomain );
    f.write ( ( char* ) &m_maxSwing, sizeof ( double ) );
    f.write ( ( char* ) &m_reg, sizeof ( double ) );
    f.write ( ( char* ) &m_bias, sizeof ( double ) );
    f.write ( ( char* ) &m_scale, sizeof ( double ) );
    f.close();
}

/**
 * Load the weights and all other parameters and make the model ready to predict
 *
 */
void LinearModelNonNeg::loadWeights ( int cross )
{
    char buf[1024];
    sprintf ( buf,"%03d",cross );
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile + "." + buf + Framework::getGradientBoostingEpochString();
    cout<<"Load:"<<name<<endl;
    fstream f ( name.c_str(), ios::in );
    if ( f.is_open() == false )
        assert ( false );
    f.read ( ( char* ) &m_nTrain, sizeof ( int ) );
    f.read ( ( char* ) &m_nFeatures, sizeof ( int ) );
    f.read ( ( char* ) &m_nClass, sizeof ( int ) );
    f.read ( ( char* ) &m_nDomain, sizeof ( int ) );

    m_x = new REAL*[m_nCross+1];
    for ( int i=0;i<m_nCross+1;i++ )
        m_x[i] = 0;
    m_x[cross] = new REAL[m_nFeatures * m_nClass * m_nDomain];

    f.read ( ( char* ) m_x[cross], sizeof ( REAL ) *m_nFeatures*m_nClass*m_nDomain );
    f.read ( ( char* ) &m_maxSwing, sizeof ( double ) );
    f.read ( ( char* ) &m_reg, sizeof ( double ) );
    f.read ( ( char* ) &m_bias, sizeof ( double ) );
    f.read ( ( char* ) &m_scale, sizeof ( double ) );
    f.close();
}

/**
 *
 */
void LinearModelNonNeg::loadMetaWeights ( int cross )
{
    char buf[1024];
    sprintf ( buf,"%03d",cross );
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile + "." + buf + Framework::getGradientBoostingEpochString();
    cout<<"LoadMeta:"<<name<<endl;
    fstream f ( name.c_str(), ios::in );
    if ( f.is_open() == false )
        assert ( false );
    f.read ( ( char* ) &m_nTrain, sizeof ( int ) );
    f.read ( ( char* ) &m_nFeatures, sizeof ( int ) );
    f.read ( ( char* ) &m_nClass, sizeof ( int ) );
    f.read ( ( char* ) &m_nDomain, sizeof ( int ) );

    REAL* tmp = new REAL[m_nFeatures*m_nClass*m_nDomain];
    f.read ( ( char* ) tmp, sizeof ( REAL ) *m_nFeatures*m_nClass*m_nDomain );
    delete[] tmp;
    f.read ( ( char* ) &m_maxSwing, sizeof ( double ) );
    f.read ( ( char* ) &m_reg, sizeof ( double ) );
    f.read ( ( char* ) &m_bias, sizeof ( double ) );
    f.read ( ( char* ) &m_scale, sizeof ( double ) );
    f.close();
}

/**
 * Generates a template of the description file
 *
 * @return The template string
 */
string LinearModelNonNeg::templateGenerator ( int id, string preEffect, int nameID, bool blendStop )
{
    stringstream s;
    s<<"ALGORITHM=LinearModelNonNeg"<<endl;
    s<<"ID="<<id<<endl;
    s<<"TRAIN_ON_FULLPREDICTOR="<<preEffect<<endl;
    s<<"DISABLE=0"<<endl;
    s<<endl;
    s<<"[int]"<<endl;
    s<<"maxTuninigEpochs=20"<<endl;
    s<<"maxNonNegSolverIterations=1000"<<endl;
    s<<endl;
    s<<"[double]"<<endl;
    s<<"initMaxSwing=0.5"<<endl;
    s<<"initReg=0.01"<<endl;
    s<<"initScale=1.0"<<endl;
    s<<"initBias=3.0"<<endl;
    s<<endl;
    s<<"[bool]"<<endl;
    s<<"enableClipping="<< ( !blendStop ) <<endl;
    s<<"enableTuneSwing=0"<<endl;
    s<<endl;
    s<<"minimzeProbe=0"<<endl;
    s<<"minimzeProbeClassificationError=0"<<endl;
    s<<"minimzeBlend="<<blendStop<<endl;
    s<<"minimzeBlendClassificationError=0"<<endl;
    s<<endl;
    s<<"[string]"<<endl;
    s<<"weightFile="<<"LinearModelNonNeg_"<<nameID<<"_weights.dat"<<endl;
    s<<"fullPrediction=LinearModelNonNeg_"<<nameID<<".dat"<<endl;

    return s.str();
}
