#ifndef __LINEAR_MODEL_
#define __LINEAR_MODEL_

#include "StandardAlgorithm.h"
#include "Framework.h"

/**
 * Linear prediction model
 * Based on simple linear regression on targets
 *
 * Tunable parameters are the regularization constant
 * It offers the possibility to tune the regularizer per feature
 *
 */

class LinearModel : public StandardAlgorithm, public Framework
{
public:
    LinearModel();
    ~LinearModel();

    virtual void modelInit();
    virtual void modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun );
    virtual void predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun );
    virtual void readSpecificMaps();
    virtual void saveWeights ( int cross );
    virtual void loadWeights ( int cross );
    virtual void loadMetaWeights ( int cross );

    static string templateGenerator ( int id, string preEffect, int nameID, bool blendStop );

private:

    // solution weights (per cross validation set)
    REAL** m_x;
    double m_reg;
    bool m_tuneRigeModifiers;

    NumericalTools solver;
    double* m_ridgeModifiers;
};


#endif
