#ifndef _ALGORITHM_H__
#define _ALGORITHM_H__

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
//#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <vector>
#include <map>
#include <dirent.h>
#include <algorithm>
#include <sstream>

using namespace std;

#include "StreamOutput.h"
#include "Data.h"
#include "Framework.h"

/**
 * This is the base class for all algorithms
 * Data is filled externally with the setDataPointers method from a Data* object
 * An algorithm needs to implement the virtual methods
 *
 */

class Algorithm : public Framework, public Data
{
    friend class BlendStopping;

public:
    Algorithm();
    virtual ~Algorithm();

    REAL clipValue ( REAL input, REAL low, REAL high );
    
    // these methods need to be implemented in the specific algorithm
    virtual double calcRMSEonProbe() = 0;
    virtual double calcRMSEonBlend() = 0;
    virtual double train() = 0;
    virtual void setPredictionMode ( int cross ) = 0;
    virtual void predictMultipleOutputs ( REAL* rawInput, REAL* effect, REAL* output, int* label, int nSamples, int crossRun ) = 0;

    // indicates the retraining status (with all data)
    bool m_inRetraining;

};


#endif
