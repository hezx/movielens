#include <stdio.h>
#include <stdlib.h>
//#include <iostream>
#include <fstream>
#include <string.h>
#include <string>
#include <math.h>
#include <vector>
#include <deque>

#include "config.h"
#include "Data.h"
#include "Scheduler.h"
#include "StreamOutput.h"
#include "AlgorithmExploration.h"
#include "NumericalTools.h"

#include <sys/sysinfo.h>
#include <sys/utsname.h>

/**
 *
 * \mainpage ELF - "Ensemble Learning Framework"
 * 
 * \section Introduction
 *
 * This piece of software, written in <a href="http://en.wikipedia.org/wiki/C%2B%2B" target="_blank">C++</a>, is constructed being a stand-alone <b>supervised machine learning</b> framework. ELF is able to solve regression as well as classification problems. Our goal is doing this as accurate and as fast as possible. Recently, ensemble learning turns out to be very popular. The ELF supports ensembling in some ways. <br><br>The optimization target can be the RMSE, MAE, AUC or the classification error. The ELF supports multi-class classification problems and multi-domain classification. Multi-domain means that we have more than one label per example. The ELF has well implemented base learners. The ensemble functionality of the framework is realized with stacking, cascade learning and residual training. Stacking is a simple linear combination. ELF has the opportunity of cascade learning, which is an extention of the features with predictions from other models. Parameter selection can be done with cross-validation or bagging. Parameter selection means searching for good metaparameters in order to control overfitting of each individual model.
 *
 * \section Features
 *
 * <ul>
 *  <li>The Framework</li>
 *    <ul>
 *      <li>Features and targets are stored as matrices (no sparse features possible!)</li>
 *      <li>The size of the feature matrix is limited to 2<sup>32</sup> elements</li>
 *      <li>Floating point precision can be single (4Byte) or double (8Byte)</li>
 *      <li>Training speed is a main design goal</li>
 *      <li>Accuracy is a main design goal</li>
 *      <li>Many learners use Intel's performance libraries: <a href=http://software.intel.com/en-us/articles/non-commercial-software-download/ target="_blank">IPP</a> and <a href=http://software.intel.com/en-us/articles/non-commercial-software-download/ target="_blank">MKL</a> (both come with Intel C++ compiler package)</li>
 *    </ul>
 *  <li>Regression</li>
 *    <ul>
 *      <li>All learners solve a regression problem</li>
 *      <li>Any classification problem is transferd to a regression problem!</li>
 *      <li><a href=http://en.wikipedia.org/wiki/Root_mean_square_deviation target="_blank">RMSE</a> or <a href=http://en.wikipedia.org/wiki/Mean_absolute_error target="_blank">MAE</a> as optimization target</li>
 *    </ul>
 *  <li>Classification</li>
 *    <ul>
 *      <li><a href=http://en.wikipedia.org/wiki/Root_mean_square_deviation target="_blank">RMSE</a> or <a href=http://en.wikipedia.org/wiki/Receiver_operating_characteristic target="_blank">AUC</a> (2-class) or classification error as optimization target</li>
 *      <li>Multi-class classification (&gt;2 classes)</li>
 *      <li>Multi-domain classification (&gt;1 labels)</li>
 *    </ul>
 *  <li>Parameter selection - control overfitting</li>
 *    <ul>
 *      <li>k-fold Cross Validation</li>
 *      <li>Bagging (out-of-bag estimate)</li>
 *      <li>Cross Validation and Bagging are parallelized via <a href=http://en.wikipedia.org/wiki/OpenMP target="_blank">OpenMP</a></li>
 *    </ul>
 *  <li>Base Learners</li>
 *    <ul>
 *      <li>Linear Regression</li>
 *      <li>Polynomial Regression</li>
 *      <li>K-Nearest Neighbors</li>
 *      <li>Neural Networks</li>
 *      <li>Gradient Boosted Decision Trees</li>
 *      <li>Kernel Ridge Regression</li>
 *    </ul>
 *  <li>Ensemble Learning</li>
 *    <ul>
 *      <li>Stacking (linear combination, lowering the RMSE)</li>
 *      <li>Training on Residuals</li>
 *      <li>Cascade Learning</li>
 *      <li>Bagging (from parameter selection)</li>
 *    </ul>
 *  <li>Prediction</li>
 *    <ul>
 *      <li>3 Modes</li>
 *        <ul>
 *          <li>Retraining - Re-Train the model with best metaparameters on all avaliable data</li>
 *          <li>CrossFoldMean - Average prediction of k models from the k-fold cross validation</li>
 *          <li>Bagging - Average prediction of k models from the bagging setup</li>
 *        </ul>
 *      <li>All model parameters and combination weights are stored in binary files</li>
 *      <li>Ready-to-predict state. Making predictions of arbitrary new test features from the whole ensemble</li>
 *    </ul>
 * </ul>
 *
 * \section Usage
 *
 * The configuration of the learners (which algorithms and which parameters) is dataset dependent. A separate directory structure for each dataset is needed. In this directory, a <tt>Master.dsc</tt> file must exist. The file ending <tt>*.dsc</tt> means this file is a description file (text file).
 *
 * \section File-Format
 *
 * This section gives a short overview of the <tt>dsc</tt>-file-format. Lines, which begin with <tt>#</tt> are comments. A value can be set with e.g. <tt>value=1.23</tt>, no spaces between the "=" sign. 
 * The <tt>Master.dsc</tt> file must begin in the first line with e.g. <tt>dataset=MNIST</tt>. The second line must be e.g. <tt>isClassificationDataset=1</tt>. Then a couple of values are set. The algorithms in their train order must be specified after a line, which contains <tt>[ALGORITHMS]</tt>.
 * In the algortihm's <tt>dsc</tt>-files (e.g. <tt>NeuralNetwork_1.dsc</tt>) there are sections for each variable type. The types are <tt>[int]</tt>, <tt>[double]</tt>, <tt>[bool]</tt> and <tt>[string]</tt>. The first line in an algorithm's description file is e.g. <tt>ALGORITHM=LinearModel</tt>. The second line is e.g. <tt>ID=1</tt>, the ids in the algorithms are ascending, beginning with 1. The third line can be e.g. <tt>TRAIN_ON_FULLPREDICTOR=NeuralNetwork_1.dat</tt>, which means that the current algorithm trains on the residuals which is stored in the <tt>NeuralNetwork_1.dat</tt> file. The fourth line can be e.g. <tt>DISABLE=1</tt>, which assumes that this algorithm is already trained (is helpful when train ensembles).
 *
 * \section Starting
 *
 * The data set name and one of these characters [t,b,p] must be specified in the console in order to start the training, blending or prediction. Blending means to combine existing results (e.g. calculate linear regression coefficients). The following examples shows the usage.<br>
 * <tt>$ ./ELF LETTER t </tt>: start training <br>
 * <tt>$ ./ELF LETTER b </tt>: start blending existing algorithms (seldom used)<br>
 * <tt>$ ./ELF LETTER p </tt>: start prediction <br>
 *
 * The ELF starts training all specified algorithms sequentially when <tt>./ELF dataset t</tt> is entered into your console. The algorithms are set in the <tt>Master.dsc</tt> file after the line <tt>[ALGORITHMS]</tt>. By calling <tt>./ELF dataset p</tt> the ELF starts to predict the predifined testset of the dataset.
 *
 */

using namespace std;

StreamOutput cout;

int main ( int argc, char *argv[] )
{
    // number of arguments check
    if ( argc <= 1 )
    {
        cout<<"1) Please supply at least one argument [e] for algorithm exploration"<<endl;
        cout<<"2) Please supply a dataset directory and [t,p,b] for training, prediction or blending"<<endl;
        cout<<"   Example: ./ELF MNIST t"<<endl<<endl;
        exit ( 0 );
    }

    // set max. number of threads for OPENMP sections
    Framework::setMaxThreads ( omp_get_max_threads() );  // store max. number of threads of the machine

    // Algorithm exploration
    // one argument 'e'
    if ( argc == 2 )
        if ( argv[1][0] == 'e' || argv[1][0] == 'E' )
        {
            AlgorithmExploration a;
            a.start();
            exit ( 0 );
        }

    // training/prediction/blending
    // two arguments: 'DATASET'  and 't' 'p' or 'b' for train, predict or blend
    if ( argc <= 2 )
    {
        cout<<"Please supply a dataset directory and [t,p,b] for training, prediction, or blending"<<endl;
        cout<<"Example: ./ELF MNIST t"<<endl<<endl;
        exit ( 0 );
    }
    if ( argc >= 4 )
        Framework::setAdditionalStartupParameter ( argv[3] );

    // the scheduler is responsible for sequential training of the algorithms
    // at the begining, the master description file is read
    Scheduler s;
    s.readMasterDscFile ( argv[1], "Master.dsc" );

    // training mode
    if ( argv[2][0] == 't' || argv[2][0] == 'T' )
        s.train();

    // prediction mode
    if ( argv[2][0] == 'p' || argv[2][0] == 'P' )
        s.predict();

    // blending mode (after successful training)
    if ( argv[2][0] == 'b' || argv[2][0] == 'B' )
        s.blend();

    // OBSOLETE! bagging: produce a set of predictions with slightly modified train set and average them
    if ( argv[2][0] == 'x' || argv[2][0] == 'X' )
        s.bagging();

    // OBSOLETE! boosting: reweight sample probabilites based on train error
    if ( argv[2][0] == 'y' || argv[2][0] == 'Y' )
        s.boosting();

    return 0;
}
