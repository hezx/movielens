#ifndef __SCHEDULER_
#define __SCHEDULER_

#include "Data.h"
#include "BlendStopping.h"
#include "Algorithm.h"
#include "LinearModel.h"
#include "KNearestNeighbor.h"
#include "NeuralNetwork.h"
#include "NeuralNetworkRBMauto.h"
#include "PolynomialRegression.h"
#include "LinearModelNonNeg.h"
#include "Framework.h"
#include "BlendingNN.h"
#include "DatasetReader.h"
#include "KernelRidgeRegression.h"
#include "Autoencoder.h"
#include "GBDT.h"
#include "LogisticRegression.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>

using namespace std;

/**
 * Schedules the training and prediction
 * Reads the master-description *dsc file
 *
 * Possible operation modes
 * - Training
 * - Prediction
 * - Blending (estimation of combination weights)
 *
 * This class can force the ready-to-prediction mode in the ensemble
 * Very useful for predicting any test feature
 */

class Scheduler : public Framework
{
public:
    Scheduler();
    ~Scheduler();

    void readMasterDscFile ( string path, string masterName );
    void train();
    void predict();
    void blend();
    void bagging();
    void boosting();

    REAL getPredictionRMSE();
    REAL getClassificationError();

    static string masterDscTemplateGenerator ( string dataset, bool isClass, vector<string> algos, int rSeed, string blendAlgo, bool cascade );

private:
    void trainAlgorithm ( string fnameTemplate, string fnameDsc );
    void checkAlgorithmTemplate ( string fname, string &algoName, string &id );
    void setPredictionModeInAlgorithm ( string fname );
    int getIDFromFullPredictor ( string fullPredictor );
    void algorithmDispatcher ( Algorithm* &algo, string name );
    void getEnsemblePrediction ( REAL* input, REAL* output );
    void preparePredictionMode();
    int getIndexOfMax ( REAL* vector, int length );
    void endPredictionMode();

    vector<string> m_algorithmList;
    vector<int> m_algorithmIDList;
    vector<string> m_algorithmNameList;
    Data* m_data;  // base data object
    vector<Algorithm*> m_algorithmObjectList;
    vector<Algorithm**> m_algorithmObjectListList;  // this is for: validationType=CrossFoldMean|Bagging

    // prediction mode
    BlendStopping* m_blender;
    BlendingNN* m_blenderNN;
    int* m_labelsPredict;
    int* m_effectID;
    REAL* m_noEffect;
    REAL** m_outputs;
    REAL** m_effects;
    REAL m_predictionRMSE;
    REAL m_predictionClassificationError;
    REAL* m_outputVectorTmp;
    int* m_labelsTmp;
    REAL* m_gradientBoostingTargetMean;
    REAL* m_gradientBoostingOutputSum;

    // bagging / boosting
    bool m_baggingRun;
    bool m_boostingRun;
    uint m_randSeedBagBoost;
    REAL* m_probs;
    REAL* m_boostingTrain;
    REAL* m_boostingTargets;
    int m_boostingNTrain;
    int m_boostingEpoch;
};


#endif
