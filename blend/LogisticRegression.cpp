#include "LogisticRegression.h"

extern StreamOutput cout;

/**
 * Constructor
 */
LogisticRegression::LogisticRegression()
{
    cout<<"LogisticRegression"<<endl;
    // init member vars
    m_x = 0;
    m_reg = 0;
    m_tuneOffsetScale = 0;
}

/**
 * Destructor
 */
LogisticRegression::~LogisticRegression()
{
    cout<<"descructor LogisticRegression"<<endl;
    for ( int i=0;i<m_nCross+1;i++ )
    {
        if ( m_x )
        {
            if ( m_x[i] )
                delete[] m_x[i];
            m_x[i] = 0;
        }
    }
    if ( m_x )
        delete[] m_x;
    m_x = 0;
}

/**
 * Read the Algorithm specific values from the description file
 *
 */
void LogisticRegression::readSpecificMaps()
{
    m_reg = m_doubleMap["initReg"];
    m_offset = m_doubleMap["initOffset"];
    m_scale = m_doubleMap["initScale"];
    m_tuneOffsetScale = m_boolMap["tuneOffsetScale"];
}

/**
 * Init the Linear Model
 *
 */
void LogisticRegression::modelInit()
{
    if(m_nDomain != 1)
        assert(false); // do not allow this
    
    // add the tunable parameter
    paramDoubleValues.push_back ( &m_reg );
    paramDoubleNames.push_back ( "reg" );
    if ( m_tuneOffsetScale )
    {
        paramDoubleValues.push_back ( &m_offset );
        paramDoubleNames.push_back ( "offset" );
        paramDoubleValues.push_back ( &m_scale );
        paramDoubleNames.push_back ( "scale" );
    }
    // alloc mem for weights
    if ( m_x == 0 )
    {
        m_x = new REAL*[m_nCross+1];
        for ( int i=0;i<m_nCross+1;i++ )
            m_x[i] = new REAL[m_nFeatures * (m_nClass - 1)];
    }
}

/**
 * Prediction for outside use, predicts outputs based on raw input values
 *
 * @param rawInputs The input feature, without normalization (raw)
 * @param outputs The output value (prediction of target)
 * @param nSamples The input size (number of rows)
 * @param crossRun Number of cross validation run (in training)
 */
void LogisticRegression::predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun )
{
    for ( int i=0;i<nSamples;i++ )
    {
        /* MATLAB
        function p = predictLogistic(A,x)
        N = size(A,1);
        k = size(x,2)+1;
        p0 = [ones(size(A,1),1) A]*x;
        p1 = [exp(p0) ones(N,1)];
        p2 = p1 ./ repmat(sum(p1,2),1,k);
        p = p2;
        */
        
        for(int j=0;j<m_nClass-1;j++)
        {
            REAL sum = 0.0;
            for(int k=0;k<m_nFeatures;k++)
                sum += rawInputs[i*m_nFeatures+k] * m_x[crossRun][k*(m_nClass-1) + j];
            outputs[i*m_nClass+j] = sum;
        }
        
        for(int j=0;j<m_nClass-1;j++)
            outputs[i*m_nClass+j] = exp(outputs[i*m_nClass+j]);
        outputs[i*m_nClass+m_nClass-1] = 1.0;
        
        REAL sum = 0.0;
        for(int j=0;j<m_nClass;j++)
            sum += outputs[i*m_nClass+j];
        
        for(int j=0;j<m_nClass;j++)
            outputs[i*m_nClass+j] /= sum;
    }
}

/**
 * Make a model update, set the new cross validation set or set the whole training set
 * for retraining
 *
 * @param input Pointer to input (can be cross validation set, or whole training set) (#rows x nFeatures)
 * @param target The targets (can be cross validation targets)
 * @param nSamples The sample size (#rows) in input
 * @param crossRun The cross validation run (for training)
 */
void LogisticRegression::modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun )
{
    // solve the linear system
    solver.LogisticRegressionMultisolutionSinglecall ( input, target, m_x[crossRun], nSamples, m_nFeatures, m_nClass-1, m_reg, m_offset, m_scale );
}

/**
 * Save the weights and all other parameters for load the complete prediction model
 *
 */
void LogisticRegression::saveWeights ( int cross )
{
    char buf[1024];
    sprintf ( buf,"%03d",cross );
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile + "." + buf + Framework::getGradientBoostingEpochString();
    if ( m_inRetraining )
        cout<<"Save:"<<name<<endl;
    fstream f ( name.c_str(), ios::out );
    f.write ( ( char* ) &m_nTrain, sizeof ( int ) );
    f.write ( ( char* ) &m_nFeatures, sizeof ( int ) );
    f.write ( ( char* ) &m_nClass, sizeof ( int ) );
    f.write ( ( char* ) &m_nDomain, sizeof ( int ) );
    f.write ( ( char* ) m_x[cross], sizeof ( REAL ) *m_nFeatures*(m_nClass-1) );
    f.write ( ( char* ) &m_reg, sizeof ( double ) );
    f.write ( ( char* ) &m_offset, sizeof ( double ) );
    f.write ( ( char* ) &m_scale, sizeof ( double ) );
    f.write ( ( char* ) &m_maxSwing, sizeof ( double ) );
    f.close();
}

/**
 * Load the weights and all other parameters and make the model ready to predict
 *
 */
void LogisticRegression::loadWeights ( int cross )
{
    char buf[1024];
    sprintf ( buf,"%03d",cross );
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile + "." + buf + Framework::getGradientBoostingEpochString();
    cout<<"Load:"<<name<<endl;
    fstream f ( name.c_str(), ios::in );
    if ( f.is_open() == false )
        assert ( false );
    f.read ( ( char* ) &m_nTrain, sizeof ( int ) );
    f.read ( ( char* ) &m_nFeatures, sizeof ( int ) );
    f.read ( ( char* ) &m_nClass, sizeof ( int ) );
    f.read ( ( char* ) &m_nDomain, sizeof ( int ) );

    m_x = new REAL*[m_nCross+1];
    for ( int i=0;i<m_nCross+1;i++ )
        m_x[i] = 0;
    m_x[cross] = new REAL[m_nFeatures * (m_nClass-1)];

    f.read ( ( char* ) m_x[cross], sizeof ( REAL ) *m_nFeatures*(m_nClass-1) );
    f.read ( ( char* ) &m_reg, sizeof ( double ) );
    f.read ( ( char* ) &m_offset, sizeof ( double ) );
    f.read ( ( char* ) &m_scale, sizeof ( double ) );
    f.read ( ( char* ) &m_maxSwing, sizeof ( double ) );
    f.close();
}

/**
 *
 */
void LogisticRegression::loadMetaWeights ( int cross )
{
    char buf[1024];
    sprintf ( buf,"%03d",cross );
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile + "." + buf + Framework::getGradientBoostingEpochString();
    cout<<"LoadMeta:"<<name<<endl;
    fstream f ( name.c_str(), ios::in );
    if ( f.is_open() == false )
        assert ( false );
    f.read ( ( char* ) &m_nTrain, sizeof ( int ) );
    f.read ( ( char* ) &m_nFeatures, sizeof ( int ) );
    f.read ( ( char* ) &m_nClass, sizeof ( int ) );
    f.read ( ( char* ) &m_nDomain, sizeof ( int ) );
    REAL* tmp = new REAL[m_nFeatures*m_nClass*m_nDomain];
    f.read ( ( char* ) tmp, sizeof ( REAL ) *m_nFeatures*(m_nClass-1) );
    delete[] tmp;
    f.read ( ( char* ) &m_reg, sizeof ( double ) );
    f.read ( ( char* ) &m_offset, sizeof ( double ) );
    f.read ( ( char* ) &m_scale, sizeof ( double ) );
    f.read ( ( char* ) &m_maxSwing, sizeof ( double ) );
    f.close();
}

/**
 * Generates a template of the description file
 *
 * @return The template string
 */
string LogisticRegression::templateGenerator ( int id, string preEffect, int nameID, bool blendStop )
{
    stringstream s;
    s<<"ALGORITHM=LogisticRegression"<<endl;
    s<<"ID="<<id<<endl;
    s<<"TRAIN_ON_FULLPREDICTOR="<<preEffect<<endl;
    s<<"DISABLE=0"<<endl;
    s<<endl;
    s<<"[int]"<<endl;
    s<<"maxTuninigEpochs=20"<<endl;
    s<<endl;
    s<<"[double]"<<endl;
    s<<"initMaxSwing=1.0"<<endl;
    s<<"initReg=0.01"<<endl;
    s<<"initOffset=0.5"<<endl;
    s<<"initScale=2.0"<<endl;
    s<<endl;
    s<<"[bool]"<<endl;
    s<<"tuneOffsetScale=1"<<endl;
    s<<"enableClipping=1"<<endl;
    s<<"enableTuneSwing=0"<<endl;
    s<<endl;
    s<<"minimzeProbe="<< ( !blendStop ) <<endl;
    s<<"minimzeProbeClassificationError=0"<<endl;
    s<<"minimzeBlend="<<blendStop<<endl;
    s<<"minimzeBlendClassificationError=0"<<endl;
    s<<endl;
    s<<"[string]"<<endl;
    s<<"weightFile="<<"LogisticRegression_"<<nameID<<"_weights.dat"<<endl;
    s<<"fullPrediction=LogisticRegression_"<<nameID<<".dat"<<endl;

    return s.str();
}
