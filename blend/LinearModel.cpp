#include "LinearModel.h"

extern StreamOutput cout;

/**
 * Constructor
 */
LinearModel::LinearModel()
{
    cout<<"LinearModel"<<endl;
    // init member vars
    m_x = 0;
    m_reg = 0;
    m_tuneRigeModifiers = false;
    m_ridgeModifiers = 0;
}

/**
 * Destructor
 */
LinearModel::~LinearModel()
{
    cout<<"descructor LinearModel"<<endl;
    for ( int i=0;i<m_nCross+1;i++ )
    {
        if ( m_x )
        {
            if ( m_x[i] )
                delete[] m_x[i];
            m_x[i] = 0;
        }
    }
    if ( m_x )
        delete[] m_x;
    m_x = 0;

    if ( m_ridgeModifiers )
        delete[] m_ridgeModifiers;
    m_ridgeModifiers = 0;
}

/**
 * Read the Algorithm specific values from the description file
 *
 */
void LinearModel::readSpecificMaps()
{
    m_reg = m_doubleMap["initReg"];
    m_tuneRigeModifiers = m_boolMap["tuneRigeModifiers"];
}

/**
 * Init the Linear Model
 *
 */
void LinearModel::modelInit()
{
    // add the tunable parameter
    paramDoubleValues.push_back ( &m_reg );
    paramDoubleNames.push_back ( "reg" );

    // alloc mem for weights
    if ( m_x == 0 )
    {
        m_x = new REAL*[m_nCross+1];
        for ( int i=0;i<m_nCross+1;i++ )
            m_x[i] = new REAL[m_nFeatures * m_nClass * m_nDomain];
    }

    if ( m_tuneRigeModifiers )
    {
        m_ridgeModifiers = new double[m_nFeatures];
        for ( int i=0;i<m_nFeatures;i++ )
        {
            m_ridgeModifiers[i] = 1.0;
            paramDoubleValues.push_back ( & ( m_ridgeModifiers[i] ) );
            char buf[1024];
            sprintf ( buf,"%d",i );
            paramDoubleNames.push_back ( "r"+string ( buf ) );
        }
    }
}

/**
 * Prediction for outside use, predicts outputs based on raw input values
 *
 * @param rawInputs The input feature, without normalization (raw)
 * @param outputs The output value (prediction of target)
 * @param nSamples The input size (number of rows)
 * @param crossRun Number of cross validation run (in training)
 */
void LinearModel::predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun )
{
    for ( int i=0;i<nSamples;i++ )
    {
        for ( int j=0;j<m_nClass*m_nDomain;j++ )
        {
            REAL sum = 0.0;
            for ( int k=0;k<m_nFeatures;k++ )
            {
                REAL x = rawInputs[i*m_nFeatures + k];
                sum += x * m_x[crossRun][k*m_nClass*m_nDomain + j];
            }
            outputs[i*m_nClass*m_nDomain + j] = sum;
        }
    }
}

/**
 * Make a model update, set the new cross validation set or set the whole training set
 * for retraining
 *
 * @param input Pointer to input (can be cross validation set, or whole training set) (#rows x nFeatures)
 * @param target The targets (can be cross validation targets)
 * @param nSamples The sample size (#rows) in input
 * @param crossRun The cross validation run (for training)
 */
void LinearModel::modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun )
{
    // solve the linear system
    solver.RidgeRegressionMultisolutionSinglecall ( input, target, m_x[crossRun], nSamples, m_nFeatures, m_nClass*m_nDomain, m_reg, true, m_ridgeModifiers );
    /*cout<<"weights:"<<endl;
    for ( int j=0;j<m_nClass*m_nDomain;j++ )
    {
        cout<<"class "<<j<<endl;
        //REAL sum = 0.0;
        for ( int k=0;k<m_nFeatures;k++ )
        {
            //REAL x = rawInputs[i*m_nFeatures + k];
            cout<<m_x[crossRun][k*m_nClass*m_nDomain + j]<<" ";
        }
        //outputs[i*m_nClass*m_nDomain + j] = sum;
        cout<<endl;
    }*/
}

/**
 * Save the weights and all other parameters for load the complete prediction model
 *
 */
void LinearModel::saveWeights ( int cross )
{
    char buf[1024];
    sprintf ( buf,"%03d",cross );
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile + "." + buf + Framework::getGradientBoostingEpochString();
    if ( m_inRetraining )
        cout<<"Save:"<<name<<endl;
    fstream f ( name.c_str(), ios::out );
    f.write ( ( char* ) &m_nTrain, sizeof ( int ) );
    f.write ( ( char* ) &m_nFeatures, sizeof ( int ) );
    f.write ( ( char* ) &m_nClass, sizeof ( int ) );
    f.write ( ( char* ) &m_nDomain, sizeof ( int ) );
    f.write ( ( char* ) m_x[cross], sizeof ( REAL ) *m_nFeatures*m_nClass*m_nDomain );
    f.write ( ( char* ) &m_reg, sizeof ( double ) );
    f.write ( ( char* ) &m_maxSwing, sizeof ( double ) );
    f.close();
}

/**
 * Load the weights and all other parameters and make the model ready to predict
 *
 */
void LinearModel::loadWeights ( int cross )
{
    char buf[1024];
    sprintf ( buf,"%03d",cross );
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile + "." + buf + Framework::getGradientBoostingEpochString();
    cout<<"Load:"<<name<<endl;
    fstream f ( name.c_str(), ios::in );
    if ( f.is_open() == false )
        assert ( false );
    f.read ( ( char* ) &m_nTrain, sizeof ( int ) );
    f.read ( ( char* ) &m_nFeatures, sizeof ( int ) );
    f.read ( ( char* ) &m_nClass, sizeof ( int ) );
    f.read ( ( char* ) &m_nDomain, sizeof ( int ) );

    m_x = new REAL*[m_nCross+1];
    for ( int i=0;i<m_nCross+1;i++ )
        m_x[i] = 0;
    m_x[cross] = new REAL[m_nFeatures * m_nClass * m_nDomain];

    f.read ( ( char* ) m_x[cross], sizeof ( REAL ) *m_nFeatures*m_nClass*m_nDomain );
    f.read ( ( char* ) &m_reg, sizeof ( double ) );
    f.read ( ( char* ) &m_maxSwing, sizeof ( double ) );
    f.close();
}

/**
 *
 */
void LinearModel::loadMetaWeights ( int cross )
{
    char buf[1024];
    sprintf ( buf,"%03d",cross );
    string name = m_datasetPath + "/" + m_tempPath + "/" + m_weightFile + "." + buf + Framework::getGradientBoostingEpochString();
    cout<<"LoadMeta:"<<name<<endl;
    fstream f ( name.c_str(), ios::in );
    if ( f.is_open() == false )
        assert ( false );
    f.read ( ( char* ) &m_nTrain, sizeof ( int ) );
    f.read ( ( char* ) &m_nFeatures, sizeof ( int ) );
    f.read ( ( char* ) &m_nClass, sizeof ( int ) );
    f.read ( ( char* ) &m_nDomain, sizeof ( int ) );
    REAL* tmp = new REAL[m_nFeatures*m_nClass*m_nDomain];
    f.read ( ( char* ) tmp, sizeof ( REAL ) *m_nFeatures*m_nClass*m_nDomain );
    delete[] tmp;
    f.read ( ( char* ) &m_reg, sizeof ( double ) );
    f.read ( ( char* ) &m_maxSwing, sizeof ( double ) );
    f.close();
}

/**
 * Generates a template of the description file
 *
 * @return The template string
 */
string LinearModel::templateGenerator ( int id, string preEffect, int nameID, bool blendStop )
{
    stringstream s;
    s<<"ALGORITHM=LinearModel"<<endl;
    s<<"ID="<<id<<endl;
    s<<"TRAIN_ON_FULLPREDICTOR="<<preEffect<<endl;
    s<<"DISABLE=0"<<endl;
    s<<endl;
    s<<"[int]"<<endl;
    s<<"maxTuninigEpochs=20"<<endl;
    s<<endl;
    s<<"[double]"<<endl;
    s<<"initMaxSwing=1.0"<<endl;
    s<<"initReg=0.01"<<endl;
    s<<endl;
    s<<"[bool]"<<endl;
    s<<"tuneRigeModifiers=0"<<endl;
    s<<"enableClipping=1"<<endl;
    s<<"enableTuneSwing=0"<<endl;
    s<<endl;
    s<<"minimzeProbe="<< ( !blendStop ) <<endl;
    s<<"minimzeProbeClassificationError=0"<<endl;
    s<<"minimzeBlend="<<blendStop<<endl;
    s<<"minimzeBlendClassificationError=0"<<endl;
    s<<endl;
    s<<"[string]"<<endl;
    s<<"weightFile="<<"LinearModel_"<<nameID<<"_weights.dat"<<endl;
    s<<"fullPrediction=LinearModel_"<<nameID<<".dat"<<endl;

    return s.str();
}
