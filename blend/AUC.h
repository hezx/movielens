#ifndef __AUC_
#define __AUC_

#include "Framework.h"

#include "ippi.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>

using namespace std;

/**
 * AUC - Area under curve calculator
 *
 * Works for binary classification
 * Input: score and labels[0,1]
 *
 */

class AUC : public Framework
{
public:
    AUC();
    ~AUC();
    REAL getAUC ( REAL* prediction, int* labels, int nClass, int nDomain, int nLines );

};

#endif
