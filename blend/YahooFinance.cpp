#include "YahooFinance.h"

extern StreamOutput cout;

/**
 * Constructor
 */
YahooFinance::YahooFinance()
{
}

/**
 * Destructor
 */
YahooFinance::~YahooFinance()
{
}

// tmp function for CURL
size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t written;
    written = fwrite(ptr, size, nmemb, stream);
    return written;
}

/**
 * Read csv files from YAHOO finance
 *
 * @param files string of file names
 */
void YahooFinance::readCSVs(vector<string> &files)
{
    cout<<"Read CSV files"<<endl;
    uint cnt = 0;
    char buf[1024];
    m_globalMaxDate = 0;
    m_globalMinDate = UINT_MAX;
    
    for(int i=0;i<files.size();i++)
    {
        uint minDay = m_globalMinDate, maxDay = m_globalMaxDate;
        
        // a map for filling particular days with stock data (some days have missing values)
        map<uint, stockDay> map0;
        
        //cout<<i<<" "<<files[i]<<endl;
        cout<<"."<<flush;
        // Date, Open, High, Low,  Close,Volume, Adj Close
        //2009-11-20,28.94,29.05,28.67,28.91,1978000,28.91     <- example data
        //2009-11-19,29.09,29.36,28.67,29.13,2213500,29.13     <- example data
        fstream f(files[i].c_str(), ios::in);
        uint day, month, year, hour = 0, minute = 0, second = 0, date = 0, weekday = 0;
        f.getline(buf,1024);  // read first line
        while(f.getline(buf,1024))
        {
            string line = buf;
            char* ptr = buf, *ptrOld = 0;
            int delCnt = 0;
            REAL open = 0.0, close = 0.0, high = 0.0, low = 0.0, volume = 0.0, adjClose = 0.0, factor = 0.0;
            while(*ptr)
            {
                if(*ptr == ',' || *(ptr+1) == 0)
                {
                    if(*(ptr+1) != 0)
                        *ptr = 0;
                    if(delCnt == 0)
                    {
                        buf[4] = 0;  // clear '-'
                        buf[7] = 0;  // clear '-'
                        year = atoi(buf);
                        month = atoi(buf+5);
                        day = atoi(buf+8);
                        date = Framework::convertDateToInt(day, month, year, hour, minute, second);
                        uint day0, month0, year0, hour0, minute0, second0;
                        Framework::convertIntToDate(date, day0, month0, year0, hour0, minute0, second0, weekday);
                    }
                    else if(delCnt == 1)  // Open
                        open = atof(ptrOld);
                    else if(delCnt == 2)  // High
                        high = atof(ptrOld);
                    else if(delCnt == 3)  // Low
                        low = atof(ptrOld);
                    else if(delCnt == 4)  // Close
                        close = atof(ptrOld);
                    else if(delCnt == 5)  // Volume
                        volume = atof(ptrOld);
                    else if(delCnt == 6)  // Adj Close
                        adjClose = atof(ptrOld);
                    delCnt++;
                    ptrOld = ptr+1;
                }
                ptr++;
            }
            if(adjClose > 0.0 && ((year == MIN_YEAR && month >= MIN_MONTH) || year > MIN_YEAR))
            {
                REAL factor = adjClose/close;
                open *= factor;
                high *= factor;
                low *= factor;
                close *= factor;
                if(high < open || low > open || high < close || low > close)  // should never happen
                {
                    cout<<"Warning: inconsistent data in "<<files[i]<<" line:"<<line<<endl; //"  open:"<<open<<" close:"<<close<<" high:"<<high<<" low:"<<low<<" endl;
                    //assert(false);
                }
                else if(open > 0.0 && close > 0.0 && high > 0.0 && low > 0.0 && volume > 0.0)  // when data is valid
                {
                    uint dateDay = date/86400;
                    map0[dateDay].m_open = open;
                    map0[dateDay].m_close = close;
                    map0[dateDay].m_high = high;
                    map0[dateDay].m_low = low;
                    map0[dateDay].m_volume = volume;
                    map0[dateDay].m_weekday = weekday;
                    cnt++;
                    if(minDay > dateDay)
                        minDay = dateDay;
                    if(maxDay < dateDay)
                        maxDay = dateDay;
                }
            }
        }
        
        // build a stock cycle
        stockCycle sc;
        for(uint j=0;j<maxDay-minDay+1;j++)
        {
            stockDay sd;
            sd.m_open = 0.0;
            sd.m_close = 0.0;
            sd.m_high = 0.0;
            sd.m_low = 0.0;
            sd.m_volume = 0.0;
            sd.m_weekday = 0.0;
            sc.m_data.push_back(sd);
            sc.m_dataDay.push_back(j+minDay);
        }
        sc.m_maxDate = maxDay;
        sc.m_minDate = minDay;
        sc.m_name = files[i];
        for(map<uint, stockDay>::iterator it=map0.begin();it!=map0.end();it++)
        {
            uint d = it->first;
            stockDay sd = it->second;
            sc.m_data[d-minDay] = sd;
            if(d == minDay)
                sc.m_beginClose = sd.m_close;
        }
        
        // add stock data to pool
        m_stocks.push_back(sc);
        if(sc.m_data.size() != sc.m_dataDay.size())
            assert(false);
        
        // update the global min/max day
        if(m_globalMaxDate < maxDay)
            m_globalMaxDate = maxDay;
        if(m_globalMinDate > minDay)
            m_globalMinDate = minDay;
    }
    
    cout<<endl<<"data points:"<<cnt<<"  average lines per stock:"<<(double)cnt/(double)m_stocks.size()<<" globalMaxDate:"<<m_globalMaxDate<<" globalMinDate:"<<m_globalMinDate<<endl<<endl;
}

/**
 * Request data in the past of a given stockCycle
 *
 * @param stock The stockCycle
 * @param date On this day we request data
 * @param nDaysBackSearch Max. number of days in the past for the lookup
 * @return -1 when no data is avaliable, absolute day if success
 */
int YahooFinance::requestDate(stockCycle &stock, uint date, uint nDaysBackSearch)
{
    if(date < stock.m_minDate)  // too early request date
        return -1;  // invalid
    if(date > stock.m_maxDate)  // too late request date
        return -1;  // invalid
    
    for(uint i=0;i<nDaysBackSearch;i++)
    {
        int ind = (int)date - (int)(stock.m_minDate) - (int)i;
        if(ind < 0)
            return -1;  // invalid
        if(ind >= stock.m_dataDay.size())
            assert(false);  // sth is wrong
        uint rqDate = stock.m_dataDay[ind];
        if(stock.m_data[ind].m_open != 0.0)
            return rqDate;
    }
    return -1;
}

/**
 * Calculate an nDay moving average
 * 
 * @param data stock cycle raw input
 * @param nDays lenght of filter window
 * @return complete stock cycle filtered by a moving average filter
 */
stockCycle YahooFinance::calculateNDayMean(stockCycle &data, uint nDays)
{
    stockCycle s;
    s.m_maxDate = data.m_maxDate;
    s.m_minDate = data.m_minDate;
    for(uint i=0;i<data.m_data.size();i++)  // make an empty copy of data
    {
        stockDay sd;
        sd.m_open = 0.0;
        sd.m_close = 0.0;
        sd.m_high = 0.0;
        sd.m_low = 0.0;
        sd.m_volume = 0;
        sd.m_weekday = 0;
        s.m_data.push_back(sd);
        s.m_dataDay.push_back(i+data.m_minDate);
        if(i+data.m_minDate != data.m_dataDay[i])
            assert(false);
    }
    for(uint i=nDays-1;i<data.m_data.size();i++)
    {
        double sumOpen = 0.0, sumClose = 0.0, sumHigh = 0.0, sumLow = 0.0, sumVolume = 0.0;
        int cnt = 0;
        for(uint j=0;j<nDays;j++)
        {
            int pos = i - j;
            if(data.m_data[pos].m_open != 0.0)
            {
                sumOpen += data.m_data[pos].m_open;
                sumClose += data.m_data[pos].m_close;
                sumHigh += data.m_data[pos].m_high;
                sumLow += data.m_data[pos].m_low;
                sumVolume += data.m_data[pos].m_volume;
                cnt++;
            }
        }
        if(cnt > 0)
        {
            stockDay sd;
            sd.m_open = sumOpen/(double)cnt;
            sd.m_close = sumClose/(double)cnt;
            sd.m_high = sumHigh/(double)cnt;
            sd.m_low = sumLow/(double)cnt;
            sd.m_volume = sumVolume/(double)cnt;
            s.m_data[i] = sd;
        }
    }
    
    return s;
}

/**
 * Make features based on a SVD decomposition of all stocks in a time window of m_nDayWindow days
 * Resulting features are stored in member variables
 */
void YahooFinance::calculateSVDFeatures()
{
    cout<<"Calculate the SVD stock x time features"<<endl;
    cout<<"nFeatures:"<<m_nFeatures<<" nDayWindow:"<<m_nDayWindow<<endl;
    
    int daySpan = m_globalMaxDate - m_globalMinDate + 1, nStocks = m_stocks.size();
    cout<<"daySpan:"<<daySpan<<" nStocks:"<<nStocks<<endl;
    
    // allocate memory
    m_timeSVDFeatures = new REAL**[daySpan];  // [day] -> feature
    m_timeStockSVDFeatures = new REAL**[daySpan];  // [day][stock] -> feature
    for(int i=0;i<daySpan;i++)
    {
        m_timeSVDFeatures[i] = new REAL*[m_nDayWindow];
        m_timeStockSVDFeatures[i] = new REAL*[nStocks];
        for(int j=0;j<m_nDayWindow;j++)
        {
            m_timeSVDFeatures[i][j] = new REAL[m_nFeatures];
            for(int k=0;k<m_nFeatures;k++)
                m_timeSVDFeatures[i][j][k] = 0.0;
        }
        for(int j=0;j<nStocks;j++)
        {
            m_timeStockSVDFeatures[i][j] = new REAL[m_nFeatures];
            for(int k=0;k<m_nFeatures;k++)
                m_timeStockSVDFeatures[i][j][k] = 0.0;
        }
    }
    
    // fill big data matrix
    REAL* data = new REAL[nStocks*daySpan];
    for(uint i=0;i<nStocks*daySpan;i++)
        data[i] = 0.0;
    for(int i=0;i<nStocks;i++)  // all stocks
    {
        for(int j=0;j<m_stocks[i].m_data.size();j++)  // all values per stock
        {
            uint d = m_stocks[i].m_dataDay[j] - m_globalMinDate;
            if(d >= daySpan)
            {
                cout<<"m_stocks["<<i<<"].m_dataDay["<<j<<"]:"<<m_stocks[i].m_dataDay[j]<<" d:"<<d<<" daySpan:"<<daySpan<<endl;
                assert(false);
            }
            if(m_stocks[i].m_data[j].m_open != 0.0)
                data[i*daySpan + d] = m_stocks[i].m_data[j].m_close;
            
            /*
            int date = m_stocks[i].m_dataDay[j];
            int dateBefore = requestDate(m_stocks[i], date-1);
            if(dateBefore != -1 && m_stocks[i].m_data[j].m_open != 0.0)
            {
                int jBefore = dateBefore - m_stocks[i].m_minDate;
                data[i*daySpan+date-m_globalMinDate] = (m_stocks[i].m_data[j].m_close - m_stocks[i].m_data[jBefore].m_close)/m_stocks[i].m_data[jBefore].m_close;
            }*/
        }
    }
    
    /*
    fstream f("A.txt",ios::out);
    for(int i=0;i<nStocks;i++)  // all stocks
    {
        for(int j=0;j<daySpan;j++)
            f<<data[i*daySpan+j]<<" ";
        f<<endl;
    }
    f.close();
    exit(0);
    */
    REAL* A = new REAL[nStocks*m_nDayWindow];
    REAL* mean = new REAL[nStocks];
    REAL* std = new REAL[nStocks];
    
    time_t t0 = time(0);
    for(int rawDay = m_globalMinDate; rawDay <= m_globalMaxDate; rawDay++)
    {
        int day = rawDay - m_globalMinDate;
        if(day >= daySpan || day < 0)
            assert(false);
        if(day < m_nDayWindow)
            continue;
        
        //cout<<"day:"<<day<<" (total: "<<m_globalMaxDate - m_globalMinDate<<")"<<endl;
        cout<<"."<<flush;
        
        // copy submatrix from data to A
        for(int i=0;i<nStocks*m_nDayWindow;i++)
            A[i] = 0.0;
        int dataOffset = day - m_nDayWindow;
        for(int i=0;i<nStocks;i++)
            for(int j=0;j<m_nDayWindow;j++)
            {
                if(dataOffset+j >= daySpan)
                    assert(false);
                A[i*m_nDayWindow+j] = data[i*daySpan+dataOffset+j];
            }
        
        // normalize stocks to mean=0, std=1
        for(int i=0;i<nStocks;i++)  // all stocks
        {
            double sum = 0.0, sum2 = 0.0;
            uint sumCnt = 0;
            mean[i] = 0.0;
            std[i] = 1.0;
            for(int j=0;j<m_nDayWindow;j++)
            {
                REAL v = A[i*m_nDayWindow+j];
                //if(v > 0.0)
                {
                    sum += v;
                    sum2 += v*v;
                    sumCnt++;
                }
            }
            if(sumCnt > 0)
            {
                mean[i] = sum/(double)sumCnt;
                std[i] = sqrt(sum2/(double)sumCnt - (sum/(double)sumCnt)*(sum/(double)sumCnt));
            }
            if(std[i] < 0.1)
                std[i] = 0.1;  // limit the normalization
            //cout<<mean[i]<<" "<<std[i]<<endl;
        }
        
        for(int i=0;i<nStocks;i++)
            for(int j=0;j<m_nDayWindow;j++)
                A[i*m_nDayWindow+j] = (A[i*m_nDayWindow+j] - mean[i])/std[i];
        
        
        REAL* leftMatrix;
        REAL* rightMatrix;
        calculateSVD(A, nStocks, m_nDayWindow, m_nFeatures, leftMatrix, rightMatrix);
        
        // copy stock features
        for(int i=0;i<nStocks;i++)
            for(int j=0;j<m_nFeatures;j++)
                m_timeStockSVDFeatures[day][i][j] = leftMatrix[i*m_nFeatures+j];
        // copy time features
        for(int i=0;i<m_nDayWindow;i++)
            for(int j=0;j<m_nFeatures;j++)
                m_timeSVDFeatures[day][i][j] = rightMatrix[i*m_nFeatures+j];
        
        delete[] leftMatrix;
        delete[] rightMatrix;
    }
    cout<<" "<<time(0)-t0<<"[s]"<<endl;
    
    delete[] A;
    delete[] mean;
    delete[] std;
    delete[] data;
}

/**
 * Decompose a matrix A with nRows and nCols to a rank k matrix
 * A ~ P*Q'
 *
 * @param A input, data alignment: row-wise
 * @param nRows number of rows in A
 * @param nCols number of columns in A
 * @param rank the desired rank
 * @param leftMatrix left factor matrix P
 * @param rightMatrix right factor matrix Q
 */
void YahooFinance::calculateSVD(REAL* A, int nRows, int nCols, int rank, REAL* &leftMatrix, REAL* &rightMatrix)
{
    // make a column-major data array
    REAL* AcolMajor = new REAL[nRows*nCols];
    int cnt = 0;
    for(int i=0;i<nCols;i++)
        for(int j=0;j<nRows;j++)
            AcolMajor[j+i*nRows] = A[j*nCols+i];
    
    int nMin = min(nRows,nCols);
    REAL* s = new REAL[nMin];
    REAL* u = new REAL[nRows*nRows];
    REAL* v = new REAL[nCols*nCols];
    REAL* work = new REAL[1];
    int lwork = -1, info;
    SVD_DECOMP("A", "A", &nRows, &nCols, AcolMajor, &nRows, s, u, &nRows, v, &nCols, work, &lwork, &info);  // workspace query
    lwork = work[0];
    delete[] work;
    work = new REAL[lwork];
    SVD_DECOMP("A", "A", &nRows, &nCols, AcolMajor, &nRows, s, u, &nRows, v, &nCols, work, &lwork, &info);  // Computes the singular value decomposition of a general rectangular matrix
    
    /*
    cout<<endl<<"SVD(A) = U * diag(S) * V"<<endl<<endl;
    cout<<endl<<"U:"<<endl; for(int i=0;i<nRows;i++){ for(int j=0;j<nRows;j++) cout<<u[j*nRows+i]<<" ";  cout<<endl; }
    cout<<endl<<"S:"<<endl;  for(int i=0;i<nMin;i++)  cout<<s[i]<<" "; cout<<endl;
    cout<<endl<<"V^T:"<<endl; for(int i=0;i<nCols;i++){ for(int j=0;j<nCols;j++) cout<<v[j*nCols+i]<<" ";  cout<<endl; }
    */
    
    leftMatrix = new REAL[nRows*rank];
    rightMatrix = new REAL[nCols*rank];
    
    // approximation ~ left * right
    // fill left
    for(int i=0;i<nRows;i++)
        for(int j=0;j<rank;j++)
            leftMatrix[j+i*rank] = u[j*nRows+i] * (j<nMin?s[j]:0.0);
    
    // fill right
    for(int i=0;i<nCols;i++)
        for(int j=0;j<rank;j++)
            rightMatrix[j+i*rank] = v[j+i*nCols];
    
    /*
    cout<<endl;
    for(int rank=1;rank<=nMin;rank++)
    {
        cout<<"Rank "<<rank<<" approximation"<<endl;
        REAL* leftMatrix = new REAL[nRows*rank];
        REAL* rightMatrix = new REAL[nCols*rank];
        
        // approximation ~ left * right
        // fill left
        for(int i=0;i<nRows;i++)
            for(int j=0;j<rank;j++)
                leftMatrix[j+i*rank] = u[j*nRows+i] * (j<nMin?s[j]:0.0);
        
        // fill right
        for(int i=0;i<nCols;i++)
            for(int j=0;j<rank;j++)
                rightMatrix[j+i*rank] = v[j+i*nCols];
        
        // print
        double mse = 0.0;
        for(int i=0;i<nRows;i++)
        {
            for(int j=0;j<nCols;j++)
            {
                REAL sum = 0.0;
                for(int k=0;k<rank;k++)
                    sum += leftMatrix[k+i*rank] * rightMatrix[k+j*rank];
                mse += (sum - A[j+i*nCols])*(sum - A[j+i*nCols]);
                //cout<<sum<<" ";
            }
            //cout<<endl;
        }
        cout<<"MSE:"<<mse<<endl<<endl;
        
        delete[] leftMatrix;
        delete[] rightMatrix;
    }
    exit(0);
    */
    
    delete[] work;
    delete[] s;
    delete[] u;
    delete[] v;
    delete[] AcolMajor;
}

/**
 * Check a feature for nan and inf values
 * @param feature the feature
 */
void YahooFinance::checkFeature(vector<float> &feature)
{
    for(int i=0;i<feature.size();i++)
    {
        if(isnan(feature[i]))
        {
            cout<<"nan value on index:"<<i<<endl;
            assert(false);
        }
        if(isinf(feature[i]))
        {
            cout<<"inf value on index:"<<i<<endl;
            assert(false);
        }
    }
}

/**
 * Make feature of raw stock data
 * Time resolution: days
 */
void YahooFinance::makeFeatures()
{
    cout<<"Make the features"<<endl;
    
    // calculate the SVD: stock x time features
    m_nFeatures = 5;
    m_nDayWindow = 80;
    //calculateSVDFeatures();
    
    int numberOfDaysInNDayMean = 40;
    int numberOfDaysInDifferentialFeatures = 40;
    int numberOfDaysInLastNrRiseFallCnt = 40;
    
    int nFeatOld = 0, nFeat = 0;
    time_t t0 = time(0);
    
    // over all stocks
    for(int i=0;i<m_stocks.size();i++)
    {
        cout<<"."<<flush;
        //cout<<i<<"|"<<m_stocks.size()<<" Stock:"<<m_stocks[i].m_name<<"  nFeat:"<<nFeat-nFeatOld<<"  "<<flush;
        nFeatOld = nFeat;
        
        // HACK
        /*int l0 = m_stocks[i].m_data.size();
        for(int k=0;k<20;k++)
            cout<<k<<":"<<m_stocks[i].m_data[l0-20+k].m_open<<" ";
        cout<<endl;
        */
        
        // calculate nDay means (moving average)
        vector<stockCycle> dayNMean;
        for(int j=2;j<numberOfDaysInNDayMean;j++)
        {
            dayNMean.push_back(calculateNDayMean(m_stocks[i], j));
            // HACK
            /*for(int k=0;k<20;k++)
                cout<<k<<":"<<dayNMean[dayNMean.size()-1].m_data[l0-20+k].m_open<<" ";
            cout<<endl;
            */
        }
        
        // over all days of stock i
        for(int j=m_stocks[i].m_data.size()-1;j>=0;j--)
        {
            // absolute days
            uint date = m_stocks[i].m_dataDay[j];
            int dateBefore = requestDate(m_stocks[i], date-1);
            
            if(m_stocks[i].m_data[j].m_open == 0.0)  // check if target date is avaliable
                continue;
            if(dateBefore == -1 || date - dateBefore > 4)  // check avaliability
                continue;
            
            uint dateBeforeNoOffset = dateBefore - m_globalMinDate;
            if(dateBeforeNoOffset >= m_globalMaxDate - m_globalMinDate + 1)
                assert(false);
            
            // reference to today and before stock days
            stockDay &data = m_stocks[i].m_data[date - m_stocks[i].m_minDate];
            stockDay &dataBefore = m_stocks[i].m_data[dateBefore - m_stocks[i].m_minDate];
            
            assert(data.m_open != 0.0);
            assert(dataBefore.m_open != 0.0);
            
            // avaliability check of the features
            vector<bool> isFeatureAvaliable;
            
            // check avaliability of all nDayMean features
            isFeatureAvaliable.push_back(true);
            for(int k=0;k<dayNMean.size();k++)
            {
                assert(dayNMean[k].m_data.size() == m_stocks[i].m_data.size());
                if(dayNMean[k].m_data[dateBefore-dayNMean[k].m_minDate].m_open == 0.0)
                {
                    isFeatureAvaliable[isFeatureAvaliable.size()-1] = false;
                    break;
                }
            }
            
            // check avaliability of differential features
            isFeatureAvaliable.push_back(true);
            for(int k=0;k<numberOfDaysInDifferentialFeatures;k++)
            {
                int datePast = requestDate(m_stocks[i], date-1-k);
                if(datePast == -1)
                    isFeatureAvaliable[isFeatureAvaliable.size()-1] = false;
            }
            
            // check avaliability of timeStockSVD features
            //for(int k=0;k<m_nFeatures;k++)
            //    if(m_timeStockSVDFeatures[dateBeforeNoOffset][i][k] == 0.0)
            //        timeStockSVDAvaliable = false;
            
            // check avaliability of timeSVD features
            //bool timeSVDAvaliable = true;
            //for(int k=0;k<m_nDayWindow;k++)
            //    for(int l=0;l<m_nFeatures;l++)
            //        if(m_timeSVDFeatures[dateBeforeNoOffset][k][l] == 0.0)
            //            timeSVDAvaliable = false;
            
            bool allFeaturesAvaliable = true;
            for(int k=0;k<isFeatureAvaliable.size();k++)
                allFeaturesAvaliable &= isFeatureAvaliable[k];
            
            if(allFeaturesAvaliable == false)
                continue;
            
            // ============================= features are added ===============================
            
            vector<float> feature;
            vector<float> target;
            int lastFeatPos = 0;
            
            assert(dataBefore.m_open != 0.0);
            assert(dataBefore.m_close != 0.0);
            assert(dataBefore.m_high != 0.0);
            assert(dataBefore.m_low != 0.0);
            assert(dataBefore.m_volume != 0);
            
            // === per stock: differential features, nDays in past ===
            for(int k=0;k<numberOfDaysInDifferentialFeatures;k++)
            {
                int datePast = requestDate(m_stocks[i], date-1-k);
                if(datePast == -1)  // no date before avaliable
                    assert(false);
                
                if(k==0 && datePast != dateBefore)
                    assert(false);
                
                stockDay &dataPast = m_stocks[i].m_data[datePast - m_stocks[i].m_minDate];
                
                float closeCloseDiff = limitToAbs((dataBefore.m_close - dataPast.m_close) / dataPast.m_close, 0.5);
                
                REAL maxSwing = 0.2;
                float openCloseDiff = limitToAbs((dataPast.m_close - dataPast.m_open) / dataPast.m_open, maxSwing);
                float lowHighDiff = limitToAbs((dataPast.m_high - dataPast.m_low) / dataPast.m_low, maxSwing);
                float openHighDiff = limitToAbs((dataPast.m_high - dataPast.m_open) / dataPast.m_open, maxSwing);
                float openLowDiff = limitToAbs((dataPast.m_low - dataPast.m_open) / dataPast.m_open, maxSwing);
                float closeHighDiff = limitToAbs((dataPast.m_high - dataPast.m_close) / dataPast.m_close, maxSwing);
                float closeLowDiff = limitToAbs((dataPast.m_low - dataPast.m_close) / dataPast.m_close, maxSwing);
                int volumeDiff = (double)((int)dataPast.m_volume - (int)dataPast.m_volume) / (double)dataPast.m_volume;
                
                if(k!=0)
                    feature.push_back(closeCloseDiff);
                
                feature.push_back(openCloseDiff);
                feature.push_back(lowHighDiff);
                feature.push_back(openHighDiff);
                feature.push_back(openLowDiff);
                feature.push_back(closeHighDiff);
                feature.push_back(closeLowDiff);
                //feature.push_back(volumeDiff);
            }
            checkFeature(feature);
            // HACK
            /*for(int k=lastFeatPos;k<feature.size();k++)
                cout<<k<<":"<<feature[k]<<" ";
            cout<<endl;
            lastFeatPos=feature.size();
            */
            
            // === per stock: #days lag to the first past data (1-of-n encoding) ===
            assert(date!=dateBefore && date-dateBefore < LOOKBACK_N_DAYS);
            feature.push_back(date-dateBefore == 1 ? 1.0 : -1.0);
            feature.push_back(date-dateBefore == 2 ? 1.0 : -1.0);
            feature.push_back(date-dateBefore == 3 ? 1.0 : -1.0);
            feature.push_back(date-dateBefore == 4 ? 1.0 : -1.0);
            
            // === per stock: which weekday (1-of-n encoding) ===
            assert(data.m_weekday>=1 && data.m_weekday <=5);  // trades are only on monday to friday
            feature.push_back(data.m_weekday == 1 ? 1.0 : -1.0);  // monday
            feature.push_back(data.m_weekday == 2 ? 1.0 : -1.0);  // tuesday
            feature.push_back(data.m_weekday == 3 ? 1.0 : -1.0);  // wednesday
            feature.push_back(data.m_weekday == 4 ? 1.0 : -1.0);  // thursday
            feature.push_back(data.m_weekday == 5 ? 1.0 : -1.0);  // friday
            checkFeature(feature);
            // HACK
            /*for(int k=lastFeatPos;k<feature.size();k++)
                cout<<k<<":"<<feature[k]<<" ";
            cout<<endl;
            lastFeatPos=feature.size();
            */
            
            // === per stock: difference to nDay averages ===
            for(int k=0;k<dayNMean.size();k++)
            {
                uint ind = dateBefore - dayNMean[k].m_minDate;
                assert(ind < dayNMean[k].m_data.size());
                assert(dayNMean[k].m_data[ind].m_open != 0.0);
                feature.push_back((dataBefore.m_close - dayNMean[k].m_data[ind].m_close) / dayNMean[k].m_data[ind].m_close);
            }
            checkFeature(feature);
            // HACK
            /*for(int k=lastFeatPos;k<feature.size();k++)
                cout<<k<<":"<<feature[k]<<" ";
            cout<<endl;
            lastFeatPos=feature.size();
            */
            
            /*
            // SVD features day : per day we have m_nDayWindow features
            //for(int k=0;k<m_nDayWindow;k++)
            for(int k=m_nDayWindow-1;k<m_nDayWindow;k++)
                for(int l=0;l<m_nFeatures;l++)
                    feature.push_back(m_timeSVDFeatures[dateBeforeNoOffset][k][l]);
            
            
            // SVD features stock
            for(int k=0;k<m_nFeatures;k++)
                feature.push_back(m_timeStockSVDFeatures[dateBeforeNoOffset][i][k]);
            */
            
            /*
            for(int k=0;k<feature.size();k++)
                //if(isnan(feature[k] || isinf(feature[k])))
                {
                    cout<<feature[k]<<" "<<flush;
                    //exit(0);
                }
                cout<<endl;
                */
            // volume dependent
            //feature.push_back(before->second.m_volume * before->second.m_close);
            //for(int j=0;j<dayNMeanDay.size();j++)
            //    feature.push_back(before->second.m_volume * dayNMeanDay[j]->second.m_close);
            /*feature.push_back(before->second.m_volume * before->second.m_close);
            feature.push_back(before->second.m_volume * day5MeanDay->second.m_close);
            feature.push_back(before->second.m_volume * day10MeanDay->second.m_close);
            feature.push_back(before->second.m_volume * day15MeanDay->second.m_close);
            feature.push_back(before->second.m_volume * day30MeanDay->second.m_close);
            feature.push_back(before->second.m_volume * day60MeanDay->second.m_close);
            */
            
            // === per stock: in last n days: #rise and #fall ===
            //                std over nDays
            //                NEI feature: (close-avg(nDays))/std(nDays)
            for(int k=1;k<numberOfDaysInLastNrRiseFallCnt;k++)
            {
                int riseCnt = 0, fallCnt = 0;
                double sum = 0.0, sum2 = 0.0, sumCnt = 0.0;
                for(int l=0;l<k;l++)
                {
                    int nPast = k-l;
                    int datePast = requestDate(m_stocks[i], date-1-nPast);
                    if(datePast == -1)
                        assert(false);
                    stockDay &dataPast = m_stocks[i].m_data[datePast - m_stocks[i].m_minDate];
                    if(dataPast.m_close > dataPast.m_open)
                        riseCnt++;
                    if(dataPast.m_close < dataPast.m_open)
                        fallCnt++;
                    sum += dataPast.m_close;
                    sum2 += dataPast.m_close * dataPast.m_close;
                    sumCnt += 1.0;
                }
                feature.push_back(riseCnt);
                feature.push_back(fallCnt);
                
                // binary encoding of past nDays #fall/#rise
                int datePast = requestDate(m_stocks[i], date-k);
                if(datePast == -1)
                    assert(false);
                stockDay &dataPast = m_stocks[i].m_data[datePast - m_stocks[i].m_minDate];
                if(dataPast.m_close > dataPast.m_open)
                    feature.push_back(1.0);  // rise
                else if(dataPast.m_close < dataPast.m_open)
                    feature.push_back(-1.0);  // fall
                else
                    feature.push_back(0.0);
                
                // std and NEI
                assert(sumCnt != 0.0);
                double std = sqrt(sum2/sumCnt - (sum/sumCnt)*(sum/sumCnt));
                if(isnan(std) || isinf(std) || std < 0.001)
                    std = 0.001;
                feature.push_back(std);  // std over nDays
                feature.push_back((dataBefore.m_close - sum/sumCnt)/std);  // NEI
            }
            checkFeature(feature);
            // HACK
            /*for(int k=lastFeatPos;k<feature.size();k++)
            cout<<k<<":"<<feature[k]<<" ";
            cout<<endl;
            lastFeatPos=feature.size();
            */
            
            // ===                                          ===
            // over all stocks: 1.) #fall/#rise daily
            //                  2.) std of (close-open)
            for(int k=0;k<numberOfDaysInDifferentialFeatures;k++)
            {
                int nrFall = 0, nrRise = 0;
                double sum = 0.0, sum2 = 0.0, sumCnt = 0.0;
                for(int l=0;l<m_stocks.size();l++)
                {
                    int datePast = requestDate(m_stocks[l], date-1-k);
                    if(datePast == -1)
                        continue;
                    stockDay &dataPast = m_stocks[l].m_data[datePast - m_stocks[l].m_minDate];
                    
                    double open = dataPast.m_open;
                    double close = dataPast.m_close;
                    double relativeDiff = (close-open)/open;
                    
                    sum += relativeDiff;
                    sum2 += relativeDiff * relativeDiff;
                    sumCnt += 1.0;
                    
                    if(close > open)
                        nrRise++;
                    if(close < open)
                        nrFall++;
                }
                if(nrFall+nrRise == 0)
                    assert(false);
                else
                    feature.push_back((double)nrFall/(double)(nrFall+nrRise));
                feature.push_back(sqrt(sum2/sumCnt-(sum/sumCnt)*(sum/sumCnt)));
            }
            checkFeature(feature);
            // HACK
            /*for(int k=lastFeatPos;k<feature.size();k++)
                cout<<k<<":"<<feature[k]<<" ";
            cout<<endl;
            lastFeatPos=feature.size();
            */
            
            assert(data.m_close != 0.0);
            assert(data.m_open != 0.0);
            
            // TARGETS
            // target: regression
            //target.push_back((data.m_close - data.m_open)/data.m_open);  // open-close diff : >0.0 when close is higher than open
            
            // target: classification
            //target.push_back((data.m_close - data.m_open)/data.m_open );  // open-close diff : >0.0 when close is higher than open
            //target.push_back( -((data.m_close - data.m_open)/data.m_open) );  // open-close diff : >0.0 when close is higher than open
            target.push_back(data.m_close - data.m_open > 0.0 ? +1.0 : -1.0);  // open-close diff : >0.0 when close is higher than open
            target.push_back(data.m_close - data.m_open > 0.0 ? -1.0 : +1.0);  // open-close diff : >0.0 when close is higher than open
            
            if(m_features.size() > 0)
            {
                int n0 = m_features[m_features.size()-1].size();
                int n1 = feature.size();
                if(n0 != n1)
                    assert(false);
            }
            m_features.push_back(feature);
            m_targets.push_back(target);
            m_dates.push_back(date);
            nFeat++;
            
            /*int ti = time(0)-tt;
            if(ti > 1)
            {
                cout<<endl<<ti<<endl;
                cout<<endl;
            }*/
        }
        //cout<<time(0)-t0<<"[s]"<<endl;
        t0 = time(0);
    }
    
    cout<<endl<<"#features:"<<m_features.size()<<" (dim:"<<m_features[0].size()<<")  #targets:"<<m_targets.size()<<" (dim:"<<m_targets[0].size()<<")"<<endl<<endl;
    
     // features
    /*dataVec[0] = (stockClose[j] - stockOpen[j])/stockOpen[j];  // open-close diff
    dataVec[1] = (stockHigh[j] - stockLow[j])/stockLow[j];  // low-high diff
    dataVec[2] = (stockHigh[j] - stockOpen[j])/stockOpen[j];  // open-high diff
    dataVec[3] = (stockLow[j] - stockOpen[j])/stockOpen[j];  // open-low diff
    dataVec[4] = (stockHigh[j] - stockClose[j])/stockClose[j];  // close-high diff
    dataVec[5] = (stockLow[j] - stockClose[j])/stockClose[j];  // close-low diff
    dataVec[6] = (stockClose[j] - stockClose10DayMean[j])/stockClose10DayMean[j];  // close-10dAvg diff
    dataVec[7] = stockWeekday[j] == 1.0? 1.0 : -1.0; // Monday
    dataVec[8] = stockWeekday[j] == 2.0? 1.0 : -1.0; // Tuesday
    dataVec[9] = stockWeekday[j] == 3.0? 1.0 : -1.0; // Wednesday
    dataVec[10] = stockWeekday[j] == 4.0? 1.0 : -1.0; // Thursday
    dataVec[11] = stockVolume[j]*stockClose[j];
    data.push_back(dataVec);
    target.push_back(targetVec);*/
}

void YahooFinance::readData(string path, REAL* &train, REAL* &trainTarget, int* &trainLabel, REAL* &test, REAL* &testTarget, int* &testLabel, uint& nTrain, uint& nTest, int& nClass, int& nDomain, int& nFeat, REAL positiveTarget, REAL negativeTarget, int &nValid, REAL* &valid, REAL* &validTarget, int* &validLabel)
{
    vector<string> symbols;
    vector<string> indices;
    
    // PUSH ALL INDICES, all contained individual stocks are automatically downloaded
    
    //indices.push_back("^GSPC");  // S&P 500 (USA)
    //indices.push_back("^GSPTSE"); // S&P/TSX Composite index (Interi (^GSPTSE)
    indices.push_back("^SML"); // S&P SMALLCAP 600 INDEX
    //indices.push_back("^GDAXI");  // DAX
    
    /*
    // =========== INDIZES DEUTSCHE BÖRSE =============  :  http://de.finance.yahoo.com/m8
    // === Auswahlindizes ===
    symbols.push_back("^GDAXI");   // DAX Index
    symbols.push_back("^TECDAX");  // TecDAX Index
    symbols.push_back("^MDAXI");   // MDAX Index
    symbols.push_back("^SDAXI");   // SDAX Index
    symbols.push_back("^GDAXHI");  // HDAX Index
    symbols.push_back("^MCAPM");   // Midcap Market Index
    symbols.push_back("ED6P.DE");  // DAX Entry Standard
    symbols.push_back("D1AR.DE");  // X-DAX
    symbols.push_back("^DXRPT.DE");   // DAXglobal Russia+ Index
    // === Volatilitätsindizes ===
    symbols.push_back("^VDAX");    // VDAX Index
    //symbols.push_back("V1X.DE");   // VDAX New Index
    // === Strategieindizes ===
    symbols.push_back("D1EP.DE");  // DAXplus Export Strategy
    symbols.push_back("D1AB.DE");  // DAXplus Seasonal Strategy
    symbols.push_back("D3CC.DE");  // DAXplus Covered Call
    symbols.push_back("^GSUL");    // DivDAX Index
    // === All Share Indizes ===
    symbols.push_back("^PRIME");   // Prime All Share Index
    symbols.push_back("^GEXI");    // GEX Index
    symbols.push_back("^TECALL");  // Technology All Share Index
    symbols.push_back("^CLALL");   // Classic All Share Index
    symbols.push_back("^CDAXX");   // CDAX Index
    // === Rentenindizes ===
    symbols.push_back("^GREXP");   // REX Index
    symbols.push_back("RXOR.DE");  // RDAX Index
    // === Internationale Indizes ===
    symbols.push_back("D1A1.DE");  // DAXglobal BRIC
    symbols.push_back("D1AT.DE");  // DBIX India Index
    // === Branchenindizes ===
    symbols.push_back("^CXPIX");    // Prime Insurance
    symbols.push_back("^CXPDX");    // Prime Media
    symbols.push_back("^CXPPX");    // Prime Pharma & Healthcare
    symbols.push_back("^CXPRX");    // Prime Retail
    symbols.push_back("^CXPSX");    // Prime Software
    symbols.push_back("^CXPHX");    // Prime Technology
    symbols.push_back("^CXPTX");    // Prime Telecommunication
    symbols.push_back("^CXPLX");    // Prime Transportation&Logistics
    symbols.push_back("^CXPUX");    // Prime Utilities
    symbols.push_back("^CXPAX");    // Prime Automobile
    symbols.push_back("^CXPBX");    // Prime Banks
    symbols.push_back("^CXPEX");    // Prime Basic Resources
    symbols.push_back("^CXPCX");    // Prime Chemicals
    symbols.push_back("^CXPOX");    // Prime Construction
    symbols.push_back("^CXPYX");    // Prime Consumer
    symbols.push_back("^CXPVX");    // Prime Financial Services
    symbols.push_back("^CXPFX");    // Prime Food & Beverages
    symbols.push_back("^CXPNX");    // Prime Industrial
    */
    
    /*
    // AOC is now: AON
    // http://ichart.yahoo.com/table.csv?s=AON&d=10&e=10&f=2020&g=d&a=0&b=3&c=1950&ignore=.csv
    CURL *curl;
    FILE *fp;
    CURLcode res;
    
    char buf[1024];
    string url, fName, stockName;
    int beginDay=3, beginMonth=0, beginYear = 1950;
    int endDay=22, endMonth=10, endYear = 2019;
    
    
    // READ THE INDIVIDUAL STOCKS (per index)
    // through all indices
    for ( int i=0;i<indices.size();i++ )
    {
        fName = path + "/" + indices[i] + ".csv";
        fp = fopen ( fName.c_str(),"wb" );
        url = "http://de.old.finance.yahoo.com/d/quotes.csv?s=@" + indices[i] + "&f=sl1d1t1c1ohgv&e=.csv";
        //http://de.old.finance.yahoo.com/d/quotes.csv?s=@%5ETECDAX&f=sl1d1t1c1ohgv&e=.csv
        curl = curl_easy_init();
        curl_easy_setopt ( curl, CURLOPT_URL, url.c_str() );
        curl_easy_setopt ( curl, CURLOPT_WRITEFUNCTION, write_data );
        curl_easy_setopt ( curl, CURLOPT_WRITEDATA, fp );
        res = curl_easy_perform ( curl );
        curl_easy_cleanup ( curl );
        fclose ( fp );
    
        // read the stocks
        int stockCnt = 0;
        fstream f ( fName.c_str(), ios::in );
        while ( f.getline ( buf,1024 ) )
        {
            int pos = 0;
            char* strPtr = buf;
            while ( *strPtr )
            {
                if ( *strPtr == ';' )
                {
                    *strPtr = 0;
                    break;
                }
                pos++;
                strPtr++;
            }
            string symbol = buf;
            cout<<stockCnt<<". "<<symbol<<endl;
            symbols.push_back ( symbol );
            stockCnt++;
        }
        f.close();
    }
    
    for ( int i=0;i<symbols.size() ;i++ )
    {
        stockName = symbols[i];
        cout<<i+1<<"/"<< ( int ) symbols.size() <<"  read: "<<stockName<<endl;
    
        fName = path + "/" + stockName + ".csv";
        fp = fopen ( fName.c_str(),"wb" );
        url = "http://ichart.yahoo.com/table.csv?s=";
        url += stockName + "&d=";
        sprintf ( buf,"%d",endMonth );
        url += string ( buf ) + "&e=";
        sprintf ( buf,"%d",endDay );
        url += string ( buf ) + "&f=";
        sprintf ( buf,"%d",endYear );
        url += string ( buf ) + "&g=d&a=";
        sprintf ( buf,"%d",beginMonth );
        url += string ( buf ) + "&b=";
        sprintf ( buf,"%d",beginDay );
        url += string ( buf ) + "&c=";
        sprintf ( buf,"%d",beginYear );
        url += string ( buf ) + "&ignore=.csv";
        curl = curl_easy_init();
        curl_easy_setopt ( curl, CURLOPT_URL, url.c_str() );
        curl_easy_setopt ( curl, CURLOPT_WRITEFUNCTION, write_data );
        curl_easy_setopt ( curl, CURLOPT_WRITEDATA, fp );
        res = curl_easy_perform ( curl );
        curl_easy_cleanup ( curl );
        fclose ( fp );
        
        sleep(1);
    }
    cout<<endl;
    */
    
    vector<string> filesTmp = Data::getDirectoryFileList ( path + "/" );
    vector<string> files;
    for(int i=0;i<filesTmp.size();i++)
    {
        string name = filesTmp[i];
        size_t pos = name.find_last_of('/')+1;
        int len = name.length() - pos - 4;
        if(len < 0)
            len = 0;
        name = name.substr(pos, len);
        bool check = false;
        for(int j=0;j<indices.size();j++)
            if(name == indices[j])
                check = true;
        if(check == false && len > 0)
            files.push_back(filesTmp[i]);
    }
    
    // read and parse the CSV stock data and read them to the member variable m_stocks
    sort(files.begin(), files.end());
    readCSVs(files);
    
    // make the features from the day-based data points
    makeFeatures();
    
    //=============== split the dataset to [train] [validation] [test] =================
    
    uint minDate = UINT_MAX, maxDate = 0;
    for(int i=0;i<m_dates.size();i++)
    {
        if(minDate > m_dates[i])
            minDate = m_dates[i];
        if(maxDate < m_dates[i])
            maxDate = m_dates[i];
    }
    cout<<"minDate:"<<minDate<<" maxDate:"<<maxDate<<endl;
    
    uint* hist = new uint[maxDate-minDate+1];
    // init and fill histogram
    for(int i=0;i<maxDate-minDate+1;i++)
        hist[i] = 0;
    for(int i=0;i<m_dates.size();i++)
        hist[m_dates[i]-minDate]++;
    
    // print start date
    time_t seconds = m_globalMinDate * 86400;
    struct tm * timeinfo;
    timeinfo = localtime ( &seconds );
    cout<<"Start date:"<<asctime (timeinfo);
    
    // histogram walk through
    uint cnt = 0, trainDate = 0, validDate = 0, testDate = 0, trainCnt = 0, testCnt = 0, validCnt = 0;
    for(int i=0;i<maxDate-minDate+1;i++)
    {
        cnt += hist[i];
        if(cnt > (int)((double)m_dates.size()*(1.0-PERCENT_TEST-PERCENT_VALIDATION)) && trainCnt == 0 )
        {
            trainDate = i+minDate;
            trainCnt = cnt;
            cout<<"Train set up to including date:"<<trainDate<<"  trainCnt:"<<trainCnt<<"  -> ";
            time_t seconds = trainDate * 86400;
            struct tm * timeinfo;
            timeinfo = localtime ( &seconds );
            cout<<asctime (timeinfo);
        }
        else if(cnt > (int)((double)m_dates.size()*(1.0-PERCENT_TEST)) && validCnt == 0 )
        {
            validDate = i+minDate;
            validCnt = cnt - trainCnt;
            cout<<"Valid set up to including date:"<<validDate<<"  validCnt:"<<validCnt<<"  -> ";
            time_t seconds = validDate * 86400;
            struct tm * timeinfo;
            timeinfo = localtime ( &seconds );
            cout<<asctime (timeinfo);
        }
        else if(i == maxDate-minDate)
        {
            testDate = i+minDate;
            testCnt = cnt - (validCnt+trainCnt);
            cout<<"Test set up to including date:"<<testDate<<"  testCnt:"<<testCnt<<"  -> ";
            time_t seconds = testDate * 86400;
            struct tm * timeinfo;
            timeinfo = localtime ( &seconds );
            cout<<asctime (timeinfo);
        }
    }
    delete[] hist;
    
    nFeat = m_features[0].size();
    nClass = m_targets[0].size();
    train = new REAL[trainCnt*nFeat];
    trainTarget = new REAL[trainCnt*nClass];
    valid = new REAL[validCnt*nFeat];
    validTarget = new REAL[validCnt*nClass];
    test = new REAL[testCnt*nFeat];
    testTarget = new REAL[testCnt*nClass];
    
    uint cnt0 = 0, cnt1 = 0, cnt2 = 0;
    for(uint i=0;i<m_features.size();i++)
    {
        if(m_dates[i] <= trainDate)
        {
            if(m_dates[i] > trainDate || m_dates[i] < minDate)
                assert(false);
            for(uint j=0;j<nFeat;j++)
                train[cnt0*nFeat+j] = m_features[i][j];
            for(uint j=0;j<nClass;j++)
                trainTarget[cnt0*nClass+j] = m_targets[i][j];
            cnt0++;
        }
        else if(m_dates[i] <= validDate)
        {
            if(m_dates[i] > validDate || m_dates[i] <= trainDate)
                assert(false);
            for(uint j=0;j<nFeat;j++)
                valid[cnt1*nFeat+j] = m_features[i][j];
            for(uint j=0;j<nClass;j++)
                validTarget[cnt1*nClass+j] = m_targets[i][j];
            cnt1++;
        }
        else
        {
            if(m_dates[i] > testDate || m_dates[i] <= validDate)
                assert(false);
            for(uint j=0;j<nFeat;j++)
                test[cnt2*nFeat+j] = m_features[i][j];
            for(uint j=0;j<nClass;j++)
                testTarget[cnt2*nClass+j] = m_targets[i][j];
            cnt2++;
        }
    }
    
    trainLabel = 0;
    validLabel = 0;
    testLabel = 0;
    nTrain = trainCnt;
    nValid = validCnt;
    nTest = testCnt;
    nDomain = 1;
    
    if(nClass == 2)
    {
        cout<<"Classification problem (2-class)"<<endl;
        
        trainLabel = new int[trainCnt];
        int posCnt = 0, negCnt = 0;
        for(uint i=0;i<trainCnt;i++)
        {
            if(trainTarget[i*2] > trainTarget[i*2+1] )
            {
                trainLabel[i] = 0;
                posCnt++;
            }
            else
            {
                trainLabel[i] = 1;
                negCnt++;
            }
        }
        cout<<"Train:  posCnt:"<<posCnt<<" negCnt:"<<negCnt<<"  pos/(pos+neg):"<<(double)posCnt/(double)(posCnt+negCnt)<<endl;
        
        validLabel = new int[validCnt];
        posCnt = 0; negCnt = 0;
        for(uint i=0;i<validCnt;i++)
        {
            if(validTarget[i*2] > validTarget[i*2+1])
            {
                validLabel[i] = 0;
                posCnt++;
            }
            else
            {
                validLabel[i] = 1;
                negCnt++;
            }
        }
        cout<<"Valid:   posCnt:"<<posCnt<<" negCnt:"<<negCnt<<"  pos/(pos+neg):"<<(double)posCnt/(double)(posCnt+negCnt)<<endl;
        
        testLabel = new int[testCnt];
        posCnt = 0; negCnt = 0;
        for(uint i=0;i<testCnt;i++)
        {
            if(testTarget[i*2] > testTarget[i*2+1])
            {
                testLabel[i] = 0;
                posCnt++;
            }
            else
            {
                testLabel[i] = 1;
                negCnt++;
            }
        }
        cout<<"Test:   posCnt:"<<posCnt<<" negCnt:"<<negCnt<<"  pos/(pos+neg):"<<(double)posCnt/(double)(posCnt+negCnt)<<endl;
    }
    else if(nClass == 1)
    {
        cout<<"Regression problem"<<endl;
        
        double sum = 0.0, sum2 = 0.0;
        for(uint i=0;i<trainCnt;i++)
        {
            sum += trainTarget[i];
            sum2 += trainTarget[i] * trainTarget[i];
        }
        cout<<"Train:  meanTarget:"<<sum/(double)trainCnt<<"  stdDev:"<<sqrt(sum2/(double)trainCnt - (sum/(double)trainCnt)*(sum/(double)trainCnt))<<endl;
        sum = 0.0;
        sum2 = 0.0;
        for(uint i=0;i<testCnt;i++)
        {
            sum += testTarget[i];
            sum2 += testTarget[i] * testTarget[i];
        }
        cout<<"Test:   meanTarget:"<<sum/(double)testCnt<<"  stdDev:"<<sqrt(sum2/(double)testCnt - (sum/(double)testCnt)*(sum/(double)testCnt))<<endl;
        sum = 0.0;
        sum2 = 0.0;
        for(uint i=0;i<validCnt;i++)
        {
            sum += validTarget[i];
            sum2 += validTarget[i] * validTarget[i];
        }
        cout<<"Valid:  meanTarget:"<<sum/(double)validCnt<<"  stdDev:"<<sqrt(sum2/(double)testCnt - (sum/(double)testCnt)*(sum/(double)testCnt))<<endl;
    }
}

REAL YahooFinance::limitToAbs(REAL v, REAL limit)
{
    if(v < -limit)
        v = -limit;
    if(v > limit)
        v = limit;
    return v;
}
