#ifndef __AUTOENCODER_
#define __AUTOENCODER_

#include "Algorithm.h"
#include "AutomaticParameterTuner.h"
#include "StandardAlgorithm.h"
#include "Framework.h"
#include "nnrbm.h"

using namespace std;

/**
 * Perform: dimensionality reduction of the data
 *
 * This is for preprocessing the dataset
 * Here, a RBM is used to initialize the weight of a deep neural network
 * Finally, the net gets finetuned by standard backprop with stochastic gradient descent
 * Stopping criteria of finetuning is minimum reconstruction RMSE on the cross validation set
 *
 */

class Autoencoder : public Algorithm, public AutomaticParameterTuner, public Framework
{
public:
    Autoencoder();
    ~Autoencoder();

    virtual double train();
    void readMaps();
    void modelInit();
    void modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun );
    void predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun );
    void readSpecificMaps();
    void saveWeights();
    void loadWeights();
    virtual double calcRMSEonProbe();
    virtual double calcRMSEonBlend()
    {
        return 0.0;
    };
    virtual void setPredictionMode ( int cross ) {};
    virtual void predictMultipleOutputs ( REAL* rawInput, REAL* effect, REAL* output, int* label, int nSamples, int crossRun ) {};
    virtual void saveBestPrediction() {};

    void readDataset ( Data* data, string datasetName, bool enlargeFeaturesWithCodeLayer = false );
    void loadNormalizations();

    static string templateGenerator ( int id, string preEffect, int nameID, bool blendStop );

private:
    // inputs/targets
    REAL** m_inputs;

    // the NNs
    NNRBM** m_nn;
    int m_epoch;

    // RBM setup
    bool *m_isFirstEpoch;

    // dsc file
    int m_maxTuninigEpochs;
    int m_minTuninigEpochs;
    int m_nrLayer;
    int m_batchSize;
    int m_nFixEpochs;
    double m_offsetOutputs;
    double m_scaleOutputs;
    double m_initWeightFactor;
    double m_learnrate;
    double m_learnrateMinimum;
    double m_learnrateSubtractionValueAfterEverySample;
    double m_learnrateSubtractionValueAfterEveryEpoch;
    double m_momentum;
    double m_weightDecay;
    double m_minUpdateErrorBound;
    double m_etaPosRPROP;
    double m_etaNegRPROP;
    double m_minUpdateRPROP;
    double m_maxUpdateRPROP;
    bool m_enableL1Regularization;
    bool m_enableErrorFunctionMAE;
    bool m_enableRPROP;
    bool m_useBLASforTraining;
    string m_neuronsPerLayer;

    REAL* m_meanRBM;
    REAL* m_stdRBM;
};


#endif
