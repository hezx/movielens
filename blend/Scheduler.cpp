#include "Scheduler.h"
#include <stdlib.h>

extern StreamOutput cout;

/**
 * Constructor
 */
Scheduler::Scheduler()
{
    cout<<"Scheduler"<<endl;
    // init member vars
    m_data = 0;
    m_blender = 0;
    m_blenderNN = 0;
    m_labelsPredict = 0;
    m_effectID = 0;
    m_noEffect = 0;
    m_outputs = 0;
    m_effects = 0;
    m_predictionRMSE = 0;
    m_predictionClassificationError = 0;

    m_data = new Data();
    m_data->setPathes ( TMP_PATH, DSC_PATH, FULL_PREDICTOR_PATH, DATA_PATH );

    m_baggingRun = 0;
    m_boostingRun = 0;
    m_randSeedBagBoost = 0;
    m_probs = 0;
    m_boostingTrain = 0;
    m_boostingTargets = 0;
    m_boostingNTrain = 0;
    m_gradientBoostingTargetMean = 0;
    m_gradientBoostingOutputSum = 0;
}

/**
 * Destructor
 */
Scheduler::~Scheduler()
{
    cout<<"descructor Scheduler"<<endl;
    if ( m_data )
        delete m_data;
    m_data = 0;
}

/**
 * Read the master description file
 * The master file set up some initial train settings, dataset name and train ordering
 *
 * @param fname Name of master-dsc file (string)
 */
void Scheduler::readMasterDscFile ( string path, string masterName )
{
    m_data->m_datasetPath = path;
    cout<<"Open master .dsc file:"<<(path + "/" + masterName)<<endl;
    fstream fMaster ( (path + "/" + masterName).c_str(), ios::in );

    // check is file exists
    if ( fMaster.is_open() == 0 )
    {
        cout<<"Error: no Master.dsc file found in "<<path<<endl;
        exit ( 0 );
    }

    // read all lines
    char buf[1024];
    bool readAlgorithmList = false;
    while ( fMaster.getline ( buf, 1024 ) ) // read all lines
    {
        // the line
        string line = string ( buf );

        // an empty line or comments
        if ( line=="" || line[0]=='#' )
            continue;

        // read the algorithm dsc files
        if ( readAlgorithmList )
        {
            m_algorithmList.push_back ( line );
            continue;
        }

        // list of algorithm dsc files begins
        if ( line=="[ALGORITHMS]" )
        {
            readAlgorithmList = true;
            continue;
        }

        // split into 2 strings at the '=' char
        int pos = line.find ( "=" );
        string name = line.substr ( 0, pos );
        string value = line.substr ( pos+1 );

        // read the meta training values
        if ( name=="dataset" )
            m_data->m_datasetName = value;
        if ( name=="isClassificationDataset" )
            Framework::setDatasetType ( atoi ( value.c_str() ) );
        if ( name=="maxThreads" )
        {
            cout<<"Set max. threads in MKL and IPP: "<<atoi ( value.c_str() ) <<endl;
            mkl_set_num_threads ( atoi ( value.c_str() ) );
            ippSetNumThreads ( atoi ( value.c_str() ) );
        }
        if ( name=="maxThreadsInCross" )
        {
            Framework::setMaxThreads ( atoi ( value.c_str() ) );  // store max. number of threads
            m_data->m_maxThreadsInCross = atoi ( value.c_str() );  // #threads in cross-fold-validation
        }
        if ( name=="nCrossValidation" )
        {
            m_data->m_nCross = atoi ( value.c_str() );
            cout<<"Train "<<m_data->m_nCross<<"-fold cross validation"<<endl;
        }
        if ( name=="validationType" )
        {
            assert ( value=="Retraining" || value=="CrossFoldMean" || value=="Bagging" || value=="ValidationSet");
            m_data->m_validationType = value;
            Framework::setValidationType(value);
            cout<<"ValidationType: "<<value<<endl;
        }
        if ( name=="positiveTarget" )
            m_data->m_positiveTarget = atof ( value.c_str() );
        if ( name=="negativeTarget" )
            m_data->m_negativeTarget = atof ( value.c_str() );
        if ( name=="standardDeviationMin" )
            m_data->m_standardDeviationMin = atof ( value.c_str() );
        if ( name=="randomSeed" )
        {
            if ( value=="time(0)" )
                m_data->m_randSeed = time ( 0 );
            else
                m_data->m_randSeed = atoi ( value.c_str() );
            cout<<"Set random seed to: "<<m_data->m_randSeed<<endl;
            setRandomSeed ( m_data->m_randSeed );
        }
        if ( name=="nMixDataset" )
            m_data->m_nMixDataset = atoi ( value.c_str() );
        if ( name=="nMixTrainList" )
            m_data->m_nMixTrainList = atoi ( value.c_str() );
        if ( name=="blendingRegularization" )
            m_data->m_blendingRegularization = atof ( value.c_str() );
        if ( name=="blendingAlgorithm" )
            m_data->m_blendingAlgorithm = value;
        if ( name=="blendingEnableCrossValidation" )
            m_data->m_blendingEnableCrossValidation = atoi ( value.c_str() );
        if ( name=="enablePostNNBlending" )
            m_data->m_enablePostNNBlending = atoi ( value.c_str() );
        if ( name=="enableCascadeLearning" )
            m_data->m_enableCascadeLearning = atoi ( value.c_str() );
        if ( name=="enableGlobalMeanStdEstimate" )
            m_data->m_enableGlobalMeanStdEstimate = atoi ( value.c_str() );
        if ( name=="enableSaveMemory" )
            m_data->m_enableSaveMemory = atoi ( value.c_str() );
        if ( name=="errorFunction" )
            m_data->m_errorFunction = value;
        if ( name=="enablePostBlendClipping" )
            m_data->m_enablePostBlendClipping = atoi ( value.c_str() );
        if ( name=="addOutputNoise" )
            m_data->m_addOutputNoise = atof ( value.c_str() );
        if ( name=="enableFeatureSelection" )
            m_data->m_enableFeatureSelection = atoi ( value.c_str() );
        if ( name=="featureSelectionWriteBinaryDataset" )
            m_data->m_featureSelectionWriteBinaryDataset = atoi ( value.c_str() );
        if ( name=="enableGlobalBlendingWeights" )
            m_data->m_enableGlobalBlendingWeights = atoi ( value.c_str() );
        if ( name=="disableWriteDscFile" )
        {
            m_data->m_disableWriteDscFile = atoi ( value.c_str() );
            if ( m_data->m_disableWriteDscFile )
                cout.disableFileOutputs();
        }
        if ( name=="enableStaticNormalization" )
            m_data->m_enableStaticNormalization = atoi ( value.c_str() );
        if ( name=="staticMeanNormalization" )
            m_data->m_staticMeanNormalization = atof ( value.c_str() );
        if ( name=="staticStdNormalization" )
            m_data->m_staticStdNormalization = atof ( value.c_str() );
        if ( name=="enableProbablisticNormalization" )
            m_data->m_enableProbablisticNormalization = atoi ( value.c_str() );
        if ( name=="addAutoencoderFeatures" )
            m_data->m_addAutoencoderFeatures = atoi ( value.c_str() );
        if ( name=="dimensionalityReduction" )
            m_data->m_dimensionalityReduction = value;
        if ( name=="subsampleTrainSet" )
            m_data->m_subsampleTrainSet = atof ( value.c_str() );
        if ( name=="subsampleFeatures" )
            m_data->m_subsampleFeatures = atof ( value.c_str() );
        if ( name=="globalTrainingLoops" )
            m_data->m_globalTrainingLoops = atoi ( value.c_str() );
        if ( name=="addConstantInput" )
            m_data->m_addConstantInput = atoi ( value.c_str() );
        if ( name=="gradientBoostingMaxEpochs" )
        {
            m_data->m_gradientBoostingMaxEpochs = atoi ( value.c_str() );
            Framework::setGradientBoostingMaxEpochs( m_data->m_gradientBoostingMaxEpochs );
        }
        if ( name=="gradientBoostingLearnrate" )
            m_data->m_gradientBoostingLearnrate = atof ( value.c_str() );
        if ( name=="rejectConstantDataColumns" )
            m_data->m_rejectConstantDataColumns = atoi ( value.c_str() );
        if ( name=="noNormalizationForBinaryColumns" )
            m_data->m_noNormalizationForBinaryColumns = atoi ( value.c_str() );
    }

    fMaster.close();
}

/**
 * Train the stack of Algorithms (described is dsc files itself)
 *
 */
void Scheduler::train()
{
    Framework::setFrameworkMode ( 0 );

    cout<<"Start scheduled training"<<endl;

    // fill the data object with the dataset
    cout<<"Fill data"<<endl;

    // autoencoder file objects
    fstream fA0 ( ( m_data->m_datasetPath + "/" + m_data->m_tempPath + "/AutoencoderDataMean.dat" ).c_str(), ios::in );
    fstream fA1 ( ( m_data->m_datasetPath + "/" + m_data->m_tempPath + "/AutoencoderDataStd.dat" ).c_str(), ios::in );
    fstream fA2 ( ( m_data->m_datasetPath + "/" + m_data->m_tempPath + "/AutoencoderWeights.dat" ).c_str(), ios::in );

    bool autoencoderFilesOK = fA0.is_open() && fA1.is_open() && fA2.is_open();
    fA0.close();
    fA1.close();
    fA2.close();
    
    // perform: reduce the dimensionalty of data
    if ( m_data->m_dimensionalityReduction == "Autoencoder" && autoencoderFilesOK == false )
    {
        cout<<"Autoencoder: start training"<<endl;
        
        // fix random seed
        srand ( m_data->m_randSeed );

        // read dataset
        m_data->readDataset ( m_data->m_datasetName );
        m_data->mergeTrainAndTest();
        m_data->mixDataset();
        
        // prepare cross-fold validation
        m_data->allocMemForCrossValidationSets();
        
        // save mean and std for later dim. reduction
        cout<<"Save mean and std for later dim. reduction"<<endl;
        fstream f0((m_data->m_datasetPath + "/" + m_data->m_tempPath + "/AutoencoderDataMeanPre.dat").c_str(),ios::out);
        f0.write((char*)m_data->m_mean, sizeof(REAL)*m_data->m_nFeatures);
        f0.close();
        f0.open((m_data->m_datasetPath + "/" + m_data->m_tempPath + "/AutoencoderDataStdPre.dat").c_str(),ios::out);
        f0.write((char*)m_data->m_std, sizeof(REAL)*m_data->m_nFeatures);
        f0.close();
        
        // train algorithm
        trainAlgorithm ( m_data->m_datasetPath + "/Autoencoder.dsc", m_data->m_datasetPath + "/" + string ( DSC_PATH ) + "/Autoencoder.dsc" );

        // clear mem
        m_data->deleteMemory();
        
        cout<<"Exit now. Please restart for using the dim. reduction."<<endl;
        exit(0);
    }

    // optimize sequentially the whole ensemble
    // 1st run: build initial ensemble
    // 2nd...endRun: optimize each algorithm's metaparameters
    time_t totalTime = time(0);
    cout<<"gradientBoostingLoops:"<<m_data->m_gradientBoostingMaxEpochs<<endl;
    for(int gradientBoostingLoop=0;gradientBoostingLoop<m_data->m_gradientBoostingMaxEpochs;gradientBoostingLoop++)
    {
        // set current gradient boosting epoch
        Framework::setGradientBoostingEpoch(gradientBoostingLoop);
        
        if(m_data->m_gradientBoostingMaxEpochs > 1)
        {
            cout<<"==================================================="<<endl;
            cout<<"|| gradientBoostingLoop:"<<gradientBoostingLoop<<endl;
            cout<<"==================================================="<<endl;
            cout<<"globalTrainingLoops:"<<m_data->m_globalTrainingLoops<<endl;
        }
        for ( int globalLoop=0;globalLoop<m_data->m_globalTrainingLoops;globalLoop++ )
        {
            // train all template files
            for ( int i=0;i<m_algorithmList.size();i++ )
            {
                //m_data->m_randSeed+=i;
                
                // fix random seed
                srand ( m_data->m_randSeed );
                
                // read dataset
                if ( m_data->m_dimensionalityReduction == "Autoencoder" )
                {
                    Autoencoder a;
                    a.setDataPointers ( m_data );
                    
                    // fix random seed
                    srand ( m_data->m_randSeed );
                    
                    a.readDataset ( m_data, m_data->m_datasetName, m_data->m_addAutoencoderFeatures);
                }
                else
                    m_data->readDataset ( m_data->m_datasetName );
                
                // enlarge dataset with external features
                //if(m_data->m_addAutoencoderFeatures)
                //    m_data->addExternalFeatures();
                
                // bagging: modify the trainset in retraining
                m_data->enableBagging ( m_baggingRun );
                m_data->baggingRandomSeed ( m_randSeedBagBoost );
                
                // copy train data for later evaluation
                if ( m_boostingRun )
                {
                    if ( m_probs == 0 )
                    {
                        cout<<"Init bootstrap probabilities to 1/N"<<endl;
                        m_probs = new REAL[m_data->m_nTrain];
                        for ( int j=0;j<m_data->m_nTrain;j++ )
                            m_probs[j] = 1.0 / ( ( REAL ) m_data->m_nTrain );
                    }
                    if ( m_boostingTrain==0 )
                    {
                        cout<<"Copy train set (features + targets) to boosting trainset"<<endl;
                        m_boostingNTrain = m_data->m_nTrain;
                        m_boostingTrain = new REAL[m_data->m_nTrain*m_data->m_nFeatures];
                        m_boostingTargets = new REAL[m_data->m_nTrain*m_data->m_nClass*m_data->m_nDomain];
                        memcpy ( m_boostingTrain, m_data->m_trainOrig, sizeof ( REAL ) *m_data->m_nTrain*m_data->m_nFeatures );
                        memcpy ( m_boostingTargets, m_data->m_trainTargetOrig, sizeof ( REAL ) *m_data->m_nTrain*m_data->m_nClass*m_data->m_nDomain );
                    }
    
                    if ( m_boostingEpoch > 0 )
                        m_data->doBootstrapSampling ( m_probs,m_data->m_trainOrig,m_data->m_trainTargetOrig,m_data->m_trainTargetOrigEffect,m_data->m_trainTargetOrigResidual,m_data->m_trainLabelOrig );
                }
    
                srand ( m_data->m_randSeed );
    
                // set the list of already trained predictors
                if ( globalLoop == 0 )
                    m_data->setAlgorithmList ( vector<string> ( m_algorithmList.begin(), m_algorithmList.begin() +i ) );
                else
                {
                    vector<string> tmp;
                    for ( int j=0;j<m_algorithmList.size();j++ )
                        if ( j != i )
                            tmp.push_back ( m_algorithmList[j] );
                    m_data->setAlgorithmList ( tmp );
                }
    
                time_t beginTime = time ( 0 );
    
                // extend input features with previous predictions
                if ( m_data->m_enableCascadeLearning )
                {
                    if(m_data->m_validationType=="ValidationSet")
                        assert(false);
                    m_data->fillCascadeLearningInputs();
                    m_data->extendTrainDataWithCascadeInputs();
                }
    
                // algorithm dsc file (template)
                string fAlgoTemplateName = m_data->m_datasetPath + "/" + m_algorithmList[i];
                string fAlgoName = m_data->m_datasetPath + "/" + string ( DSC_PATH ) + "/" + m_algorithmList[i];
                
                // normalize data
                m_data->readDscFile ( fAlgoTemplateName );
                m_data->allocMemForCrossValidationSets();
    
                /*fstream fAlgoTemplate(fAlgoTemplateName.c_str(), ios::in);
    
                // open the dsc file
                fstream fAlgo(fAlgoName.c_str(), ios::out);
    
                cout<<"AlgoTemplate:"<<fAlgoTemplateName<<"  Algo:"<<fAlgoName<<endl;
    
                // copy the content from the template to the dsc file
                char buf[1024];
                while(fAlgoTemplate.getline(buf, 1024))  // read all lines
                {
                    string line = string(buf);
                    fAlgo<<line<<endl;
                }
    
                fAlgoTemplate.close();
                fAlgo.close();
    
                // redirect cout to filename
                cout.setOutputFile(fAlgoName);
    
                cout<<"Floating point precision: "<<(int)sizeof(REAL)<<" Bytes"<<endl;
                */
                
                // =========================== train the algorithm ===========================
                if ( globalLoop > 0 )
                    m_data->m_loadWeightsBeforeTraining = true;
    
                trainAlgorithm ( fAlgoTemplateName, fAlgoName );
                cout<<"Finished in "<<time ( 0 )-beginTime<<"[s]"<<endl;
    
                // clear file redirection of cout<<
                cout.setOutputFile ( "" );
    
                // clear mem
                m_data->deleteMemory();
            }
        }
    }

    cout<<"Total training time:"<<time(0)-totalTime<<"[s]"<<endl;
    
    if ( m_data->m_enablePostNNBlending )
    {
        // set the list of already trained predictors
        m_data->setAlgorithmList ( vector<string> ( m_algorithmList.begin(), m_algorithmList.end() ) );

        // fix random seed
        srand ( m_data->m_randSeed );

        // read dataset
        m_data->readDataset ( m_data->m_datasetName );
        srand ( m_data->m_randSeed );
        m_data->allocMemForCrossValidationSets();
        m_data->partitionDatasetToCrossValidationSets();

        m_data->readDscFile ( m_data->m_datasetPath + "/NeuralNetwork.dscblend" );

        BlendingNN nn;
        nn.setDataPointers ( m_data );
        nn.readSpecificMaps();
        nn.init();
        nn.train();

        // clear mem
        m_data->deleteMemory();
    }
}

/**
 * Blend the predictions with a neural network
 *
 */
void Scheduler::blend()
{
    Framework::setFrameworkMode ( 0 );

    cout<<"Start blending after training"<<endl;

    // set the list of already trained predictors
    m_data->setAlgorithmList ( vector<string> ( m_algorithmList.begin(), m_algorithmList.end() ) );

    // fix random seed
    srand ( m_data->m_randSeed );

    // fill the data object with the dataset
    cout<<"Fill data"<<endl;
    m_data->readDataset ( m_data->m_datasetName );
    srand ( m_data->m_randSeed );
    m_data->allocMemForCrossValidationSets();
    m_data->partitionDatasetToCrossValidationSets();

    if ( m_data->m_enablePostNNBlending )
    {
        m_data->readDscFile ( m_data->m_datasetPath + "/NeuralNetwork.dscblend" );

        BlendingNN nn;
        nn.setDataPointers ( m_data );
        nn.readSpecificMaps();
        nn.init();
        nn.train();
    }
    else
    {
        BlendStopping bb ( ( Algorithm* ) m_data, "" );
        bb.setRegularization ( m_data->m_blendingRegularization );
        double rmse = bb.calcBlending();
        cout<<"BLEND RMSE OF ACTUAL FULLPREDICTION PATH:"<<rmse<<endl;
        bb.saveBlendingWeights ( m_data->m_datasetPath + "/" + m_data->m_tempPath );
    }
}


/**
 * Predict the testset
 * save the predictions to a binary file: out.dat
 *
 */
void Scheduler::predict()
{
    Framework::setFrameworkMode ( 1 );

    preparePredictionMode();

    int progress = m_data->m_nTest / 100 + 1;
    double mean = 0.0, rmse = 0.0;

    // output file (binary)
    string fname;
    string fname2; // normal output
    if ( m_data->m_datasetName=="NETFLIX" && Framework::getAdditionalStartupParameter() >= 0 )
    {
        cout<<"Dataset:NETFLIX, slot:"<<Framework::getAdditionalStartupParameter() <<" ";
        char buf[512];
        sprintf ( buf,"p%d",Framework::getAdditionalStartupParameter() );
        fname = string ( NETFLIX_SLOTDATA_ROOT_DIR ) + buf + "/testPrediction.data";
        cout<<"pName:"<<fname<<endl;
    }
    else if ( m_data->m_datasetName=="NETFLIX" && Framework::getAdditionalStartupParameter() < -100 )
    {
        char buf[512];
        sprintf ( buf,"ELFprediction%d",Framework::getRandomSeed() );
        string algos;
        for ( int i=0;i<m_algorithmList.size();i++ )
            algos += "_" + m_algorithmList[i];
        fname = m_data->m_datasetPath + "/" + m_data->m_tempPath + "/" + buf + algos + ".dat";
        cout<<"pName:"<<fname<<endl;
    }
    else
    {
        char nr[512];
        sprintf ( nr,"%d",rand() );
        fname = m_data->m_datasetPath + "/" + m_data->m_tempPath + "/testPrediction" + string ( nr ) + ".data";
        fname2 = m_data->m_datasetPath + "/" + m_data->m_tempPath + "/testPrediction" + string ( nr ) + "_csv.data";
        //fname = m_data->m_datasetPath + "/" + m_data->m_tempPath + "/testPrediction.data";
    }

    fstream fOutput ( fname.c_str(),ios::out );
    fstream fOutput2 ( fname2.c_str(),ios::out );

    // the output vector of the ensemble
    REAL* ensembleOutput = new REAL[m_data->m_nClass*m_data->m_nDomain];

    int* wrongLabelCnt = new int[m_data->m_nDomain];
    for ( int i=0;i<m_data->m_nDomain;i++ )
        wrongLabelCnt[i] = 0;

    // store the real input dimension of data
    int nrFeat = m_data->m_nFeatures;

    m_outputVectorTmp = new REAL[m_data->m_nClass*m_data->m_nDomain];
    m_labelsTmp = new int[m_data->m_nClass*m_data->m_nDomain];

    // load the autoencoder net
    Autoencoder* autoEnc = 0;
    bool enableAutoencoder = false;
    REAL* autoencoderOutput = 0, *autoencoderMeanPre = 0, *autoencoderStdPre = 0, *autoencoderInputFeature = 0;
    
    if ( m_data->m_dimensionalityReduction == "Autoencoder" )
    {
        autoencoderMeanPre = new REAL[m_data->m_nFeatures];
        autoencoderStdPre = new REAL[m_data->m_nFeatures];
        autoencoderInputFeature = new REAL[m_data->m_nFeatures];
        fstream f0((m_data->m_datasetPath + "/" + m_data->m_tempPath + "/AutoencoderDataMeanPre.dat").c_str(),ios::in);
        assert(f0.is_open());
        f0.read((char*)autoencoderMeanPre, sizeof(REAL)*m_data->m_nFeatures);
        f0.close();
        f0.open((m_data->m_datasetPath + "/" + m_data->m_tempPath + "/AutoencoderDataStdPre.dat").c_str(),ios::in);
        assert(f0.is_open());
        f0.read((char*)autoencoderStdPre, sizeof(REAL)*m_data->m_nFeatures);
        f0.close();
        
        autoEnc = new Autoencoder();
        autoEnc->setDataPointers ( m_data );
        autoEnc->loadWeights();
        autoEnc->loadNormalizations();
        enableAutoencoder = true;
        autoencoderOutput = new REAL[autoEnc->m_nClass];
        if(m_data->m_addAutoencoderFeatures)
            m_data->m_nFeatures += autoEnc->m_nClass;  //  modify input dimension
        else
            m_data->m_nFeatures = autoEnc->m_nClass;  //  modify input dimension
    }

    // enlarge dataset with external features
    //if(m_data->m_addAutoencoderFeatures)
    //    m_data->addExternalFeatures();
    
    cout<<endl<<"predict(100 dots): "<<flush;
    time_t t0 = time ( 0 );

    // confusion matrix
    uint* confusionMatrix = 0;
    if(m_data->m_nDomain == 1)
    {
        confusionMatrix = new uint[m_data->m_nClass*m_data->m_nClass];
        for(uint i=0;i<m_data->m_nClass*m_data->m_nClass;i++)
            confusionMatrix[i] = 0;
    }
    
    // predict every sample in the test set
    for ( uint i=0;i<m_data->m_nTest;i++ )
    {
        if ( i % progress == 0 )
            cout<<"."<<flush;

        // predict one example
        REAL* inputFeature = m_data->m_testOrig + i * ( uint ) nrFeat;

        if ( enableAutoencoder )
        {
            // prepare normalized feature
            for(int j=0;j<nrFeat;j++)
                autoencoderInputFeature[j] = ( inputFeature[j] - autoencoderMeanPre[j] ) / autoencoderStdPre[j];
            
            autoEnc->predictAllOutputs ( autoencoderInputFeature, autoencoderOutput, 1, 0 );
            if(m_data->m_addAutoencoderFeatures)
            {
                REAL* outputMerged = new REAL[autoEnc->m_nClass + nrFeat];
                for(int j=0;j<nrFeat;j++)
                    outputMerged[j] = inputFeature[j];
                for(int j=0;j<autoEnc->m_nClass;j++)
                    outputMerged[nrFeat+j] = autoencoderOutput[j];
                getEnsemblePrediction ( outputMerged, ensembleOutput );
                delete[] outputMerged;
            }
            else
                getEnsemblePrediction ( autoencoderOutput, ensembleOutput );
        }
        else
            getEnsemblePrediction ( inputFeature, ensembleOutput );

        // if the dataset has classification type, count the #wrong labeled
        if ( Framework::getDatasetType() )
        {
            for ( uint d=0;d<m_data->m_nDomain;d++ )
            {
                if ( getIndexOfMax ( ensembleOutput + d * m_data->m_nClass, m_data->m_nClass ) != m_data->m_testLabelOrig[d+i* ( uint ) m_data->m_nDomain] )
                    wrongLabelCnt[d]++;
                if(m_data->m_nDomain == 1)
                {
                    uint predictedLabel = getIndexOfMax ( ensembleOutput + d * m_data->m_nClass, m_data->m_nClass );
                    uint trueLabel = m_data->m_testLabelOrig[d+i* ( uint ) m_data->m_nDomain];
                    confusionMatrix[predictedLabel + trueLabel*m_data->m_nClass]++;
                }
            }
        }

        // rmse calculation over all targets
        for ( uint j=0;j<m_data->m_nClass*m_data->m_nDomain;j++ )
        {
            REAL target = m_data->m_testTargetOrig[i * ( uint ) m_data->m_nClass * ( uint ) m_data->m_nDomain + j];
            REAL prediction = ensembleOutput[j];
	    if (target - 0.0 < 1.0e-5 || target - 0.1 > 1.0e-5) { 
            	rmse += ( prediction - target ) * ( prediction - target );
	    }
            mean += prediction;
            float predictionSP = prediction;
            fOutput.write ( ( char* ) &predictionSP, sizeof ( float ) );
	    fOutput2 << predictionSP << "\n";
        }

    }

    delete[] m_outputVectorTmp;
    delete[] m_labelsTmp;

    // print classification error
    if ( Framework::getDatasetType() )
    {
        int nWrong = 0;
        for ( int d=0;d<m_data->m_nDomain;d++ )
        {
            nWrong += wrongLabelCnt[d];
            if ( m_data->m_nDomain > 1 )
                cout<<"["<< ( double ) wrongLabelCnt[d]/ ( double ) m_data->m_nTest<<"] ";
        }
        m_predictionClassificationError = 100.0* ( double ) nWrong/ ( ( double ) m_data->m_nTest* ( double ) m_data->m_nDomain );
        cout<<endl<<"Classification test error: "<<m_predictionClassificationError<<"%"<<endl;
    }

    // print RMSE
    m_predictionRMSE = sqrt ( rmse/ ( double ) ( ( uint ) m_data->m_nClass * ( uint ) m_data->m_nDomain * m_data->m_nTest ) );
    cout<<"RMSE test: "<<m_predictionRMSE<<endl;

    // print confusion matrix
    if(m_data->m_nDomain == 1)
    {
        cout<<endl<<"ConfusionMatrix"<<endl<<"predicted-> ";
        for(uint i=0;i<m_data->m_nClass;i++)
            cout<<"|  "<<i<<"   ";
        cout<<"|"<<endl;
        for(uint i=0;i<m_data->m_nClass;i++)
        {
            cout<<"            ";
            for(uint j=0;j<m_data->m_nClass;j++)
            {
                char buf[128];
                sprintf(buf,"%6d",confusionMatrix[j+i*m_data->m_nClass]);
                cout<<"|"<<buf;
            }
            cout<<"| "<<i<<" (true class)"<<endl;
        }
    }
    
    // print info
    cout<<endl<<"Predictions are written to binary output file: "<<fname<<" ("<< ( uint ) ( ( uint ) m_data->m_nTest* ( uint ) m_data->m_nClass*m_data->m_nDomain*sizeof ( float ) );
    cout<<endl<<"Predictions are written to csv output file: "<<fname2<<" ("<< ( uint ) ( ( uint ) m_data->m_nTest* ( uint ) m_data->m_nClass*m_data->m_nDomain*sizeof ( float ) );
    cout<<endl<<" Bytes, REAL="<< ( int ) sizeof ( float ) <<"Bytes, #elements:"<< ( uint ) m_data->m_nTest* ( uint ) m_data->m_nClass*m_data->m_nDomain<<") ";
    cout<<"[mean:"<<mean/ ( double ) ( ( uint ) m_data->m_nTest* ( uint ) m_data->m_nClass* ( uint ) m_data->m_nDomain ) <<"] )"<<endl;
    cout<<"Prediction time: "<<time ( 0 )-t0<<"[s]"<<endl<<endl;

    fOutput.close();
    fOutput2.close();

    if ( ensembleOutput )
        delete[] ensembleOutput;
    ensembleOutput = 0;

    endPredictionMode();
}

/**
 * Generate a bagging ensemble
 * produce a set of predictions with modified train set
 * measuring accuracy on the test set
 *
 */
void Scheduler::bagging()
{
    int epochs = Framework::getAdditionalStartupParameter();
    cout<<endl<<endl;
    cout<<"================================= Bagging ================================="<<endl;
    cout<<"epochs:"<<epochs<<endl<<endl<<endl;
    m_baggingRun = true;

    vector<string> baggingFileNames;
    uint testSize = 0;
    double rmseMean = 0.0, classErrMean = 0.0;

    for ( int e=0;e<epochs;e++ )
    {
        cout<<"e:"<<e<<endl;

        m_randSeedBagBoost = e + 1;

        // train and predict testset
        train();
        predict();

        rmseMean += getPredictionRMSE();
        classErrMean += getClassificationError();

        fstream fTest ( ( m_data->m_datasetPath+"/"+TMP_PATH+"/testPrediction.data" ).c_str(),ios::in );
        if ( fTest.is_open() ==false )
            assert ( false );
        char buf[512];
        sprintf ( buf,"%s.%d", ( m_data->m_datasetPath+"/"+TMP_PATH+"/testPrediction.data" ).c_str(),e );
        baggingFileNames.push_back ( buf );
        fstream fTmp ( buf,ios::out );

        // get length of file
        fTest.seekg ( 0, ios::end );
        uint length = fTest.tellg();
        testSize = length/sizeof ( REAL );
        fTest.seekg ( 0, ios::beg );

        // allocate memory
        char* buffer = new char[length];

        // read data as a block
        fTest.read ( buffer,length );
        fTest.close();

        // write
        fTmp.write ( buffer,length );
        delete[] buffer;

        fTmp.close();
    }


    srand ( m_data->m_randSeed );
    m_data->readDataset ( m_data->m_datasetName );

    testSize = m_data->m_nTest * m_data->m_nClass * m_data->m_nDomain;

    // calc bag mean
    REAL* testMean = new REAL[testSize];
    for ( int i=0;i<testSize;i++ )
        testMean[i] = 0.0;
    for ( int e=0;e<epochs;e++ )
    {
        char nameBuf[512];
        sprintf ( nameBuf,"%s.%d", ( m_data->m_datasetPath+"/"+TMP_PATH+"/testPrediction.data" ).c_str(),e );
        fstream f ( nameBuf,ios::in );
        float* buf = new float[testSize];
        f.read ( ( char* ) buf,sizeof ( float ) *testSize );
        f.close();

        // add this run to ensemble
        for ( int i=0;i<testSize;i++ )
            testMean[i] += buf[i];

        delete[] buf;


        // per epoch: calculate prediction RMSE and classification error
        double classErrBag = 0.0;
        double rmseBag = 0.0;

        // go through the test set
        for ( uint i=0;i<m_data->m_nTest;i++ )
        {
            REAL* ensembleOutput = testMean + i * m_data->m_nClass*m_data->m_nDomain;
            REAL* ensembleOutputNorm = new REAL[m_data->m_nClass*m_data->m_nDomain];
            for ( int j=0;j<m_data->m_nClass*m_data->m_nDomain;j++ )
                ensembleOutputNorm[j] = ensembleOutput[j] / ( ( double ) e+1.0 );

            // if the dataset has classification type, count the #wrong labeled
            if ( Framework::getDatasetType() )
            {
                for ( int d=0;d<m_data->m_nDomain;d++ )
                    if ( getIndexOfMax ( ensembleOutputNorm + d * m_data->m_nClass, m_data->m_nClass ) != m_data->m_testLabelOrig[d+i*m_data->m_nDomain] )
                        classErrBag += 1.0;
            }

            // rmse calculation over all targets
            for ( int j=0;j<m_data->m_nClass*m_data->m_nDomain;j++ )
            {
                REAL target = m_data->m_testTargetOrig[i * m_data->m_nClass * m_data->m_nDomain + j];
                REAL prediction = ensembleOutputNorm[j];
                rmseBag += ( prediction - target ) * ( prediction - target );
            }

            delete[] ensembleOutputNorm;
        }

        if ( Framework::getDatasetType() )
            classErrBag = 100.0*classErrBag/ ( ( double ) m_data->m_nTest* ( double ) m_data->m_nDomain );
        rmseBag = sqrt ( rmseBag/ ( double ) ( m_data->m_nClass*m_data->m_nDomain*m_data->m_nTest ) );
        cout<<e<<": "<<"RMSE:"<<rmseBag<<" classErr:"<<classErrBag<<endl;

    }

    // take the mean
    for ( int i=0;i<testSize;i++ )
        testMean[i] /= ( REAL ) epochs;

    fstream fTest ( ( m_data->m_datasetPath+"/"+TMP_PATH+"/testPrediction.data" ).c_str(),ios::in );
    if ( fTest.is_open() ==false )
        assert ( false );
    fTest.write ( ( char* ) testMean,sizeof ( float ) *testSize );
    fTest.close();


    // calculate prediction RMSE and classification error
    double classErrBag = 0.0;
    double rmseBag = 0.0;

    // go through the test set
    for ( uint i=0;i<m_data->m_nTest;i++ )
    {
        REAL* ensembleOutput = testMean + i * m_data->m_nClass*m_data->m_nDomain;

        // if the dataset has classification type, count the #wrong labeled
        if ( Framework::getDatasetType() )
        {
            for ( int d=0;d<m_data->m_nDomain;d++ )
                if ( getIndexOfMax ( ensembleOutput + d * m_data->m_nClass, m_data->m_nClass ) != m_data->m_testLabelOrig[d+i*m_data->m_nDomain] )
                    classErrBag += 1.0;
        }

        // rmse calculation over all targets
        for ( int j=0;j<m_data->m_nClass*m_data->m_nDomain;j++ )
        {
            REAL target = m_data->m_testTargetOrig[i * m_data->m_nClass * m_data->m_nDomain + j];
            REAL prediction = ensembleOutput[j];
            rmseBag += ( prediction - target ) * ( prediction - target );
        }

    }

    // calc errors
    if ( Framework::getDatasetType() )
        classErrBag = 100.0*classErrBag/ ( ( double ) m_data->m_nTest* ( double ) m_data->m_nDomain );
    rmseBag = sqrt ( rmseBag/ ( double ) ( m_data->m_nClass * m_data->m_nDomain * m_data->m_nTest ) );

    m_predictionRMSE = rmseBag;
    m_predictionClassificationError = classErrBag;

    cout<<endl;
    cout<<epochs<<" runs"<<endl;
    cout<<"Bagging runs (with boostrap sample):   rmseMean:"<<rmseMean/ ( double ) epochs<<"   classErrMean:"<<classErrMean/ ( double ) epochs<<endl;
    cout<<"Bagged (mean)                      :   rmse    :"<<rmseBag<<"   classErr    :"<<classErrBag<<endl<<endl;

    delete[] testMean;
}

/**
 * Generate a boosting ensemble
 * reweight sample probabilites based on train error
 *
 */
void Scheduler::boosting()
{
    int epochs = Framework::getAdditionalStartupParameter();
    cout<<endl<<endl;
    cout<<"================================= Boosting ================================="<<endl;
    cout<<"epochs:"<<epochs<<endl<<endl<<endl;
    m_boostingRun = true;

    vector<string> boostingFileNames;
    uint testSize = 0;
    double rmseMean = 0.0, classErrMean = 0.0;
    REAL* beta = new REAL[epochs];
    for ( m_boostingEpoch=0;m_boostingEpoch<epochs;m_boostingEpoch++ )
    {
        cout<<"e:"<<m_boostingEpoch<<endl;

        m_randSeedBagBoost = m_boostingEpoch;

        // train and predict testset (testset must be fixed)
        train();
        predict();

        fstream f ( "A.txt",ios::out );
        for ( int i=0;i<m_boostingNTrain;i++ )
            f<<m_probs[i]<<endl;
        f.close();

        rmseMean += getPredictionRMSE();
        classErrMean += getClassificationError();

        fstream fTest ( ( m_data->m_datasetPath+"/"+TMP_PATH+"/testPrediction.data" ).c_str(),ios::in );
        if ( fTest.is_open() ==false )
            assert ( false );
        char buf[512];
        sprintf ( buf,"%s.%d", ( m_data->m_datasetPath+"/"+TMP_PATH+"/testPrediction.data" ).c_str(),m_boostingEpoch );
        boostingFileNames.push_back ( buf );
        fstream fTmp ( buf,ios::out );

        // get length of file
        fTest.seekg ( 0, ios::end );
        uint length = fTest.tellg();
        testSize = length/sizeof ( float );
        fTest.seekg ( 0, ios::beg );

        // allocate memory
        char* buffer = new char [length];

        // read data as a block
        fTest.read ( buffer,length );
        fTest.close();

        // write
        fTmp.write ( buffer,length );
        delete[] buffer;

        fTmp.close();

        // ==================== predict train set =====================
        double rmseBoost = 0.0, epsilon = 0.0, rmseTrain = 0.0;
        REAL min = m_data->m_negativeTarget, max = m_data->m_positiveTarget;
        Framework::setFrameworkMode ( 1 );

        preparePredictionMode();
        REAL* ensembleOutput = new REAL[m_data->m_nClass*m_data->m_nDomain];
        REAL* loss = new REAL[m_boostingNTrain];
        // go through the train set
        int nOut = m_data->m_nClass*m_data->m_nDomain;
        for ( int i=0;i<m_boostingNTrain;i++ )
        {
            // predict one example
            REAL* inputFeature = m_boostingTrain + i * m_data->m_nFeatures;
            getEnsemblePrediction ( inputFeature, ensembleOutput );

            // rmse calculation over all targets
            REAL err = 0.0, err2 = 0.0;
            for ( int j=0;j<m_data->m_nDomain;j++ )
            {
                int indMax = -1;
                REAL maxTarget = -1e10;
                for ( int k=0;k<m_data->m_nClass;k++ )
                    if ( maxTarget < m_boostingTargets[i * nOut + m_data->m_nClass*j + k] )
                    {
                        maxTarget = m_boostingTargets[i * nOut + m_data->m_nClass*j + k];
                        indMax = k;
                    }
                if ( indMax == -1 )
                    assert ( false );
                for ( int k=0;k<m_data->m_nClass;k++ )
                {
                    if ( indMax != k )
                    {
                        REAL predictionTarget = ensembleOutput[m_data->m_nClass*j + indMax];
                        REAL prediction = ensembleOutput[m_data->m_nClass*j + k];

                        err += 1.0 - ( predictionTarget-min ) / ( max-min ) + ( prediction-min ) / ( max-min );
                        err2 += 1.0 + ( predictionTarget-min ) / ( max-min ) - ( prediction-min ) / ( max-min );
                    }
                }

                for ( int j=0;j<m_data->m_nDomain;j++ )
                    for ( int k=0;k<m_data->m_nClass;k++ )
                    {
                        REAL out = ensembleOutput[m_data->m_nClass*j + k];
                        REAL target = m_boostingTargets[i * nOut + m_data->m_nClass*j + k];
                        rmseTrain += ( out-target ) * ( out-target );
                    }

            }
            epsilon += m_probs[i] * err / ( REAL ) ( m_data->m_nClass-1 );
            loss[i] = err2 / ( REAL ) ( m_data->m_nClass-1 );
        }
        rmseTrain = sqrt ( rmseTrain/ ( double ) ( m_boostingNTrain*m_data->m_nClass*m_data->m_nDomain ) );
        cout<<"rmseTrain(boosting):"<<rmseTrain<<endl;
        epsilon *= 0.5;
        beta[m_boostingEpoch] = epsilon / ( 1.0 - epsilon );
        // update example probabilities
        for ( int i=0;i<m_boostingNTrain;i++ )
            m_probs[i] *= pow ( beta[m_boostingEpoch], 0.5 * loss[i] );
        double sum = 0.0;
        for ( int i=0;i<m_boostingNTrain;i++ )
            sum += m_probs[i];
        // normalize
        for ( int i=0;i<m_boostingNTrain;i++ )
            m_probs[i] /= sum;

        delete[] loss;
        delete[] ensembleOutput;

        endPredictionMode();
    }

    // read test data
    srand ( m_data->m_randSeed );
    m_data->readDataset ( m_data->m_datasetName );

    // calc boosting mean
    cout<<endl<<endl<<"#test values:"<<testSize<<" (dataset size:"<<m_data->m_nTest<<")"<<endl;
    REAL* testMean = new REAL[testSize];
    for ( int i=0;i<testSize;i++ )
        testMean[i] = 0.0;
    for ( int e=0;e<epochs;e++ )
    {
        cout<<"Cascade layer "<<e<<": weight:"<<log10 ( 1.0/beta[e] ) <<"  "<<flush;
        fstream f ( boostingFileNames[e].c_str(),ios::in );
        if ( f.is_open() == false )
            assert ( false );
        float* buf = new float[testSize];
        f.read ( ( char* ) buf,sizeof ( float ) *testSize );
        f.close();

        // add this run to ensemble
        for ( int i=0;i<testSize;i++ )
        {
            REAL w = log10 ( 1.0/beta[e] );
            testMean[i] += w*buf[i];
        }
        delete[] buf;


        // Calculate per-epoch errors
        // go through the test set
        double classErrBoostingPerEpoch = 0.0;
        double rmseBoostingPerEpoch = 0.0;
        double rmseBoostingPerEpoch0 = 0.0;
        double rmseBoostingPerEpoch1 = 0.0;
        for ( int i=0;i<m_data->m_nTest;i++ )
        {
            REAL* ensembleOutput = testMean + i * m_data->m_nClass*m_data->m_nDomain;
            REAL* ensembleOutputNorm0 = new REAL[m_data->m_nClass*m_data->m_nDomain];
            REAL* ensembleOutputNorm1 = new REAL[m_data->m_nClass*m_data->m_nDomain];

            REAL norm0 = 0.0;
            for ( int j=0;j<=e;j++ )
                norm0 += log10 ( 1.0/beta[e] );
            for ( int j=0;j<m_data->m_nClass*m_data->m_nDomain;j++ )
            {
                ensembleOutputNorm0[j] = ensembleOutput[j]/ ( REAL ) ( e+1 );
                ensembleOutputNorm1[j] = ensembleOutput[j]/norm0;
            }

            // if the dataset has classification type, count the #wrong labeled
            if ( Framework::getDatasetType() )
            {
                for ( int d=0;d<m_data->m_nDomain;d++ )
                    if ( getIndexOfMax ( ensembleOutput + d * m_data->m_nClass, m_data->m_nClass ) != m_data->m_testLabelOrig[d+i*m_data->m_nDomain] )
                        classErrBoostingPerEpoch += 1.0;
            }

            // rmse calculation over all targets
            for ( int j=0;j<m_data->m_nClass*m_data->m_nDomain;j++ )
            {
                REAL target = m_data->m_testTargetOrig[i * m_data->m_nClass * m_data->m_nDomain + j];
                REAL prediction = ensembleOutput[j];
                rmseBoostingPerEpoch += ( prediction - target ) * ( prediction - target );

                prediction = ensembleOutputNorm0[j];
                rmseBoostingPerEpoch0 += ( prediction - target ) * ( prediction - target );

                prediction = ensembleOutputNorm1[j];
                rmseBoostingPerEpoch1 += ( prediction - target ) * ( prediction - target );
            }

            delete[] ensembleOutputNorm0;
            delete[] ensembleOutputNorm1;
        }
        // calc errors
        if ( Framework::getDatasetType() )
            classErrBoostingPerEpoch = 100.0*classErrBoostingPerEpoch/ ( ( double ) m_data->m_nTest* ( double ) m_data->m_nDomain );
        rmseBoostingPerEpoch = sqrt ( rmseBoostingPerEpoch/ ( double ) ( m_data->m_nClass * m_data->m_nDomain * m_data->m_nTest ) );
        rmseBoostingPerEpoch0 = sqrt ( rmseBoostingPerEpoch0/ ( double ) ( m_data->m_nClass * m_data->m_nDomain * m_data->m_nTest ) );
        rmseBoostingPerEpoch1 = sqrt ( rmseBoostingPerEpoch1/ ( double ) ( m_data->m_nClass * m_data->m_nDomain * m_data->m_nTest ) );
        cout<<"Boosting:  rmse:"<<rmseBoostingPerEpoch<<"  rmse0:"<<rmseBoostingPerEpoch0<<"  rmse1:"<<rmseBoostingPerEpoch1<<"  classErr:"<<classErrBoostingPerEpoch<<"%"<<endl;
    }

    // take the mean
    for ( int i=0;i<testSize;i++ )
        testMean[i] /= ( REAL ) epochs;

    fstream fTest ( ( m_data->m_datasetPath+"/"+TMP_PATH+"/testPrediction.data" ).c_str(),ios::in );
    if ( fTest.is_open() ==false )
        assert ( false );
    fTest.write ( ( char* ) testMean,sizeof ( float ) *testSize );
    fTest.close();


    // calculate prediction RMSE and classification error
    double classErrBoosting = 0.0;
    double rmseBoosting = 0.0;

    // go through the test set
    for ( int i=0;i<m_data->m_nTest;i++ )
    {
        REAL* ensembleOutput = testMean + i * m_data->m_nClass*m_data->m_nDomain;

        // if the dataset has classification type, count the #wrong labeled
        if ( Framework::getDatasetType() )
        {
            for ( int d=0;d<m_data->m_nDomain;d++ )
                if ( getIndexOfMax ( ensembleOutput + d * m_data->m_nClass, m_data->m_nClass ) != m_data->m_testLabelOrig[d+i*m_data->m_nDomain] )
                    classErrBoosting += 1.0;
        }

        // rmse calculation over all targets
        for ( int j=0;j<m_data->m_nClass*m_data->m_nDomain;j++ )
        {
            REAL target = m_data->m_testTargetOrig[i * m_data->m_nClass * m_data->m_nDomain + j];
            REAL prediction = ensembleOutput[j];
            rmseBoosting += ( prediction - target ) * ( prediction - target );
        }

    }

    // calc errors
    if ( Framework::getDatasetType() )
        classErrBoosting = 100.0*classErrBoosting/ ( ( double ) m_data->m_nTest* ( double ) m_data->m_nDomain );
    rmseBoosting = sqrt ( rmseBoosting/ ( double ) ( m_data->m_nClass * m_data->m_nDomain * m_data->m_nTest ) );

    m_predictionRMSE = rmseBoosting;
    m_predictionClassificationError = classErrBoosting;

    cout<<endl;
    cout<<epochs<<" runs"<<endl;
    cout<<"Boosting runs (mean boostrap sample):   rmseMean:"<<rmseMean/ ( double ) epochs<<"   classErrMean:"<<classErrMean/ ( double ) epochs<<"%"<<endl;
    cout<<"Boosting (mean)                     :   rmse    :"<<rmseBoosting<<"   classErr    :"<<classErrBoosting<<"%"<<endl<<endl;

    delete[] testMean;
}

/**
 * Check if the Algorithm dsc file has no errors
 *
 * @param fname Dsc filename of Algorithm
 * @param algoName Reference the the algorithm name (KNN, NN, LinearModel, ..)
 * @param id Reference to the ID (ascending number from 0,1..)
 */
void Scheduler::checkAlgorithmTemplate ( string fname, string &algoName, string &id )
{
    // check, if the algorithm line exists
    fstream f ( fname.c_str(), ios::in );
    if ( f.is_open() == false )
        assert ( false );
    string firstLine, secondLine, thirdLine;
    f>>firstLine;
    f>>secondLine;
    f>>thirdLine;
    f.close();
    int pos = firstLine.find ( "=" );
    string name = firstLine.substr ( 0, pos );
    algoName = firstLine.substr ( pos+1 );
    if ( name != "ALGORITHM" )
    {
        cout<<"Wrong dsc file, no ALGORITHM=.. found in first line"<<endl;
        exit ( 0 );
    }
    pos = secondLine.find ( "=" );
    name = secondLine.substr ( 0, pos );
    id = secondLine.substr ( pos+1 );
    if ( name != "ID" )
    {
        cout<<"Wrong dsc file, no ID=.. found in second line"<<endl;
        exit ( 0 );
    }
}

/**
 * Start the training of the particular Algorithm
 *
 * @param fname Dsc file name of algorithm
 */
void Scheduler::trainAlgorithm ( string fnameTemplate, string fnameDsc ) //string fname)
{
    cout<<"Train algorithm:"<<fnameTemplate<<endl;

    string algoName, id;
    checkAlgorithmTemplate ( fnameTemplate, algoName, id );

    // read dsc file
    m_data->readDscFile ( fnameTemplate );
    if ( m_data->m_disableTraining )
    {
        cout<<"Training disabled."<<endl;
        return;
    }

    // copy the content of the template to the dsc file
    fstream fAlgoTemplate ( fnameTemplate.c_str(), ios::in );
    fstream fAlgo ( fnameDsc.c_str(), ios::out );
    cout<<"AlgoTemplate:"<<fnameTemplate<<"  Algo:"<<fnameDsc<<endl;
    char buf[1024];
    while ( fAlgoTemplate.getline ( buf, 1024 ) ) // read all lines
    {
        string line = string ( buf );
        fAlgo<<line<<endl;
    }
    fAlgoTemplate.close();
    fAlgo.close();

    // redirect cout to filename
    cout.setOutputFile ( fnameDsc );

    cout<<"Floating point precision: "<< ( int ) sizeof ( REAL ) <<" Bytes"<<endl;
    
    // read effect file and prepare validation
    m_data->partitionDatasetToCrossValidationSets();

    // start the algorithm
    Algorithm* algo = 0;
    algorithmDispatcher ( algo, algoName );
    algo->setDataPointers ( m_data );

    if ( m_data->m_enableFeatureSelection )
    {
        algo->doFeatureSelection();
        exit ( 0 );
    }
    else
        algo->train();

    if ( algo )
    {
        cout<<"delete algo"<<endl;
        delete algo;
    }
    algo = 0;
    cout<<"Finished train algorithm:"<<fnameTemplate<<endl;

}

/**
 * Returns the ID from the corresponding dsc-file of the given full-prediction file
 *
 * @param fullPredictor Full-predictor file name
 * @return The id from the dsc-file (which belongs to the full-predictor)
 */
int Scheduler::getIDFromFullPredictor ( string fullPredictor )
{
    if ( fullPredictor=="" )
        return 0;
    for ( int i=0;i<m_algorithmObjectList.size();i++ )
        if ( m_algorithmObjectList[i]->m_stringMap["fullPrediction"] == fullPredictor )
            return m_algorithmObjectList[i]->m_algorithmID;
    cout<<"Error, this fullPredictor was not found:"<<fullPredictor<<endl;
    assert ( false );
}

/**
 * Predict a target vector with given input feature
 * Based on the trained ensemble
 *
 * @param input REAL* vector to original input feature (read)
 * @param output REAL* vector to target (write)
 */
void Scheduler::getEnsemblePrediction ( REAL* input, REAL* output )
{
    int N = m_algorithmList.size();
    REAL* tmp = new REAL[m_data->m_nFeatures+N];
    
    int gbEpochMax = 1;
    if(Framework::getGradientBoostingMaxEpochs() > 1)
        gbEpochMax = getGradientBoostingMaxEpochs();
    // gradient boosting: all epochs
    for(int gbEpoch=0;gbEpoch<gbEpochMax;gbEpoch++)
    {
        if(Framework::getGradientBoostingMaxEpochs() > 1)
        {
            Framework::setGradientBoostingEpoch(gbEpoch);
            if(gbEpoch==0)
                for(int i=0;i<m_data->m_nClass*m_data->m_nDomain;i++)
                    m_gradientBoostingOutputSum[i] = m_gradientBoostingTargetMean[i];
        }
        
        // predict all targets per algorithm
        // if the algorithm needs a preprocessor, the effect file is loaded
        for ( int i=0;i<N;i++ )
        {
            int normalizationOffset = i*m_data->m_nFeatures;
            
            // effect = pre-processor for this algorithm
            int ID = m_effectID[i];
            REAL* effect = m_noEffect;  // constant zero
            REAL* outputVector = m_outputs[i+1]; // +1: jump over constant 1
            if ( ID != 0 )
            {
                if ( ID < 0 || ID > i )
                    assert ( false );
                effect = m_outputs[ID];  // output of another prediction as effect
            }
    
            // cascade learning: add predictions of previous model as input to current
            if ( m_data->m_enableCascadeLearning )
            {
                int nF = m_data->m_nFeatures;
                int nFAlgo = m_algorithmObjectList[i+N*gbEpoch]->m_nFeatures;
    
                // add input feature + normalize
                for ( int j=0;j<nF;j++ )
                    tmp[j] = ( input[j] - m_data->m_mean[j+normalizationOffset] ) / m_data->m_std[j+normalizationOffset];
    
                // add predictions + normalize
                for ( int j=0;j<i;j++ ) // over all previous models
                {
                    REAL* previousOutputVector = m_outputs[j+1];
                    int nOut = m_data->m_nClass*m_data->m_nDomain;
                    for ( int k=0;k<nOut;k++ )
                        tmp[nF+j*nOut+k] = ( previousOutputVector[k] - m_data->m_mean[nF+j*nOut+k+normalizationOffset] ) / m_data->m_std[nF+j*nOut+k+normalizationOffset];
                }
            }
            else  // standard prediction
            {
                for ( int j=0;j<m_data->m_nFeatures;j++ )
                    tmp[j] = ( input[j] - m_data->m_mean[j+normalizationOffset] ) / m_data->m_std[j+normalizationOffset];  // feature normalization
            }
            
            if ( m_data->m_validationType=="Retraining" || m_data->m_validationType=="ValidationSet")
                m_algorithmObjectList[i+N*gbEpoch]->predictMultipleOutputs ( tmp, effect, outputVector, m_labelsPredict, 1, m_data->m_nCross );
            else if ( m_data->m_validationType=="CrossFoldMean" || m_data->m_validationType=="Bagging" )
            {
                for ( int j=0;j<m_data->m_nClass*m_data->m_nDomain;j++ )
                    outputVector[j] = 0.0;
                for ( int j=0;j<m_data->m_nCross;j++ )
                {
                    m_algorithmObjectListList[i+N*gbEpoch][j]->predictMultipleOutputs ( tmp, effect, m_outputVectorTmp, m_labelsTmp, 1, j );
                    for ( int k=0;k<m_data->m_nClass*m_data->m_nDomain;k++ )
                        outputVector[k] += m_outputVectorTmp[k];
                }
                for ( int j=0;j<m_data->m_nClass*m_data->m_nDomain;j++ )
                    outputVector[j] /= ( REAL ) m_data->m_nCross;
                
                // calc output labels (for classification dataset)
                if ( Framework::getDatasetType() )
                {
                    // in all domains
                    for ( int d=0;d<m_data->m_nDomain;d++ )
                    {
                        // find max. output value
                        int indMax = -1;
                        REAL max = -1e10;
                        for ( int j=0;j<m_data->m_nClass;j++ )
                        {
                            if ( max < outputVector[d*m_data->m_nClass+j] )
                            {
                                max = outputVector[d*m_data->m_nClass+j];
                                indMax = j;
                            }
                        }
                        m_labelsPredict[d] = indMax;
                    }
                }
    
            }
            else
                assert(false);
            
            // gradient boosting
            if(Framework::getGradientBoostingMaxEpochs() > 1)
                for(int j=0;j<m_data->m_nClass*m_data->m_nDomain;j++)
                    m_gradientBoostingOutputSum[j] += outputVector[j];
        }
    }
    delete[] tmp;
    
    if(Framework::getGradientBoostingMaxEpochs() > 1)
        for(int i=0;i<m_data->m_nClass*m_data->m_nDomain;i++)
            m_outputs[1][i] = m_gradientBoostingOutputSum[i];
    
    // calculate the ensemble output with the blender
    if ( m_data->m_enablePostNNBlending )
        m_blenderNN->predictEnsembleOutput ( m_outputs, output );
    else
        m_blender->predictEnsembleOutput ( m_outputs, output );
}

/**
 * Prepare the trained ensemble to predict unknown input features
 *
 */
void Scheduler::preparePredictionMode()
{
    cout<<"Start scheduled prediction"<<endl;

    // fix random seed
    srand ( m_data->m_randSeed );

    // load test set
    m_data->readDataset ( m_data->m_datasetName );
    srand ( m_data->m_randSeed );

    if(m_data->m_validationType=="ValidationSet")
        m_data->m_nCross = 0;
    
    // number of algorithms in the ensemble
    int N = m_algorithmList.size();
    m_data->m_algorithmNameList = m_algorithmList;
    
    // load normalization (mean, std)
    if ( m_data->m_enableCascadeLearning )
        m_data->loadNormalization ( N-1 );
    else
        m_data->loadNormalization();

    // go to prediction mode in all template files
    for ( int i=0;i<N;i++ )
    {
        string fAlgoTemplateName = m_data->m_datasetPath + "/" + m_algorithmList[i];
        setPredictionModeInAlgorithm ( fAlgoTemplateName );
    }
    
    // gradient boosting: Load target mean values
    if(Framework::getGradientBoostingMaxEpochs() > 1)
    {
        m_gradientBoostingTargetMean = new REAL[m_data->m_nClass*m_data->m_nDomain];
        m_gradientBoostingOutputSum = new REAL[m_data->m_nClass*m_data->m_nDomain];
        string fname = m_data->m_datasetPath + "/" + m_data->m_tempPath + "/trainPrediction.data.gb.targetMean";
        fstream f(fname.c_str(), ios::in);
        assert(f.is_open());
        f.read((char*)m_gradientBoostingTargetMean, sizeof(REAL)*m_data->m_nClass*m_data->m_nDomain);
        f.close();
    }
    
    // new NN blender (seldom used)
    if ( m_data->m_enablePostNNBlending )
    {
        m_blenderNN = new BlendingNN();
        m_blenderNN->setDataPointers ( m_data );
        m_blenderNN->readDscFile ( m_data->m_datasetPath + "/NeuralNetwork.dscblend" );
        m_blenderNN->readSpecificMaps();
        m_blenderNN->loadWeights();
    }

    // load blending weights
    m_blender = new BlendStopping ( ( Algorithm* ) m_data );
    m_blender->loadBlendingWeights ( m_data->m_datasetPath + "/" + m_data->m_tempPath, N+1 );

    for ( int i=0;i<N;i++ )
        cout<<"ALGO FROM MASTER DSC-FILE:"<<m_algorithmObjectList[i]->m_stringMap["fullPrediction"]<<endl;

    m_blender->printWeights();

    int nClass = m_data->m_nClass;
    int nDomain = m_data->m_nDomain;

    // precompute IDs from effect files per prediction
    m_effectID = new int[N];
    for ( int i=0;i<N;i++ )
        m_effectID[i] = getIDFromFullPredictor ( m_algorithmObjectList[i]->m_trainOnFullPredictorFile );

    // used in prediction mode
    // per sample: store prediction for every model
    m_noEffect = new REAL[nClass*nDomain];
    for ( int i=0;i<nClass*nDomain;i++ )
        m_noEffect[i] = 0.0;
    m_outputs = new REAL*[N+1];
    m_effects = new REAL*[N+1];
    for ( int i=0;i<N+1;i++ )
    {
        m_outputs[i] = new REAL[nClass*nDomain];
        m_effects[i] = new REAL[nClass*nDomain];
        for ( int j=0;j<nClass*nDomain;j++ )
        {
            m_outputs[i][j] = 1.0;  // init with constant 1.0 (needed in blend)
            m_effects[i][j] = 0.0;
        }
    }

    // tmp variable for predict labels
    m_labelsPredict = new int[m_data->m_nDomain];
}

/**
 * End of the prediction mode
 * Deallocation of memory
 */
void Scheduler::endPredictionMode()
{
    cout<<"End scheduled prediction"<<endl;
    m_data->deleteMemory();

    for ( int i=0;i<m_algorithmObjectList.size();i++ )
        delete m_algorithmObjectList[i];
    m_algorithmObjectList.clear();

    if ( m_data->m_enablePostNNBlending )
        delete m_blenderNN;
    delete m_blender;
    delete[] m_effectID;
    int N = m_algorithmList.size();
    for ( int i=0;i<N+1;i++ )
    {
        delete[] m_outputs[i];
        delete[] m_effects[i];
    }
    delete[] m_noEffect;
    delete[] m_outputs;
    delete[] m_effects;
    delete[] m_labelsPredict;

}

/**
 * Find the largest element in a vector and return the index
 *
 * @param vector Input REAL vector
 * @param length The number of elements of vector
 * @return The index of the largest element
 */
int Scheduler::getIndexOfMax ( REAL* vector, int length )
{
    int indMax = -1;
    REAL max = -1e10;
    for ( int i=0;i<length;i++ )
    {
        if ( max < vector[i] )
        {
            max = vector[i];
            indMax = i;
        }
    }

    return indMax;
}

/**
 * Used in Prediction mode
 *
 * Set the particular algorithm in the prediction mode
 *
 * @param fname The name of the dsc-file of the Algorithm
 */
void Scheduler::setPredictionModeInAlgorithm ( string fname )
{
    cout<<"Prediction mode in algorithm:"<<fname<<endl;

    // check the dsc file
    string algoName, id;
    checkAlgorithmTemplate ( fname, algoName, id );

    // read dsc file
    m_data->readDscFile ( fname );

    // make an instance of the algorithm and give him the data
    int gbEpochMax = Framework::getGradientBoostingMaxEpochs();
    if(gbEpochMax < 1)
        gbEpochMax = 1;
    for(int gbEpoch=0;gbEpoch<gbEpochMax;gbEpoch++)  // init each model in the gradient boosting chain
    {
        if(Framework::getGradientBoostingMaxEpochs() > 1)
            Framework::setGradientBoostingEpoch(gbEpoch);
        
        if ( m_data->m_validationType=="Retraining" || m_data->m_validationType=="ValidationSet")
        {
            Algorithm* algo = 0;
            algorithmDispatcher ( algo, algoName );
            algo->setDataPointers ( m_data );
            algo->setPredictionMode ( m_data->m_nCross );
            
            // add the algorithm to internal object list of algorithms
            m_algorithmObjectList.push_back ( algo );
        }
        else if ( m_data->m_validationType=="CrossFoldMean" || m_data->m_validationType=="Bagging" )
        {
            cout<<"Make "<<m_data->m_nCross<<" models ready to predict"<<endl;
            Algorithm** algoList = new Algorithm*[m_data->m_nCross];
            for ( int i=0;i<m_data->m_nCross;i++ )
            {
                Algorithm* algo = 0;
                algorithmDispatcher ( algo, algoName );
                algo->setDataPointers ( m_data );
                algo->setPredictionMode ( i );
                algoList[i] = algo;
            }
            m_algorithmObjectListList.push_back ( algoList );
            m_algorithmObjectList.push_back ( algoList[0] );
        }
        else
            assert(false);
    }

    // check, if id already exist
    for ( int i=0;i<m_algorithmIDList.size();i++ )
        if ( m_algorithmIDList[i] == atoi ( id.c_str() ) )
        {
            cout<<"ID:"<<id<<" in "<<algoName<<" already exists"<<endl;
            assert ( false );
        }

    m_algorithmIDList.push_back ( atoi ( id.c_str() ) );

    m_algorithmNameList.push_back ( algoName );

    cout<<endl;
}

/**
 * Make a new instance of an Algorithm based on the model name
 *
 * @param algo Reference to the Algorithm object pointer
 * @param name Name of the model
 */
void Scheduler::algorithmDispatcher ( Algorithm* &algo, string name )
{
    if ( name == "LinearModel" )
        algo = new LinearModel();
    else if ( name == "KNearestNeighbor" )
        algo = new KNearestNeighbor();
    else if ( name == "NeuralNetwork" )
        algo = new NeuralNetwork();
    else if ( name == "PolynomialRegression" )
        algo = new PolynomialRegression();
    else if ( name == "LinearModelNonNeg" )
        algo = new LinearModelNonNeg();
    else if ( name == "KernelRidgeRegression" )
        algo = new KernelRidgeRegression();
    else if ( name == "NeuralNetworkRBMauto" )
        algo = new NeuralNetworkRBMauto();
    else if ( name == "Autoencoder" )
        algo = new Autoencoder();
    else if ( name == "GBDT" )
        algo = new GBDT();
    else if ( name == "LogisticRegression" )
        algo = new LogisticRegression();
    else
        assert ( false );
}

/**
 * Generates a template of the master description file
 * This is an example of a Master.dsc file
 *
 * @return The template string
 */
string Scheduler::masterDscTemplateGenerator ( string dataset, bool isClass, vector<string> algos, int rSeed, string blendAlgo, bool cascade )
{
    stringstream s;
    s<<"dataset="<<dataset<<endl;
    s<<"isClassificationDataset="<<isClass<<endl;
    s<<"maxThreads=2"<<endl;
    s<<"maxThreadsInCross=2"<<endl;
    s<<"nCrossValidation=6"<<endl;
    s<<"validationType=Retraining"<<endl;
    s<<"positiveTarget=1.0"<<endl;
    s<<"negativeTarget=-1.0"<<endl;
    s<<"randomSeed="<<rSeed<<endl;
    s<<"nMixDataset=20"<<endl;
    s<<"nMixTrainList=100"<<endl;
    s<<"standardDeviationMin=0.01"<<endl;
    s<<"blendingRegularization=1e-4"<<endl;
    s<<"blendingEnableCrossValidation=0"<<endl;
    s<<"blendingAlgorithm="<<blendAlgo<<endl;
    s<<"enablePostNNBlending=0"<<endl;
    s<<"enableCascadeLearning="<<cascade<<endl;
    s<<"enableGlobalMeanStdEstimate=0"<<endl;
    s<<"enableSaveMemory=1"<<endl;
    s<<"addOutputNoise=0"<<endl;
    s<<"enablePostBlendClipping=0"<<endl;
    s<<"enableFeatureSelection=0"<<endl;
    s<<"featureSelectionWriteBinaryDataset=0"<<endl;
    s<<"enableGlobalBlendingWeights=1"<<endl;
    s<<"errorFunction=RMSE"<<endl;
    s<<"disableWriteDscFile=0"<<endl;
    s<<"enableStaticNormalization=0"<<endl;
    s<<"staticMeanNormalization=0.0"<<endl;
    s<<"staticStdNormalization=1.0"<<endl;
    s<<"enableProbablisticNormalization=0"<<endl;
    s<<"dimensionalityReduction=no"<<endl;
    s<<"addAutoencoderFeatures=0"<<endl;
    s<<"subsampleTrainSet=1.0"<<endl;
    s<<"subsampleFeatures=1.0"<<endl;
    s<<"globalTrainingLoops=1"<<endl;
    s<<"addConstantInput=0"<<endl;
    s<<"gradientBoostingMaxEpochs=1"<<endl;
    s<<"gradientBoostingLearnrate=0.1"<<endl;
    s<<"rejectConstantDataColumns=1"<<endl;
    s<<"noNormalizationForBinaryColumns=1"<<endl;
    s<<endl;
    s<<"[ALGORITHMS]"<<endl;
    for ( int i=0;i<algos.size();i++ )
        s<<algos[i]<<endl;

    return s.str();
}

/**
 * Return rmse of last testset prediction
 *
 */
REAL Scheduler::getPredictionRMSE()
{
    return m_predictionRMSE;
}

/**
 * Return classification error of last testset prediction
 *
 */
REAL Scheduler::getClassificationError()
{
    return m_predictionClassificationError;
}
