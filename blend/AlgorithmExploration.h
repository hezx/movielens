#ifndef __ALGORITHM_EXPLORATION_
#define __ALGORITHM_EXPLORATION_

#include "Framework.h"
#include "Scheduler.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

/**
 * Test different train configurations in order to
 * explore the best ensemble setup
 *
 * Objective:
 * - best performance on a dataset
 * - which chain of models is the best?
 * - compare: stacking, cascade, residual, cascade+residual
 *
 */

class AlgorithmExploration : public Framework, public Scheduler
{
public:
    AlgorithmExploration();
    ~AlgorithmExploration();

    void start();
    void randPerm ( vector<string> algorithmStack, vector<string> availableAlgorithms, int maxDepth );
private:

    vector<vector<string> > m_trainList;
};


#endif
