#include "StreamOutput.h"

#include <string>
#include <ctime>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <fstream>

using namespace std;

/**
 * Constructor
 */
StreamOutput::StreamOutput()
{
    m_name = "";
    m_enableOutput = 1;
    m_enableFileOutput = 1;
}

/**
 * Destructor
 */
StreamOutput::~StreamOutput()
{

}

/**
 * Set the textfile, which reports all cout<<..
 * This is in most cases a description (dsc) file
 *
 * @param s The text file name (log for cout)
 */
void StreamOutput::setOutputFile ( string s )
{
    if ( m_enableOutput )
    {
        if ( s == "" )
            cout<<"Clear output file for cout"<<endl;
        else
            cout<<"Output File for cout redirect is set now to "<<s<<endl;
    }
    m_name = s;
}

StreamOutput& StreamOutput::operator << ( const char* s )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

StreamOutput& StreamOutput::operator << ( char* s )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

StreamOutput& StreamOutput::operator << ( char s )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

StreamOutput& StreamOutput::operator << ( int s )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

StreamOutput& StreamOutput::operator << ( unsigned long s )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

StreamOutput& StreamOutput::operator << ( double s )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

StreamOutput& StreamOutput::operator << ( double* s )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

StreamOutput& StreamOutput::operator << ( float s )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

StreamOutput& StreamOutput::operator << ( float* s )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

StreamOutput& StreamOutput::operator << ( time_t s )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

StreamOutput& StreamOutput::operator << ( unsigned int s )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

StreamOutput& StreamOutput::operator << ( string s )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

/**
 * This is used to pass the std::endl manipulator (as function pointer)
 */
StreamOutput& StreamOutput::operator << ( ostream& ( *s ) ( ostream& ) )
{
    if ( m_enableOutput )
    {
        cout<<s;
        if ( m_name!="" && m_enableFileOutput )
        {
            fstream f ( m_name.c_str(), ios::out|ios::app|ios::ate );
            f<<s;
            f.close();
        }
    }
    return ( *this );
}

/**
 * Global disable output
 */
void StreamOutput::disableAllOutputs()
{
    m_enableOutput = 0;
    m_enableFileOutput = 0;
}

/**
 * Global diaseble write to file output
 */
void StreamOutput::disableFileOutputs()
{
    cout<<"Disable file outputs"<<endl;
    m_enableFileOutput = 0;
}

/**
 * Global enable output
 */
void StreamOutput::enableAllOutputs()
{
    m_enableOutput = 1;
    m_enableFileOutput = 1;
}
