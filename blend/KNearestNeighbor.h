#ifndef __K_NEAREST_NEIGHBOR_
#define __K_NEAREST_NEIGHBOR_

#include "StandardAlgorithm.h"
#include "Framework.h"

using namespace std;

/**
 * k-nearest neighbor model for real-valued prediction
 * based on a distance measure in feature space
 *
 * The class is an online k-NN prediction Algorithm
 * This means that no precalcuated distances are stored
 *
 * Pearson correlations is a matrix-matrix product (heavy load on processor with MKL)
 * Euclidean distance is a matrix-matrix product (heavy load on processor with MKL)
 *
 * Tunable parameters are the neighborhood size k and some other optional params
 */

class KNearestNeighbor : public StandardAlgorithm, public Framework
{
public:
    KNearestNeighbor();
    ~KNearestNeighbor();

    virtual void modelInit();
    virtual void modelUpdate ( REAL* input, REAL* target, uint nSamples, uint crossRun );
    virtual void predictAllOutputs ( REAL* rawInputs, REAL* outputs, uint nSamples, uint crossRun );
    virtual void readSpecificMaps();
    virtual void saveWeights ( int cross );
    virtual void loadWeights ( int cross );
    virtual void loadMetaWeights ( int cross );

    static string templateGenerator ( int id, string preEffect, int nameID, bool blendStop );

private:

    // tmp vars
    REAL** m_targetActualPtr;
    int* m_lengthActual;
    bool m_isLoadWeights;

    REAL** m_xNormalized;
    REAL* m_allCorrelations;
    REAL* m_allCorrelationsTransposed;
    REAL* m_predictionAllOutputs;

    // for all prediction
    REAL* m_predictionAllOutputsAllTargets;

    // params
    double m_scale;
    double m_scale2;
    double m_offset;
    double m_globalOffset;
    double m_power;
    int m_k;
    double m_euclideanPower;
    double m_euclideanLimit;

    // dsc file
    int m_evaluationBlockSize;
    bool m_enableAdvancedKNN;
    bool m_dataDenormalization;
    bool m_takeEuclideanDistance;
    bool m_optimizeEuclideanPower;
};


#endif
