#ifndef __YAHOOFINANCE_
#define __YAHOOFINANCE_

#include <vector>

#include <curl/curl.h>
//#include <curl/types.h>
#include <curl/easy.h>
#include <cstdio>

#include "StreamOutput.h"
#include "Data.h"

using namespace std;

#define MIN_YEAR 2009
#define MIN_MONTH 1

#define LOOKBACK_N_DAYS 5

// data gets splitted in time: [train][validation][test]
#define PERCENT_VALIDATION 0.05
#define PERCENT_TEST 0.05

/**
 * Reads the YahooFinance stock data
 *
 */

// data from one day and one stock
typedef struct stockDay_
{
    float m_open;
    float m_close;
    float m_high;
    float m_low;
    uint m_volume;
    uint m_weekday;
} stockDay;

// data from one stock
typedef struct stockCycle_
{
    vector<stockDay> m_data; // per day the stock value
    vector<uint> m_dataDay;  // absolute days, m_dataDay[0] -> m_minDate ,  m_dataDay[end] -> m_maxDate
    uint m_maxDate;
    uint m_minDate;
    float m_beginClose;
    string m_name;
} stockCycle;

class YahooFinance
{
    public:
        YahooFinance();
        ~YahooFinance();
        
        void readData(string path, REAL* &train, REAL* &trainTarget, int* &trainLabel, REAL* &test, REAL* &testTarget, int* &testLabel, uint& nTrain, uint& nTest, int& nClass, int& nDomain, int& nFeat, REAL positiveTarget, REAL negativeTarget, int &nValid, REAL* &valid, REAL* &validTarget, int* &validLabel);
        
    private:
        int requestDate(stockCycle &stock, uint date, uint nDaysBackSearch = LOOKBACK_N_DAYS);
        stockCycle calculateNDayMean(stockCycle &data, uint nDays);
        void calculateSVDFeatures();
        void calculateSVD(REAL* A, int nRows, int nCols, int rank, REAL* &leftMatrix, REAL* &rightMatrix);
        REAL limitToAbs(REAL v, REAL limit);
        
        void readCSVs(vector<string> &files);
        void makeFeatures();
        void checkFeature(vector<float> &feature);
        
        // DATA
        // raw stock values
        vector<stockCycle> m_stocks;  // m_stocks[x] -> stockCycle
        uint m_globalMaxDate;
        uint m_globalMinDate;
        
        uint m_nFeatures;
        uint m_nDayWindow;
        REAL*** m_timeSVDFeatures;  // [day][nDayWindow] -> feature
        REAL*** m_timeStockSVDFeatures;  // [day][stock] -> feature
        
        // generated features, targets and corresponding days for supervised training
        vector<vector<float> > m_features;
        vector<vector<float> > m_targets;
        vector<uint> m_dates;
        
};

#endif
