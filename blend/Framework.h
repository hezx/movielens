#ifndef __FRAMEWORK_
#define __FRAMEWORK_

#include <omp.h>

#include <mkl.h>
#include <ipp.h>
#include <ipps.h>
#include "config.h"
#include "StreamOutput.h"

using namespace std;

/**
 * Basic class for the framework, all classes are derived from this class
 * Currently this class has the information of the dataset type as a static member
 * and other infos like random seed
 *
 * The global status vars can be accessed from everywhere by static methods
 */

class Framework
{
public:
    Framework();
    ~Framework();

    // set and get methods for the dataset type
    static void setDatasetType ( bool isClassification );
    static bool getDatasetType();

    // set and get methods for maxThreads
    static void setMaxThreads ( int n );
    static int getMaxThreads();

    // set and get methods for randomSeed
    static void setRandomSeed ( int n );
    static int getRandomSeed();

    // set and get methods for framework mode (train(0), predict(1), blend(2))
    static void setFrameworkMode ( int mode );
    static int getFrameworkMode();

    // set and get methods for additional run parameter on startup
    static void setAdditionalStartupParameter ( char* s );
    static int getAdditionalStartupParameter();

    // set and get methods for the validation type
    static void setValidationType ( string s );
    static string getValidationType();
    
    // date conversions
    static uint convertDateToInt(uint day, uint month, uint year, uint hour, uint minute, uint second);
    static void convertIntToDate(uint date, uint &day, uint &month, uint &year, uint &hour, uint &minute, uint &second, uint &weekday);
    
    // set and get current number of epoch in gradient boosting
    static void setGradientBoostingEpoch( int epoch );
    static void setGradientBoostingMaxEpochs( int epochs );
    static int getGradientBoostingEpoch();
    static int getGradientBoostingMaxEpochs();
    static string getGradientBoostingEpochString();
    
    // used for communication in gradient boosting
    static void setFrameworkID(int id);
    static int getFramworkID();
    
private:

    static bool m_isClassificationDataset;
    static int m_maxThreads;
    static int m_randomSeed;
    static int m_frameworkMode;
    static int m_additionalStartupParameter;
    static int m_gradientBoostingEpoch;
    static int m_gradientBoostingMaxEpochs;
    static int m_id;
    static string m_validationTypeFramework;
};

#endif
