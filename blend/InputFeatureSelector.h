#ifndef _INPUT_FEATURE_SELECTOR__
#define _INPUT_FEATURE_SELECTOR__

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
//#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <vector>
#include <map>
#include <dirent.h>
#include <algorithm>
#include <sstream>
#include "ippi.h"

using namespace std;

#include "StreamOutput.h"
#include "Data.h"
#include "Framework.h"
#include "AUC.h"
#include "NumericalTools.h"

#define FEATURE_TXT_FILE "features.txt"


/**
 * Used for feature selection
 * This implements a greedy technique, based on a linear model with small ridge regression
 * We use double cross-fold validation to determine the test error and break criteria
 * Same idea from: Andrew W. Moore http://www.cs.cmu.edu/~awm/
 * http://www.autonlab.org/tutorials/overfit.html  "cross-validation can overfit"
 *
 */

class InputFeatureSelector : public Framework
{

public:
    InputFeatureSelector();
    virtual ~InputFeatureSelector();

    static void forwardSelection ( bool* selection, REAL* inputs, int nFeatures, int nExamples, int* labels, REAL* targets, int nClass, int nDomain );

    static void backwardElimination ( bool* selection, REAL* inputs, int nFeatures, int nExamples, int* labels, REAL* targets, int nClass, int nDomain );

    static void selectFeatures ( bool* selection, REAL* inputs, int nFeatures, int nExamples, int* labels, REAL* targets, int nClass, int nDomain );

private:
    static void predictWithRidgeRegression ( int* colSelect, int nCol, int totalCol, int nTarget, REAL* ATAfull, REAL* ATbfull, REAL* testFeat, REAL* output, int nPred, REAL* ATA, REAL* ATb, REAL* AbTrans );

};


#endif
